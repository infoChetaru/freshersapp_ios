//
//  Commons.swift
//  FreshersApp
//
//  Created by Apple on 14/05/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import StoreKit

class Commons: NSObject {

    /**
        * This function converts UTC time to Local
        * @param: date: pass the utc date string which you want to convert to local
    */
    class func UTCToLocal(date:String, dateFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")

        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = dateFormat

        print("Converted to loacl:", dateFormatter.string(from: dt!))
        
        return dateFormatter.string(from: dt!)
    }
    
    class func convertDateFormatter(date: String, changeInto: String, ChangeFrom: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = ChangeFrom
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = changeInto
        
        if date == nil {
            return ""
        }
        return  dateFormatter.string(from: date!)
    }
    
    class func convertStringToDate(date: String, changeInto: String, ChangeFrom: String) -> Date {
        
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = ChangeFrom
//        let date = dateFormatter.date(from: date)
//        dateFormatter.dateFormat = changeInto
//
//        print("+++++++++++++++ ",dateFormatter.string(from: date!))
//
//        return  date ?? Date()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = ChangeFrom //Your date format
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT") //Current time zone
        //according to date format your date string
        guard let date = dateFormatter.date(from: date) else {
            return Date()
        }
        print(date) //Convert String to Date

        return  date
    }
        
    class func isPremiumUser() -> Bool {
        
        return AuthModel.sharedInstance.isPremiumUser
        /*if AuthModel.sharedInstance.isPremiumUser {
            
            let date1 = Date()
            let date2 = Commons.convertStringToDate(date: AuthModel.sharedInstance.strExpiryDate, changeInto: "yyyy-MM-dd HH:mm:ss", ChangeFrom: "yyyy-MM-dd HH:mm:ss")
            
            if (date1 == date2) || (date1 < date2) {
                //active, show already premium user screen
                return true
            }
            else if date1 > date2 {
                //expired, please check the receipt
                return callWebServiceForVerifyReceipt(url: kStoreUrl, restore: false)
            }
            else {
                return false
            }
        }
        else {
            return false
        }*/
    }

    //MARK: - WebService Methods
    class func callWebServiceForVerifyReceipt(url: String, restore: Bool) -> Bool {
        
        var premium = false
        
        if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
            FileManager.default.fileExists(atPath: appStoreReceiptURL.path) {

        //start loader
//        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
                    
        let SUBSCRIPTION_SECRET = "cb9e0c8f5bb545ba89528729a4855e81"
        let receiptPath = Bundle.main.appStoreReceiptURL?.path
        if FileManager.default.fileExists(atPath: receiptPath!){
            var receiptData:NSData?
            do{
                receiptData = try NSData(contentsOf: Bundle.main.appStoreReceiptURL!, options: NSData.ReadingOptions.alwaysMapped)
            }
            catch{
                print("ERROR: " + error.localizedDescription)
            }
            //let receiptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            let base64encodedReceipt = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions.endLineWithCarriageReturn)

        //set param
        let param = ["receipt-data":base64encodedReceipt!,"password":SUBSCRIPTION_SECRET]

        WebServiceHandler.postWebService(url: url, param: param, withHeader: false ) { (response, errorMsg) in
            
            //stop loader
//            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                //show error message in case of no response
//                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                premium = false
            }
            else{
                //print(response)
                //"https://sandbox.itunes.apple.com/verifyReceipt"
                if response!["status"].stringValue == "21007" {
                    self.callWebServiceForVerifyReceipt(url: kSandboxUrl, restore: restore)
                }
                else {
                    print("got")
                    if response!["pending_renewal_info"].arrayValue.count > 0 {
                        
                        var expDateArr = response!["latest_receipt_info"].arrayValue.last! ["expires_date"].stringValue.components(separatedBy: " ")
                        expDateArr.removeLast()
                        
                        var originalPurchaseDate = response!["latest_receipt_info"].arrayValue.last! ["original_purchase_date"].stringValue.components(separatedBy: " ")
                        originalPurchaseDate.removeLast()
                        
                        if response!["pending_renewal_info"].arrayValue[0]["expiration_intent"].exists() {
                            //subscription expired
                            

                            self.callWebServiceForSaveTransaction(transactionID: response!["pending_renewal_info"].arrayValue[0]["original_transaction_id"].stringValue, expiryDate: expDateArr.joined(separator: " "), paymentStatus: "2", purchaseDate: originalPurchaseDate.joined(separator: " "))
                            
//                            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "There is no active subscription plan for your account. Please upgrade your plan.")

                            premium = false

                        }
                        else {
                            //active subscription

                            self.callWebServiceForSaveTransaction(transactionID: response!["pending_renewal_info"].arrayValue[0]["original_transaction_id"].stringValue, expiryDate: expDateArr.joined(separator: " "), paymentStatus: "1", purchaseDate: originalPurchaseDate.joined(separator: " "))
                            
                            premium = true
                        }
                    }
                    else {
                        premium = false
                    }
                }
            }
            }
        }
        }
        else {
            print("No receipt found even though there is an indication something has been purchased before")
            print("^No receipt found. Need to refresh receipt.")
//            self.refreshReceipt()
            premium = false
        }
        
        return premium
    }
    
    class func callWebServiceForSaveTransaction(transactionID: String, expiryDate: String, paymentStatus: String, purchaseDate: String) {
               
//        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)

        /*let param = [
            "transactionId": transactionID,
            "expiryDate": expiryDate,
            "paymentStatus": paymentStatus,
            "purchaseDate": purchaseDate
            ] as [String : Any]*/
        
        let param = [
            "transactionId": transactionID
            ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Payment.goPremium, param: param, withHeader: true ) { (response, errorMsg) in
            
//            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
//                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                print(response)
                if response!["status"].boolValue == true {
                    if paymentStatus == "2" {
                        //expired move to upgrade screen
                        AuthModel.sharedInstance.isPremiumUser = false
                    }
                    else {
                        //payment success move to already premium user screen
                        AuthModel.sharedInstance.strExpiryDate = expiryDate
                        AuthModel.sharedInstance.strSubscriptionDate = purchaseDate
                        AuthModel.sharedInstance.isPremiumUser = true
                    }
                }
                else {
//                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    class func callWebServiceToGetNotificationCount(tabBarController: UITabBarController, index: String) {
                        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Notification.unreadNotificationCount, param: [:], withHeader: true ) { (response, errorMsg) in
                        
            if response == nil {
            }
            else{
                if response!["status"].boolValue == true {
                    if let tabItems = tabBarController.tabBar.items {
                        // In this case we want to modify the badge number of the third tab:
                        
                        if index != "3" {
                            let tabItem = tabItems[3]

                            if response!["data"].stringValue == "" || response!["data"].stringValue == "0"{
                                tabItem.badgeValue = nil
                            }
                            else {
                                tabItem.badgeValue = response!["data"].stringValue
                            }
                        }
                    }
                    if let tabItems = tabBarController.tabBar.items {
                        // In this case we want to modify the badge number of the fourth tab:
                        
                        if index != "4" {
                            let tabItem = tabItems[4]
                            
                            var chatCount = 0
                            var pendingRequestCount = 0
                            
                            if response!["chatCount"].stringValue == "" || response!["chatCount"].stringValue == "0"{
                                //                            tabItem.badgeValue = nil
                                chatCount = 0
                            }
                            else {
                                chatCount = response!["chatCount"].intValue
                                //                            tabItem.badgeValue = response!["chatCount"].stringValue
                            }
                            
                            if response!["pendingRequestCount"].stringValue == "" || response!["pendingRequestCount"].stringValue == "0"{
                                pendingRequestCount = 0
                            }
                            else {
                                pendingRequestCount = response!["pendingRequestCount"].intValue
                            }
                            
                            if chatCount != 0 || pendingRequestCount != 0 {
                                let totalCount = chatCount + pendingRequestCount
                                tabItem.badgeValue = String(totalCount)
                            }
                            else {
                                tabItem.badgeValue = nil
                            }
                        }
                    }
                }
                else {
                }
            }
        }
    }
    
    class func callWebServiceForUpdateUserLocation(strLocaleID: String, strLocaleName: String, completionHandler: @escaping () -> Void) {
                
        let param = [
            "localeId": strLocaleID
            ] as [String : Any]
        
        print(param)

        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Profile.updateLocale, param: param, withHeader: true ) { (response, errorMsg) in
                        
            if response == nil {
                completionHandler()
            }
            else{
                if response!["status"].boolValue == true {
                    
                    AuthModel.sharedInstance.strLocaleID = strLocaleID
                    AuthModel.sharedInstance.strLocaleName = strLocaleName
                    UserDefaults.standard.set(AuthModel.sharedInstance.strLocaleID, forKey: "localeId")
                    UserDefaults.standard.set(AuthModel.sharedInstance.strLocaleName, forKey: "localeName")
                    completionHandler()
                }
                else {
                    completionHandler()
                }
            }
        }
    }

}
