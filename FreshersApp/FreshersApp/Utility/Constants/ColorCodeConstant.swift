//
//  ColorCodeConstant.swift
//  Tribe365
//
//  Created by Apple on 11/11/19.
//  Copyright © 2019 chetaru. All rights reserved.
//

import UIKit

class ColorCodeConstant: NSObject {

    static let themeColor = UIColor(hexString: "640064")
    static let themeLightColor = UIColor(hexString: "d1b2d1")
}

