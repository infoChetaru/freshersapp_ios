//
//  NetworkConstant.swift
//  Tribe365
//
//  Created by kdstudio on 30/05/18.
//  Copyright © 2018 chetaru. All rights reserved.
//

import UIKit

/*LIVE*/
let kBaseURL = "https://freshersapp.chetaru.co.uk/api/"
let kTermsOfServicesURL = "https://freshersapp.chetaru.co.uk/termsOfServices"
let kPrivacyPolicyURL = "https://freshersapp.chetaru.co.uk/privacyPolicy"

/*PRODUCTION*/
//let kBaseURL = "https://fresherappdemo.chetaru.co.uk/api/"
//let kTermsOfServicesURL = "https://fresherappdemo.chetaru.co.uk/termsOfServices"
//let kPrivacyPolicyURL = "https://fresherappdemo.chetaru.co.uk/privacyPolicy"

/*PRODUCTION OLD*/
//let kBaseURL = "https://production.chetaru.co.uk/freshersApp/api/"
//let kTermsOfServicesURL = "https://production.chetaru.co.uk/freshersApp/termsOfServices"
//let kPrivacyPolicyURL = "https://production.chetaru.co.uk/freshersApp/privacyPolicy"

let kAppName  = "FreshersApp"
let kEmailUsername  = "1"
let kPhoneUsername  = "2"
let kGenderMale  = "1"
let kGenderFemale  = "2"
let kGenderOther  = "3"
let kPeopleTag  = "1"
let kHashTag  = "2"
let kLike  = "1"
let kUnLike  = "2"
let kAllowSensitive  = "1"
let kNotAllowSensitive  = "2"
let kRememberArray  = "arrayForRemember"
let kCurrentPageFeed  = "currentPageFeed"
let kSelectedFeedID  = "selectedIndexFeed"
//let kLoadMoreFeed  = "loadmorefeed"
//let kTotalPageCountFeed  = "totalpagecountfeed"

class NetworkConstant: NSObject {
    
    struct Auth {
        static let verifyCode = "verifyUserName"
        static let forgotpassword = "forgotPassword"
        static let login = "userLogin"
        static let countryCodeList = "getCountryList"
        static let signup = "usersignup"
        static let logout = "userLogout"
        static let resetPassword = "resetPasswordForPhone"
    }

    struct PostFeed {
        static let tagList = "getDataForPost"
        static let addPost = "userAddPost"
    }

    struct Feed {
        static let feedList = "userViewPosts"
        static let likePost = "userLikedPost"
        static let hashTagFeedList = "hashTagsUserPost"
        static let blockUser = "blockUser"
        static let contactUs = "userPostContactUs"
        static let postDetail = "getPostDetails"
        static let likedUser = "postLikedByUser"
        static let reportPost = "userReportPost"
    }
    
    struct Notification {
        static let notificationList = "readUnreadNotificationList"
        static let markNotificationRead = "markNotificationAsRead"
        static let unreadNotificationCount = "unreadNotificationCount"
    }
    
    struct Profile {
        static let userProfile = "getUserProfile"
        static let localeList = "getAllLocales"
        static let updateProfile = "updateUserProfile"
        static let userUploads = "getUserUploads"
        static let deleteUpload = "deleteUserPosts"
        static let likedPosts = "getUserLikedPost"
        static let updateLocale = "updateUsersLocale"
        static let blockedUsers = "getBlockedUserList"
        static let userContactUs = "userContactUs"
        static let updateSesitiveStatus = "updateUserSensitivePostStatus"
    }
    
    struct Connect {
        static let userConnect = "sendConnectionRequestToUser"
        static let connectionList = "acceptedConnectionList"
        static let requestList = "getUsersPendingRequestList"
        static let acceptRejectRequest = "acceptOrDeclineUserRequest"
        static let getChatMessages = "getUsersChat"
        static let sendMessage = "sendMessage"
        static let getConnectionStatus = "getConnectionStatusOfUser"
    }

    struct Payment {
        static let goPremium = "addExpiryDateForMembership"
        static let getDefaultLocale = "getDefaultLocale"
        static let getPremiumStatusOfUser = "getPremiumUserStatus"
    }
}
