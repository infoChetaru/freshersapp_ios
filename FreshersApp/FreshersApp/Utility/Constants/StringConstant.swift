//
//  StringConstant.swift
//  FreshersApp
//
//  Created by Apple on 15/04/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class StringConstant: NSObject {

    static let emptyPhoneNumberEmail = "Please enter Phone Number/Email."
    static let emptyUsername = "Please enter Username."
    static let emptyPassword = "Please enter Password."
    static let emptyEmail = "Please enter Email."
    static let validEmail = "Please enter valid Email."
    static let emptyPhoneNumber = "Please enter Phone Number."
    static let validCountryCode = "Please select valid country code."
    static let validPhoneNumber = "Please enter valid Phone Number."
    static let validCode = "Please enter valid code."
    static let successVerifiedCode = "Code Verified. Please set your password."
    static let emptyConfirmPassword = "Please enter Confirm Password."
    static let emptyText = "Please write something."
    static let emptyMessage = "Please enter message."

    
    static let appName = "FreshersApp"
    static let mismatchPasswordConfirmPassword = "Password and Confirm Password did not match."
    static let titleWarning = "Warning"
    static let titleNoCamera = "You don't have camera"
    static let titleOK = "OK"
    static let titleView = "View"
    static let titleSelectOptionMedia = "Choose your file from any of this options"
    static let titleCamera = "Camera"
    static let titleGallery = "Gallery"
    static let titleCancel = "Cancel"
    static let emptyDOB = "Please select Date Of Birth."
    static let emptyBio = "Please enter Bio."
    static let titleNoSupportedImage = "This image will not support."
    static let titleSureLogout = "Are you sure you want to Logout?"
    static let titleSureDeletePost = "Are you sure you want to delete post?"
    static let titleSureUnblock = "Are you sure you want to unblock the user?"

    static let titleYes = "Yes"
    static let titleNo = "No"
    static let titleDelete = "Delete"
    static let titleLogout = "Logout"
    static let titleBlock = "Block"
    static let titleDisconnect = "Disconnect"
    static let titleUnBlock = "Unblock"

    static let titleUserLocation = "User Location"
    static let titleGoToSettingsPost = "Please go to Settings and turn on the location permissions in order to post feed."
    static let titleGoToSettingsView = "Please go to Settings and turn on the location permissions in order to view feed."
    static let titleSettings = "Settings"
    static let titleComingSoon = "Coming Soon!"
    static let titleSureBlock = "Are you sure you want to Block "
    static let titleForPremiumUsers = "Only for Premium Users."

    static let titleAllowLocation = "To re-enable, please go to Settings and turn on Location Service for this app."
    static let titleLocationDenied = "App Permission Denied"


}
