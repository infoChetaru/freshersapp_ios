//
//  FeedParser.swift
//  FreshersApp
//
//  Created by Apple on 17/04/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import SwiftyJSON

/**
* This class is used to parse the api details for Feed module
*/
class FeedParser: NSObject {

    /**
    * This function is used to parse country code list
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: CountryCodeModel
    */
    class func parseTagList(response : JSON, completionHandler: @escaping ([TagModel], [TagModel]) -> Void) {
        
        var arrTagPeopleList = [TagModel]()
        var arrHashTagList = [TagModel]()

        for json in response["data"]["hashTagsList"].arrayValue {
            
            let model = TagModel()
            
            model.strID = json["id"].stringValue
            model.strName = json["tagName"].stringValue.replacingOccurrences(of: " ", with: "_")

            arrHashTagList.append(model)
        }

        for json in response["data"]["userList"].arrayValue {
            
            let model = TagModel()
            
            model.strID = json["id"].stringValue
            model.strImgUrl = json["imageUrl"].stringValue
            model.strName = json["name"].stringValue.replacingOccurrences(of: " ", with: "_")

            if model.strID == AuthModel.sharedInstance.strID {
                continue
            }
            arrTagPeopleList.append(model)
        }

        completionHandler(arrTagPeopleList, arrHashTagList)
    }
    
    /**
    * This function is used to parse feed list
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: FeedModel
    */
    class func parseFeedList(response : JSON, completionHandler: @escaping ([FeedModel], String) -> Void) {
        
        var arrFeedList = [FeedModel]()

        for json in response["data"].arrayValue {
            
            let model = FeedModel()
            var arrHashTags = [TagModel]()
            var arrUserTags = [TagModel]()

            model.strCaption = json["caption"].stringValue
            
            for subJson in json["hashTagList"].arrayValue {
                
                let model = TagModel()
                
                model.strID = subJson["id"].stringValue
                model.strName = subJson["name"].stringValue
                
                arrHashTags.append(model)
            }
            
            model.arrHashTag = arrHashTags
            model.strID = json["id"].stringValue
            model.strRoleID = json["roleId"].stringValue
            model.strUserID = json["userId"].stringValue
            model.strSensitivePost = json["sensitivePostStatus"].stringValue
            model.strImgUrl = json["imageUrl"].stringValue
            model.strUsername = json["name"].stringValue
            model.isLiked = json["likedStatus"].boolValue
            model.strUserConnectStatus = json["userConnectionStatus"].stringValue
            model.strConnectionID = json["connectionId"].stringValue
            model.strLikeCount = json["likedCount"].stringValue
            model.strUserImage = json["userImage"].stringValue
            model.strOriginalPostID = json["originalPostId"].stringValue
            model.strOriginalPostUserID = json["originalPostUserId"].stringValue

            model.strDate = Commons.UTCToLocal(date: json["postedDate"].stringValue, dateFormat: "MMM dd | hh:mm a")

            for subJson in json["userTagList"].arrayValue {
                
                let model = TagModel()
                
                model.strID = subJson["id"].stringValue
                model.strName = subJson["name"].stringValue.replacingOccurrences(of: " ", with: "_")
                model.strUserConnectStatus = subJson["tagUserConnectionStatus"].stringValue
                model.strUserConnectionID = subJson["connectionId"].stringValue

                arrUserTags.append(model)
            }
            
            model.arrUserTag = arrUserTags

            arrFeedList.append(model)
        }
        
        completionHandler(arrFeedList, response["totalPageCount"].stringValue)
    }
    
    /**
    * This function is used to parse feed list
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: FeedModel
    */
    class func parseHashTagFeedsList(response : JSON, completionHandler: @escaping ([FeedModel], String, [String]) -> Void) {
        
        var arrFeedList = [FeedModel]()

        for json in response["data"].arrayValue {
            
            let model = FeedModel()
            var arrHashTags = [TagModel]()
            var arrUserTags = [TagModel]()

            model.strCaption = json["caption"].stringValue
            
            for subJson in json["hashTagList"].arrayValue {
                
                let model = TagModel()
                
                model.strID = subJson["id"].stringValue
                model.strName = subJson["name"].stringValue
                
                arrHashTags.append(model)
            }
            
            model.arrHashTag = arrHashTags
            model.strID = json["id"].stringValue
            model.strUserID = json["userId"].stringValue
            model.strImgUrl = json["imageUrl"].stringValue
            model.strUsername = json["name"].stringValue
            model.isLiked = json["likedStatus"].boolValue
            model.strLikeCount = json["likesCount"].stringValue
            model.strDate = json["created_at"].stringValue
            model.expiredPost = json["postExpired"].boolValue
            model.strUserImage = json["userImage"].stringValue
            model.strConnectionID = json["connectionId"].stringValue
            model.strDate = Commons.UTCToLocal(date: json["postedDate"].stringValue, dateFormat: "MMM dd | hh:mm a")
            model.strRoleID = json["roleId"].stringValue
            model.strSensitivePost = json["sensitivePostStatus"].stringValue
            model.strUserConnectStatus = json["userConnectionStatus"].stringValue
            model.strOriginalPostID = json["originalPostId"].stringValue

            for subJson in json["userTagList"].arrayValue {
                
                let model = TagModel()
                
                model.strID = subJson["id"].stringValue
                model.strName = subJson["name"].stringValue.replacingOccurrences(of: " ", with: "_")

                arrUserTags.append(model)
            }
            
            model.arrUserTag = arrUserTags

            arrFeedList.append(model)
        }
        
        completionHandler(arrFeedList, response["totalPageCount"].stringValue, response["unlikedUserId"].stringValue.components(separatedBy: ","))
    }
    
    /**
    * This function is used to parse liked users list
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: UserProfileModel
    */
    class func parseLikedUsersList(response : JSON, completionHandler: @escaping ([UserProfileModel]) -> Void) {
        
        var arrUsers = [UserProfileModel]()

        for json in response["data"].arrayValue {
            
            let model = UserProfileModel()

            model.strID = json["userId"].stringValue
            model.strName = json["userName"].stringValue
            model.strProfileImage = json["imageUrl"].stringValue

            arrUsers.append(model)
        }
        
        completionHandler(arrUsers)
    }


}
