//
//  ProfileParser.swift
//  FreshersApp
//
//  Created by Apple on 12/06/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProfileParser: NSObject {

    /**
    * This function is used to parse feed list
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: LocaleModel
    */
    class func parseLocaleList(response : JSON, completionHandler: @escaping ([LocaleModel]) -> Void) {
        
        var arrLocales = [LocaleModel]()

        for json in response["data"].arrayValue {
            
            let model = LocaleModel()

            model.strID = json["id"].stringValue
            model.strName = json["name"].stringValue
            model.strCountryName = json["country"].stringValue
            
            arrLocales.append(model)
        }
        
        completionHandler(arrLocales)
    }
    
    /**
    * This function is used to parse blocked user list
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: UserProfileModel
    */
    class func parseBlockedList(response : JSON, completionHandler: @escaping ([UserProfileModel]) -> Void) {
        
        var arrUsers = [UserProfileModel]()

        for json in response["data"].arrayValue {
            
            let model = UserProfileModel()

            model.strID = json["id"].stringValue
            model.strName = json["name"].stringValue
            model.strProfileImage = json["imageUrl"].stringValue
            
            arrUsers.append(model)
        }
        
        completionHandler(arrUsers)
    }


}
