//
//  NotificationParser.swift
//  FreshersApp
//
//  Created by Apple on 14/05/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import SwiftyJSON

class NotificationParser: NSObject {

    /**
    * This function is used to parse notification list
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: NotificationModel
    */
    class func parseNotificationList(response : JSON, completionHandler: @escaping ([NotificationModel], String) -> Void) {
        
        var arrNotificationList = [NotificationModel]()

        for json in response["data"].arrayValue {
            
            let model = NotificationModel()

            model.strCreatedAt = json["created_at"].stringValue
            model.strDate = Commons.UTCToLocal(date: json["created_at"].stringValue, dateFormat: "dd MMM, yyyy")
            model.strDesc = json["description"].stringValue
            model.strID = json["id"].stringValue
            model.strImageUrl = json["image"].stringValue
            model.isRead = json["isRead"].boolValue
            model.strType = json["notificationType"].stringValue
            model.isInLocale = json["locale"].boolValue
            model.strPostID = json["postId"].stringValue
            model.strPostExpired = json["postExpired"].boolValue

            arrNotificationList.append(model)
        }
        
        completionHandler(arrNotificationList, response["totalPageCount"].stringValue)
    }
    
    /**
    * This function is used to parse feed list
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model named: FeedModel
    */
    class func parsePostDetail(response : JSON, completionHandler: @escaping (FeedModel) -> Void) {
        
        let model = FeedModel()
            
            var arrHashTags = [TagModel]()
            var arrUserTags = [TagModel]()

            model.strCaption = response["caption"].stringValue
            
            for subJson in response["hashTagList"].arrayValue {
                
                let model = TagModel()
                
                model.strID = subJson["id"].stringValue
                model.strName = subJson["name"].stringValue
                
                arrHashTags.append(model)
            }
            
            model.arrHashTag = arrHashTags
            model.strID = response["id"].stringValue
            model.strUserID = response["userId"].stringValue
            model.strOriginalPostID = response["originalPostId"].stringValue
            model.strSensitivePost = response["sensitivePostStatus"].stringValue
            model.strRoleID = response["roleId"].stringValue
            model.strImgUrl = response["imageUrl"].stringValue
            model.strUsername = response["name"].stringValue
            model.strConnectionID = response["connectionId"].stringValue
            model.strUserConnectStatus = response["userConnectionStatus"].stringValue
            model.isLiked = response["likedStatus"].boolValue
            model.strLikeCount = response["likedCount"].stringValue
            model.strDate = Commons.UTCToLocal(date: response["postedDate"].stringValue, dateFormat: "MMM dd | hh:mm a")
            model.strUserImage = response["userImage"].stringValue

            for subJson in response["userTagList"].arrayValue {
                
                let model = TagModel()
                
                model.strID = subJson["id"].stringValue
                model.strName = subJson["name"].stringValue.replacingOccurrences(of: " ", with: "_")
                model.strUserConnectStatus = subJson["tagUserConnectionStatus"].stringValue
                model.strUserConnectionID = subJson["connectionId"].stringValue

                arrUserTags.append(model)
            }
            
            model.arrUserTag = arrUserTags

        completionHandler(model)
    }

}
