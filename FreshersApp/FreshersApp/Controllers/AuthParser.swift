//
//  AuthParser.swift
//  FreshersApp
//
//  Created by Apple on 08/04/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import SwiftyJSON

/**
* This class is used to parse the api details for Auth module
*/
class AuthParser: NSObject {

    /**
    * This function is used to parse country code list
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: CountryCodeModel
    */
    class func parseCountryCodeList(response : JSON, completionHandler: @escaping ([CountryCodeModel]) -> Void) {
        
        var arrDept = [CountryCodeModel]()
        
        for json in response["data"].arrayValue {
            
            let model = CountryCodeModel()
            
            model.strID = json["id"].stringValue
            model.strCountryName = json["countryName"].stringValue
            model.strCountryCode = json["phonecode"].stringValue

            arrDept.append(model)
        }
        
        completionHandler(arrDept)
    }

    /**
    * This function is used to parse User Authentication data
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model named: AuthModel
    */
    class func parseAuthenticationDetails(response : JSON, completionHandler: @escaping (AuthModel) -> Void) {
        
        let defaults = UserDefaults.standard
        let objAuth = AuthModel.sharedInstance

        objAuth.strBio = response["data"]["bio"].stringValue
        objAuth.strCountryID = response["data"]["countryId"].stringValue
        objAuth.strDob = response["data"]["dob"].stringValue
        objAuth.strGenderID = response["data"]["gender"].stringValue
        objAuth.strID = response["data"]["id"].stringValue
        objAuth.strName = response["data"]["name"].stringValue
        objAuth.strProfileImage = response["data"]["profileImage"].stringValue
        objAuth.strRole = response["data"]["role"].stringValue
        objAuth.strToken = response["data"]["token"].stringValue
        objAuth.strType = response["data"]["type"].stringValue
        objAuth.strUsername = response["data"]["userName"].stringValue
        objAuth.strTopPostID = response["data"]["postId"].stringValue
        objAuth.strTopPostImgUrl = response["data"]["postImageUrl"].stringValue
        objAuth.strTopPostLikeCount = response["data"]["likedUserCount"].stringValue
        objAuth.isPremiumUser = response["data"]["premieUser"].boolValue
        objAuth.strLocaleID = response["data"]["localeId"].stringValue
        objAuth.strFcmId = response["data"]["fcmId"].stringValue
        objAuth.strExpiryDate = response["data"]["expiryDate"].stringValue
        objAuth.strSubscriptionDate = response["data"]["purchaseDate"].stringValue
        objAuth.strSensitivePostAllowed = response["data"]["sensitivePostsAllowed"].stringValue

        defaults.set(objAuth.strBio, forKey: "bio")
        defaults.set(objAuth.strCountryID, forKey: "countryId")
        defaults.set(objAuth.strDob, forKey: "dob")
        defaults.set(objAuth.strGenderID, forKey: "gender")
        defaults.set(objAuth.strID, forKey: "id")
        defaults.set(objAuth.strName, forKey: "name")
        defaults.set(objAuth.strProfileImage, forKey: "profileImage")
        defaults.set(objAuth.strRole, forKey: "role")
        defaults.set(objAuth.strToken, forKey: "token")
        defaults.set(objAuth.strType, forKey: "type")
        defaults.set(objAuth.strUsername, forKey: "userName")
        defaults.set(objAuth.strTopPostID, forKey: "postId")
        defaults.set(objAuth.strTopPostImgUrl, forKey: "postImageUrl")
        defaults.set(objAuth.strTopPostLikeCount, forKey: "likedUserCount")
        defaults.set(objAuth.isPremiumUser, forKey: "premiumUser")
        defaults.set(objAuth.strLocaleID, forKey: "localeId")
        defaults.set(objAuth.strFcmId, forKey: "fcmId")
        defaults.set(objAuth.strExpiryDate, forKey: "expiryDate")
        defaults.set(objAuth.strSubscriptionDate, forKey: "purchaseDate")
        defaults.set(objAuth.strSensitivePostAllowed, forKey: "sensitivePostsAllowed")

        completionHandler(objAuth)
    }
    
    /**
    * This function is used to parse User data
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model named: AuthModel
    */
    class func parseProfileDetails(response : JSON, completionHandler: @escaping (AuthModel) -> Void) {
        
        let defaults = UserDefaults.standard
        let objAuth = AuthModel.sharedInstance

        objAuth.strBio = response["data"]["bio"].stringValue
        objAuth.strDob = response["data"]["dob"].stringValue
        objAuth.strGenderID = response["data"]["gender"].stringValue
        objAuth.strID = response["data"]["id"].stringValue
        objAuth.strTopPostLikeCount = response["data"]["likedUserCount"].stringValue
        objAuth.strLocaleID = response["data"]["localeId"].stringValue
        objAuth.strName = response["data"]["name"].stringValue
        objAuth.strTopPostID = response["data"]["postId"].stringValue
        objAuth.strTopPostImgUrl = response["data"]["postImageUrl"].stringValue
        objAuth.isPremiumUser = response["data"]["premieUser"].boolValue
        objAuth.strProfileImage = response["data"]["profileImage"].stringValue
        objAuth.strUsername = response["data"]["userName"].stringValue
        objAuth.strLocaleName = response["data"]["localeName"].stringValue
        objAuth.strExpiryDate = response["data"]["expiryDate"].stringValue
        objAuth.strSubscriptionDate = response["data"]["purchaseDate"].stringValue
        objAuth.strSensitivePostAllowed = response["data"]["sensitivePostsAllowed"].stringValue

        defaults.set(objAuth.strBio, forKey: "bio")
        defaults.set(objAuth.strDob, forKey: "dob")
        defaults.set(objAuth.strGenderID, forKey: "gender")
        defaults.set(objAuth.strID, forKey: "id")
        defaults.set(objAuth.strTopPostLikeCount, forKey: "likedUserCount")
        defaults.set(objAuth.strLocaleID, forKey: "localeId")
        defaults.set(objAuth.strName, forKey: "name")
        defaults.set(objAuth.strTopPostID, forKey: "postId")
        defaults.set(objAuth.strTopPostImgUrl, forKey: "postImageUrl")
        defaults.set(objAuth.isPremiumUser, forKey: "premiumUser")
        defaults.set(objAuth.strProfileImage, forKey: "profileImage")
        defaults.set(objAuth.strUsername, forKey: "userName")
        defaults.set(objAuth.strLocaleName, forKey: "localeName")
        defaults.set(objAuth.strExpiryDate, forKey: "expiryDate")
        defaults.set(objAuth.strSubscriptionDate, forKey: "purchaseDate")
        defaults.set(objAuth.strSensitivePostAllowed, forKey: "sensitivePostsAllowed")

        completionHandler(objAuth)
    }
    
    /**
    * This function is used to parse other User data
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model named: AuthModel
    */
    class func parseUserProfileDetails(response : JSON, completionHandler: @escaping (UserProfileModel) -> Void) {
        
        let objProfile = UserProfileModel()
        
        objProfile.strConnectionStatus = response["data"]["connectionStatus"].stringValue
        objProfile.strBio = response["data"]["bio"].stringValue
        objProfile.strDob = response["data"]["dob"].stringValue
        objProfile.strGenderID = response["data"]["gender"].stringValue
        objProfile.strID = response["data"]["id"].stringValue
        objProfile.strTopPostLikeCount = response["data"]["likedUserCount"].stringValue
        objProfile.strName = response["data"]["name"].stringValue
        objProfile.strTopPostID = response["data"]["postId"].stringValue
        objProfile.strTopPostImgUrl = response["data"]["postImageUrl"].stringValue
        objProfile.isPremiumUser = response["data"]["premieUser"].boolValue
        objProfile.strProfileImage = response["data"]["profileImage"].stringValue
        objProfile.strUsername = response["data"]["userName"].stringValue
        objProfile.strLocaleName = response["data"]["localeName"].stringValue
        objProfile.strLocaleID = response["data"]["localeId"].stringValue

        completionHandler(objProfile)
    }


}
