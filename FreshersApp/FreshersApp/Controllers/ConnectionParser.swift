//
//  ConnectionParser.swift
//  FreshersApp
//
//  Created by Apple on 18/06/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import SwiftyJSON

class ConnectionParser: NSObject {

    /**
    * This function is used to parse request list
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: RequestModel
    */
    class func parseRequestList(response : JSON, completionHandler: @escaping ([RequestModel], String, [String]) -> Void) {
        
        var arr = [RequestModel]()

        for json in response["data"].arrayValue {
            
            let model = RequestModel()

            model.strID = json["id"].stringValue
            model.strImageUrl = json["imageUrl"].stringValue
            model.strName = json["name"].stringValue
            model.strOpeningMessage = json["openingMessage"].stringValue
            model.strUpdatedAt = json["updated_at"].stringValue
            model.strUserID = json["userId"].stringValue

            arr.append(model)
        }
        
        var arrOfUnfriendConnections = [String]()
        for value in response["connectionList"].arrayValue {
            arrOfUnfriendConnections.append(value.stringValue)
        }

        completionHandler(arr, response["totalPageCount"].stringValue, arrOfUnfriendConnections)
    }
    
    /**
    * This function is used to parse connection list
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: ConnectionModel
    */
    class func parseConnectionList(response : JSON, completionHandler: @escaping ([ConnectionModel], String, [String], [String]) -> Void) {
        
        var arr = [ConnectionModel]()

        for json in response["data"].arrayValue {
            
            let model = ConnectionModel()

            model.strConnectionId = json["connectionId"].stringValue
            model.strFileType = json["fileType"].stringValue
            model.strLastMessage = json["lastMessage"].stringValue
            model.strCreatedAt = json["created_at"].stringValue
            model.strUserID = json["userId"].stringValue
            model.strUserImgUrl = json["userImageUrl"].stringValue
            model.strUserName = json["userName"].stringValue
            model.strMessageCount = json["messageCount"].stringValue
            model.strUpdatedAt = json["updated_at"].stringValue

            var dateKey = "lastMsgDate"
            if json["lastMsgDate"].stringValue == "" {
                dateKey = "created_at"
            }
            
            if json[dateKey].stringValue != "" {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let lastMsgDateDate = dateFormatter.date(from: Commons.UTCToLocal(date: json[dateKey].stringValue, dateFormat: "yyyy-MM-dd HH:mm:ss"))
                
                if Calendar.current.isDateInToday(lastMsgDateDate!) {
                    model.strLastMsgDate = Commons.UTCToLocal(date: json[dateKey].stringValue, dateFormat: "hh:mm a")
                }
                else if Calendar.current.isDateInYesterday(lastMsgDateDate!) {
                    model.strLastMsgDate = "yesterday"
                }
                else {
                    model.strLastMsgDate = Commons.UTCToLocal(date: json[dateKey].stringValue, dateFormat: "dd/MM/yyyy")
                }
            }
                    
            arr.append(model)
        }
        
        var arrOfUnfriendConnections = [String]()
        for value in response["connectionList"].arrayValue {
            arrOfUnfriendConnections.append(value.stringValue)
        }

        var arrOfDeletedUsers = [String]()
        for value in response["deletedUsersList"].arrayValue {
            arrOfDeletedUsers.append(value.stringValue)
        }

        completionHandler(arr, response["totalPageCount"].stringValue, arrOfUnfriendConnections, arrOfDeletedUsers)
    }
    
    /**
    * This function is used to parse message list
    
    * @param JSON response:  Accepts response to parse
    * @param completionHandler:  call back method used to send the parsed data in form of Model array named: MessageModel
    */
    class func parseMessageList(response : JSON, arrAllMessageModel: [MessageModel], completionHandler: @escaping ([ChatModel], String) -> Void) {
        
        var arrMessageModel = [MessageModel]()
        var arrSectionModel = [ChatModel]()

        for json in response["data"].arrayValue {
            
            let model = MessageModel()

            model.strConnectionID = json["connectionId"].stringValue
            model.strCreatedAt = json["created_at"].stringValue
            model.strFileType = json["fileType"].stringValue
            model.strMessage = json["message"].stringValue
            model.strMessageID = json["messageId"].stringValue
            model.strUserID = json["userId"].stringValue
            model.strUserImage = json["userImage"].stringValue
            model.strUserName = json["userName"].stringValue
            model.strDate = Commons.UTCToLocal(date: json["msgDate"].stringValue, dateFormat: "hh:mm a")
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let strdate = dateFormatter.date(from: Commons.UTCToLocal(date: json["msgDate"].stringValue, dateFormat: "yyyy-MM-dd HH:mm:ss"))
            
            if Calendar.current.isDateInToday(strdate!) {
                model.strdSectionDate = "Today"
            }
            else if Calendar.current.isDateInYesterday(strdate!) {
                model.strdSectionDate = "Yesterday"
            }
            else {
                model.strdSectionDate = Commons.UTCToLocal(date: json["msgDate"].stringValue, dateFormat: "dd/MM/yyyy")
            }
            
            arrMessageModel.append(model)
        }
        
        var arrUniqueDates = [String]()
        
        
//        var newModelOfArrayForUniqueValues = [MessageModel]()
//        if arrAllMessageModel.count == 0 {
//            newModelOfArrayForUniqueValues = arrMessageModel
//        }
//        else {
//            newModelOfArrayForUniqueValues.append(contentsOf: arrMessageModel)
//        }
        
        var strUniDate = arrMessageModel[0].strdSectionDate
        arrUniqueDates.append(strUniDate)

        for valuemain in arrMessageModel {
            if valuemain.strdSectionDate != strUniDate {
                strUniDate = valuemain.strdSectionDate
                arrUniqueDates.append(valuemain.strdSectionDate)
            }
        }
        
        if arrMessageModel.count > 0 {
            for valuemain in arrUniqueDates {
                let chatModel = ChatModel()
                chatModel.strSectionDate = valuemain
                
                for value in arrMessageModel {
                    
                    if value.strdSectionDate == chatModel.strSectionDate {
                        chatModel.strConnectionID = value.strConnectionID
                        chatModel.arrMessages.append(value)
                    }
                }
                
                arrSectionModel.append(chatModel)
            }
        }
        
        completionHandler(arrSectionModel, response["totalPageCount"].stringValue)
    }

}
