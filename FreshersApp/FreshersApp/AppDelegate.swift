//
//  AppDelegate.swift
//  FreshersApp
//
//  Created by Apple on 03/04/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import FirebaseMessaging
//import LocalAuthentication

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        IQKeyboardManager.shared.enable = true
        
        FirebaseApp.configure()
        if #available(iOS 10.0, *) {
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()

        let defaults = UserDefaults.standard

        if AuthModel.sharedInstance.strID.isEmpty {
            if defaults.value(forKey: "token") != nil {
                
                //direct login
                setAuthenticationData()
                
                let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
                let rootController = storyboard.instantiateViewController(withIdentifier: "MainTab")
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                let rootController = storyboard.instantiateViewController(withIdentifier: "LogoutNav")
                self.window = UIWindow(frame: UIScreen.main.bounds)
                self.window?.rootViewController = rootController
                self.window?.makeKeyAndVisible()
            }
        }

        UIApplication.shared.applicationIconBadgeNumber = 0
        UIApplication.shared.isStatusBarHidden = true
        
        return true
    }
    
   /* func applicationWillEnterForeground(_ application: UIApplication) {
       /* let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Identify yourself!"
            
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                [weak self] success, authenticationError in
                
                DispatchQueue.main.async {
                    if success {
                        //success
                        print("Successssssssssssssssssssss")
                    } else {
//                        let ac = UIAlertController(title: "Biometry unavailable", message: "Your device is not configured for biometric authentication.", preferredStyle: .alert)
//                        ac.addAction(UIAlertAction(title: "OK", style: .default))
//                        self.present(ac, animated: true)
                        //error
                        print("Errorrrrrrrrrrrrrrrrrrrr")
                    }
                }
            }
        } else {
            // no biometry
            print("Errorrrrrrrrrrrrrrrrrrrr")
        }*/
        
        print("hello there!.. You have clicked the touch ID")
        
        let myContext = LAContext()
        let myLocalizedReasonString = "Biometric Authntication testing !! "
        
        var authError: NSError?
        if #available(iOS 8.0, macOS 10.12.1, *) {
            if myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
                myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString) { success, evaluateError in
                    
                    DispatchQueue.main.async {
                        if success {
                            // User authenticated successfully, take appropriate action
                            print("Awesome!!... User authenticated successfully")
                        } else {
                            // User did not authenticate successfully, look at error and take appropriate action
                            print("Sorry!!... User did not authenticate successfully")
                        }
                    }
                }
            } else {
                // Could not evaluate policy; look at authError and present an appropriate message to user
                print("Sorry!!.. Could not evaluate policy.")
            }
        } else {
            // Fallback on earlier versions
            
            print("Ooops!!.. This feature is not supported.")
        }

    }*/
    
    //MARK: - Custom Methods
    func setAuthenticationData() {
        
        let defaults = UserDefaults.standard
        let auth = AuthModel.sharedInstance
        
        auth.strBio = defaults.value(forKey: "bio") as! String
        auth.strCountryID = defaults.value(forKey: "countryId") as! String
        auth.strDob = defaults.value(forKey: "dob") as! String
        auth.strGenderID = defaults.value(forKey: "gender") as! String
        auth.strID = defaults.value(forKey: "id") as! String
        auth.strName = defaults.value(forKey: "name") as! String
        auth.strProfileImage = defaults.value(forKey: "profileImage") as! String
        auth.strRole = defaults.value(forKey: "role") as! String
        auth.strToken = defaults.value(forKey: "token") as! String
        auth.strType = defaults.value(forKey: "type") as! String
        auth.strUsername = defaults.value(forKey: "userName") as! String
        auth.strTopPostLikeCount = defaults.value(forKey: "likedUserCount") as! String
        auth.strLocaleID = defaults.value(forKey: "localeId") as! String
        auth.strTopPostID = defaults.value(forKey: "postId") as! String
        auth.strTopPostImgUrl = defaults.value(forKey: "postImageUrl") as! String
        auth.isPremiumUser = defaults.value(forKey: "premiumUser") as! Bool
        auth.strExpiryDate = defaults.value(forKey: "expiryDate") as! String
        auth.strSubscriptionDate = defaults.value(forKey: "purchaseDate") as! String
        auth.strSensitivePostAllowed = defaults.value(forKey: "sensitivePostsAllowed") as! String
        auth.strFcmId = defaults.value(forKey: "fcmId") as! String

    }


    // MARK: UISceneSession Lifecycle

    /*func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
*/

}

extension AppDelegate : MessagingDelegate {
  // [START refresh_token]
  func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
    print("Firebase registration token: \(fcmToken)")
    
    let defaults = UserDefaults.standard
    defaults.set(fcmToken, forKey: "fcm_token")
  }
  // [END refresh_token]
  // [START ios_10_data_message]
  // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
  // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
  func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
    print("Received data message: \(remoteMessage.appData)")
  }
  // [END ios_10_data_message]
}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo

        // Print full message.
        print(userInfo)
        
        if (userInfo["type"] as! String) == "sendMessage" {
            NotificationCenter.default.post(name: Notification.Name("ChatPush"), object: nil)
        }

        if (userInfo["type"] as! String) == "pendingRequest" || (userInfo["type"] as! String) == "acceptRequest" || (userInfo["type"] as! String) == "sendMessage"{
            if let tab = (self.window?.rootViewController as? UINavigationController)?.topViewController as? UITabBarController {
                
                Commons.callWebServiceToGetNotificationCount(tabBarController: tab, index: "")
            }
            if let tab = self.window?.rootViewController as? UITabBarController {
                
                Commons.callWebServiceToGetNotificationCount(tabBarController: tab, index: "")
            }

        }

        /*
         =>When user add post
        ->when tag user :- tagUser
        ->when tag hashtag user :- hashTagUser

        =>when user like post :- postLike

        =>when connection request comes :- pendingRequest

        =>when connection request accept :- acceptRequest

        =>when send message :- sendMessage
         */

        // Change this to your preferred presentation option
//        completionHandler(UNNotificationPresentationOptions.sound)
        completionHandler([.alert, .badge, .sound])

    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                   didReceive response: UNNotificationResponse,
                                   withCompletionHandler completionHandler: @escaping () -> Void) {
           let userInfo = response.notification.request.content.userInfo
           
           
           // Print full message.
           print("tap on on forground app",userInfo)
           
        
           completionHandler()
       }
}
