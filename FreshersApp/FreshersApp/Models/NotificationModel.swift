//
//  NotificationModel.swift
//  FreshersApp
//
//  Created by Apple on 14/05/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class NotificationModel: NSObject {
    var strDate = ""
    var strDesc = ""
    var strID = ""
    var strImageUrl = ""
    var isRead = false
    var strType = ""
    var isInLocale = false
    var strPostID = ""
    var strCreatedAt = ""
    var strPostExpired = false
    var expandRow = false
}
