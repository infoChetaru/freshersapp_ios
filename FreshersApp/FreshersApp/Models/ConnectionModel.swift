//
//  ConnectionModel.swift
//  FreshersApp
//
//  Created by Apple on 18/06/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class ConnectionModel: NSObject {

    var strConnectionId = ""
    var strCreatedAt = ""
    var strFileType = ""
    var strLastMessage = ""
    var strLastMsgDate = ""
    var strUserID = ""
    var strUserImgUrl = ""
    var strUserName = ""
    var strMessageCount = ""
    var strUpdatedAt = ""

}
