//
//  MessageModel.swift
//  FreshersApp
//
//  Created by Apple on 22/06/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class MessageModel: NSObject {

    var strConnectionID = ""
    var strCreatedAt = ""
    var strFileType = ""
    var strMessage = ""
    var strMessageID = ""
    var strUserID = ""
    var strUserImage = ""
    var strUserName = ""
    var strDate = ""
    var strdSectionDate = ""

}
