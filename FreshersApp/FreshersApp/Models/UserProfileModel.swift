//
//  UserProfileModel.swift
//  FreshersApp
//
//  Created by Apple on 16/06/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class UserProfileModel: NSObject {

    var strBio = ""
    var strDob = ""
    var strGenderID = ""
    var strID = ""
    var strName = ""
    var strProfileImage = ""
    var strRole = ""
    var strToken = ""
    var strType = ""
    var strUsername = ""
    var strTopPostID = ""
    var strTopPostImgUrl = ""
    var strTopPostLikeCount = ""
    var isPremiumUser = false
    var strConnectionStatus = ""
    var strLocaleName = ""
    var strLocaleID = ""

}
