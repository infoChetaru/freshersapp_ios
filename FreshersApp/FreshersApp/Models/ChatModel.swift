//
//  ChatModel.swift
//  FreshersApp
//
//  Created by Apple on 03/07/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class ChatModel: NSObject {

    var strConnectionID = ""
    var strSectionDate = ""
    var arrMessages = [MessageModel]()
}
