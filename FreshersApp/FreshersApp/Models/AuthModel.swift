//
//  AuthModel.swift
//  FreshersApp
//
//  Created by Apple on 09/04/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class AuthModel: NSObject {

    static let sharedInstance = AuthModel()

    var strBio = ""
    var strCountryID = ""
    var strDob = ""
    var strLocaleID = ""
    var strGenderID = ""
    var strID = ""
    var strName = ""
    var strProfileImage = ""
    var strRole = ""
    var strToken = ""
    var strType = ""
    var strUsername = ""
    var strTopPostID = ""
    var strTopPostImgUrl = ""
    var strTopPostLikeCount = ""
    var isPremiumUser = false
    var strLocaleName = ""
    var strFcmId = ""
    var strExpiryDate = ""
    var strSubscriptionDate = ""
    var strSensitivePostAllowed = ""
}
