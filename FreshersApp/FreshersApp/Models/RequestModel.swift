//
//  RequestModel.swift
//  FreshersApp
//
//  Created by Apple on 18/06/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class RequestModel: NSObject {

    var strID = ""
    var strImageUrl = ""
    var strName = ""
    var strOpeningMessage = ""
    var strUpdatedAt = ""
    var strUserID = ""

}
