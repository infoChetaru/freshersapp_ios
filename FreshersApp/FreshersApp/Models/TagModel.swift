//
//  TagModel.swift
//  FreshersApp
//
//  Created by Apple on 17/04/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class TagModel: NSObject {

    var strID = "-1"
    var strName = ""
    var strImgUrl = ""
    var isSelected = false
    var strUserConnectStatus = ""
    var strUserConnectionID = ""
}
