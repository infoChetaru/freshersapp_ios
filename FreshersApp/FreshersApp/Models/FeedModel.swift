//
//  FeedModel.swift
//  FreshersApp
//
//  Created by Apple on 05/05/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class FeedModel: NSObject {

    var strCaption = ""
    var arrHashTag = [TagModel]()
    var strID = ""
    var strImgUrl = ""
    var strUsername = ""
    var arrUserTag = [TagModel]()
    var isLiked = false
    var strUserConnectStatus = ""
    var strUserID = ""
    var strLikeCount = ""
    var strDate = ""
    var strUserImage = ""
    var strRoleID = ""
//    var isUserConnected = false
    var strConnectionID = ""
    var strSensitivePost = ""
    var strOriginalPostID = ""
    var expiredPost = false
    var strOriginalPostUserID = ""

}
