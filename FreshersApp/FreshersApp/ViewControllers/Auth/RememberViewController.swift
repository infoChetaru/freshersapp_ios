//
//  RememberViewController.swift
//  FreshersApp
//
//  Created by Apple on 27/08/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

class RememberViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var tblRemember: UITableView!
    @IBOutlet weak var heightOfView: NSLayoutConstraint!

    //MARK: - Variables
    var arrRemember = [String]()
    let panel = JKNotificationPanel()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        arrRemember = UserDefaults.standard.stringArray(forKey: kRememberArray) ?? [String]()
        
        if arrRemember.count > 5 {
            heightOfView.constant = CGFloat(5 * 70)
        }
        else {
            heightOfView.constant = CGFloat((arrRemember.count) * 70)
        }
        
        self.tblRemember.tableFooterView = UIView()
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnSignUpAction(_ sender : UIButton) {
         let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerifyViewController") as!  VerifyViewController
        self.navigationController?.pushViewController(objVC, animated: true)
    }

    @IBAction func btnSwitchAccountAction(_ sender : UIButton) {
         let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as!  LoginViewController
        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    @objc func btnRemoveAction(sender: UIButton) {
        arrRemember.remove(at: sender.tag)
        UserDefaults.standard.set(arrRemember, forKey: kRememberArray)
        tblRemember.reloadData()
        if arrRemember.count > 5 {
            heightOfView.constant = CGFloat(5 * 70)
        }
        else {
            heightOfView.constant = CGFloat((arrRemember.count) * 70)
        }
    }

    //MARK: - TableView data source and delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrRemember.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UserTableViewCell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell") as! UserTableViewCell
        
        cell.selectionStyle = .none
        cell.lblName.text = arrRemember[indexPath.row].components(separatedBy: ",")[1]
        cell.imgUser.kf.setImage(with: URL(string: arrRemember[indexPath.row].components(separatedBy: ",")[0]), placeholder:#imageLiteral(resourceName: "Default_Profile"))
        cell.btnRemove.tag = indexPath.row
        cell.btnRemove.addTarget(self, action: #selector(btnRemoveAction(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        callWebServiceForLogin(username: arrRemember[indexPath.row].components(separatedBy: ",")[1], password: arrRemember[indexPath.row].components(separatedBy: ",")[2])
    }
    
    //MARK: - WebService Methods
    func callWebServiceForLogin(username: String, password: String) {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [
            "userName":username,
            "password":password,
            "role":"2",
            "deviceId": UIDevice.current.identifierForVendor?.uuidString ?? "",
            "fcmToken": UserDefaults.standard.value(forKey: "fcm_token") == nil ? "" : (UserDefaults.standard.value(forKey: "fcm_token") as! String)
        ]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.login, param: param, withHeader: false ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    AuthParser.parseAuthenticationDetails(response: response!) { (model) in
                        let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "MainTab")
                        self.navigationController?.pushViewController(objVC, animated: true)
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
