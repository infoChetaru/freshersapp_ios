//
//  CountryCodeTableViewCell.swift
//  FreshersApp
//
//  Created by Apple on 07/04/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class CountryCodeTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var lblCountryName: UILabel!
    @IBOutlet weak var lblCountryCode: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
