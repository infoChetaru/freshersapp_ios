//
//  VerifyViewController.swift
//  FreshersApp
//
//  Created by Apple on 06/04/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

class VerifyViewController: UIViewController, UITextFieldDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtVerificationCode: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    @IBOutlet weak var phoneNumberView: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var btnCountryCode: UIButton!

    @IBOutlet weak var verificationCodeVerticalSpace: NSLayoutConstraint!
    @IBOutlet weak var verificationCodeHeight: NSLayoutConstraint!
    @IBOutlet weak var passwordVerticalSpace: NSLayoutConstraint!
    @IBOutlet weak var passwordHeight: NSLayoutConstraint!
    @IBOutlet weak var confirmPasswordVerticalSpace: NSLayoutConstraint!
    @IBOutlet weak var confirmPasswordHeight: NSLayoutConstraint!
    @IBOutlet weak var ORTopVerticalSpace: NSLayoutConstraint!
    @IBOutlet weak var ORBottomVerticalSpace: NSLayoutConstraint!
    @IBOutlet weak var ORHeight: NSLayoutConstraint!
    @IBOutlet weak var emailHeight: NSLayoutConstraint!
    @IBOutlet weak var phoneViewHeight: NSLayoutConstraint!

    //MARK: - Variables
    let panel = JKNotificationPanel()
    var isEmail = false
    var selectedCountryCode = CountryCodeModel()
    var verificationCode = ""
    var arrCountryCodes = [CountryCodeModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.lblCountryCode.text = "+ " + self.selectedCountryCode.strCountryCode
    }
            
    //MARK: - Custom Methods
    /**
     * Set ups the UI
     */
    func setupData() {
        
        //hide navigation bar
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        //hides keyboard when user tap on screen
        self.hideKeyboardWhenTappedAround()
        
        //set leftimage of both textfields
        txtEmail.setIcon(#imageLiteral(resourceName: "EmailPhone number"))
//        txtPhone.setIcon(#imageLiteral(resourceName: "EmailPhone number"))
        txtVerificationCode.setIcon(#imageLiteral(resourceName: "Verificationcode"))
        txtPassword.setIcon(#imageLiteral(resourceName: "password"))
        txtConfirmPassword.setIcon(#imageLiteral(resourceName: "password"))
        
        txtEmail.delegate = self
        txtPhone.delegate = self
        
//        callWebServiceForCountryCode()
        let model = CountryCodeModel()
        
        model.strID = "225"
        model.strCountryName = "United Kingdom"
        model.strCountryCode = "44"

        self.selectedCountryCode = model
        self.lblCountryCode.text = "+ " + self.selectedCountryCode.strCountryCode

    }
    
    func refreshCountryCode() {
        self.lblCountryCode.text = "+ " + self.selectedCountryCode.strCountryCode
    }
        
    //MARK: - UITextField Methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtEmail {
            txtPhone.text = ""
//            lblCountryCode.text = "+ "
            self.lblCountryCode.text = "+ " + self.selectedCountryCode.strCountryCode
            phoneNumberView.backgroundColor = UIColor.lightGray
            txtEmail.backgroundColor = UIColor.white
            isEmail = true
        }
        else if textField == txtPhone {
            txtEmail.text = ""
            txtEmail.backgroundColor = UIColor.lightGray
            phoneNumberView.backgroundColor = UIColor.white
            isEmail = false
        }
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnBackAction(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnCountryCodeAction(_ sender : UIButton) {
        
//        return
            
        txtEmail.text = ""
        txtEmail.backgroundColor = UIColor.lightGray
        phoneNumberView.backgroundColor = UIColor.white
        isEmail = false

         let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CountryCodeListViewController") as!  CountryCodeListViewController
        objVC.modalPresentationStyle = .overCurrentContext
        objVC.arrCountryCodes = self.arrCountryCodes
        objVC.arrFilteredCountryCodes = self.arrCountryCodes
        self.navigationController?.present(objVC, animated: true, completion: nil)
    }

    @IBAction func btnNextAction(_ sender : UIButton) {
        
        if verificationCodeHeight.constant == 0 && passwordHeight.constant == 0 {
            
            //case when we screen displays Email or phone number
            if (txtEmail.text?.isEmpty)! && (txtPhone.text?.isEmpty)! {
                panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyPhoneNumberEmail)
            }
            else if isEmail {
                if (txtEmail.text?.isEmpty)! {
                    panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyEmail)
                }
                else if (!(txtEmail.text?.isValidEmail())!) {
                    panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.validEmail)
                }
                else {
                    
                    callWebServiceToVerifyCode()
                }
            }
            else {
                if (txtPhone.text?.isEmpty)! {
                    panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyPhoneNumber)
                }
                else if lblCountryCode.text == "+ " {
                    panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.validCountryCode)
                }
                else if !(txtPhone.text!.isPhoneNumber) {
                    panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.validPhoneNumber)
                }
                else {
                    callWebServiceToVerifyCode()
                }
            }
        }
        else if verificationCodeHeight.constant != 0 && passwordHeight.constant == 0 {
            
            //case when we screen displays verification code

            if self.verificationCode != txtVerificationCode.text {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.validCode)
            }
            else {
                self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: StringConstant.successVerifiedCode)

                verificationCodeVerticalSpace.constant = 0
                verificationCodeHeight.constant = 0
                passwordHeight.constant = 50
                passwordVerticalSpace.constant = 30
                confirmPasswordHeight.constant = 50
                confirmPasswordVerticalSpace.constant = 30
            }
        }
        else {
            //case when we screen displays password
            
            if (txtPassword.text?.isEmpty)! {
                panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyPassword)
            }
            else if (txtConfirmPassword.text?.isEmpty)! {
                panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyConfirmPassword)
            }
            else if (txtPassword.text != txtConfirmPassword.text){
                panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.mismatchPasswordConfirmPassword)
            }
            else {
                 let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as!  SignUpViewController
                objVC.strUsername = (isEmail ? txtEmail.text : txtPhone.text)!
                objVC.strPassword = txtPassword.text!
                objVC.strType = isEmail ? "1" : "2"
                objVC.strCountryID = isEmail ? "" : selectedCountryCode.strID
                self.navigationController?.pushViewController(objVC, animated: true)
            }
        }
    }
    
    //MARK: - WebService Methods
    func callWebServiceToVerifyCode() {
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        var value = ""
        if isEmail {
            value = txtEmail.text ?? ""
        }
        else {
            value = txtPhone.text ?? ""

            if txtPhone.text?.first == "0" {
                value.remove(at: value.startIndex)
            }
        }
        
        let param = [
            "type": isEmail ? "1" : "2",
            "value": value, //isEmail ? txtEmail.text ?? "" : txtPhone.text ?? "",
            "countryId": isEmail ? "" : selectedCountryCode.strID
        ]
        
        print(param) 
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.verifyCode, param: param, withHeader: false ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: response!["message"].stringValue)

                    self.verificationCode = response!["data"]["randomNumber"].stringValue
                    
                    if self.isEmail {
                        self.verificationCodeHeight.constant = 50
                        self.verificationCodeVerticalSpace.constant = 30
                        self.ORHeight.constant = 0
                        self.ORTopVerticalSpace.constant = 0
                        self.ORBottomVerticalSpace.constant = 0
                        self.phoneViewHeight.constant = 0
                        self.txtEmail.isEnabled = false
                    }
                    else {
                        self.verificationCodeHeight.constant = 50
                        self.verificationCodeVerticalSpace.constant = 30
                        self.ORHeight.constant = 0
                        self.ORTopVerticalSpace.constant = 0
                        self.ORBottomVerticalSpace.constant = 0
                        self.emailHeight.constant = 0
                        self.txtPhone.isEnabled = false
                        self.btnCountryCode.isEnabled = false
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForCountryCode() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.countryCodeList, param: [:], withHeader: false ) { (response, errorMsg) in

            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {

                    AuthParser.parseCountryCodeList(response: response!) { (arr) in
                        self.arrCountryCodes = arr
                        
                        for value in arr {
//                            if value.strCountryName == Locale.current.localizedString(forRegionCode: Locale.current.regionCode!) {
  
                            if value.strCountryName == "United Kingdom" {

                                self.selectedCountryCode = value
                                self.lblCountryCode.text = "+ " + self.selectedCountryCode.strCountryCode
                            }
                        }
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
