//
//  InitialViewController.swift
//  FreshersApp
//
//  Created by Apple on 16/04/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        //hide navigation bar
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnLoginAction(_ sender : UIButton) {
        
        let rememberArray = UserDefaults.standard.stringArray(forKey: kRememberArray) ?? [String]()
        
        if rememberArray.count > 0 {
             let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RememberViewController") as!  RememberViewController
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        else {
             let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as!  LoginViewController
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    @IBAction func btnSignUpAction(_ sender : UIButton) {
          let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerifyViewController") as!  VerifyViewController
         self.navigationController?.pushViewController(objVC, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
