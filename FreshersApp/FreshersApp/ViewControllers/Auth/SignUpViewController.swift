//
//  SignUpViewController.swift
//  FreshersApp
//
//  Created by Apple on 06/04/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

class SignUpViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtDob: UITextField!
    @IBOutlet weak var txtBio: UITextView!
    @IBOutlet weak var imgMale: UIImageView!
    @IBOutlet weak var imgFemale: UIImageView!
    @IBOutlet weak var imgOther: UIImageView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var imgSensitiveCheckbox: UIImageView!

    //MARK: - Variable
    var imagePicker = UIImagePickerController()
    let panel = JKNotificationPanel()
    var strPassword = ""
    var strType = ""
    var strUsername = ""
    var strCountryID = ""
    var objAuthModel = AuthModel()
    var strGender = kGenderMale
    var strSesitive = kNotAllowSensitive

    override func viewDidLoad() {
        super.viewDidLoad()

        setupData()
    }
    
    //MARK: - Custom Methods
    /**
     * Set ups the UI
     */
    func setupData() {
        
        //hide navigation bar
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        //hides keyboard when user tap on screen
        self.hideKeyboardWhenTappedAround()
        
        //set leftimage of both textfields
        txtUsername.setIcon(#imageLiteral(resourceName: "username"))
        txtDob.setIcon(#imageLiteral(resourceName: "Dateofbirth"))
        txtFullName.setIcon(#imageLiteral(resourceName: "username"))

        imagePicker.delegate = self
        setSelectedGender()
        
        txtUsername.text = strUsername
    }
    
    func setSelectedGender() {
        
        switch strGender {
        case kGenderMale:
            imgMale.image = #imageLiteral(resourceName: "male_selected")
            imgFemale.image = #imageLiteral(resourceName: "female_not_selected")
            imgOther.image = #imageLiteral(resourceName: "Other_not_selected")
        case kGenderFemale:
            imgMale.image = #imageLiteral(resourceName: "male_not_selected")
            imgFemale.image = #imageLiteral(resourceName: "female_selected")
            imgOther.image = #imageLiteral(resourceName: "Other_not_selected")
        case kGenderOther:
            imgMale.image = #imageLiteral(resourceName: "male_not_selected")
            imgFemale.image = #imageLiteral(resourceName: "female_not_selected")
            imgOther.image = #imageLiteral(resourceName: "Other_selected")
            
        default:
            imgMale.image = #imageLiteral(resourceName: "male_not_selected")
            imgFemale.image = #imageLiteral(resourceName: "female_not_selected")
            imgOther.image = #imageLiteral(resourceName: "Other_not_selected")
        }
    }
    
    /**
     This function opens the camera of device
     */
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            //case when device has a camera
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            //present camera
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            //case when device has no camera
            let alert  = UIAlertController(title: StringConstant.titleWarning, message: StringConstant.titleNoCamera, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: StringConstant.titleOK, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     This function opens the gallery of device
     */
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        imagePicker.allowsEditing = true
        //present gallery
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    /**
     This function converts images into base64 and keep them into string
     */
    func convertImageToBase64(image: UIImage) -> String {
        var imgData = UIImageJPEGRepresentation(imgProfile.image!, 1.0)!
        if let imageData = imgProfile.image!.jpeg(.medium) {
            print(imageData.count)
            imgData = imageData
        }
        return imgData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnCameraAction(_ sender : UIButton) {
        //create alert view for choice of Camera or Gallery
        let alert = UIAlertController(title: StringConstant.titleSelectOptionMedia, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: StringConstant.titleCamera, style: .default, handler: { _ in
            //open camera
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: StringConstant.titleGallery, style: .default, handler: { _ in
            //open gallery
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: StringConstant.titleCancel, style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func btnMaleAction(_ sender : UIButton) {
        strGender = kGenderMale
        setSelectedGender()
    }
    
    @IBAction func btnFemaleAction(_ sender : UIButton) {
        strGender = kGenderFemale
        setSelectedGender()
    }

    @IBAction func btnOtherAction(_ sender : UIButton) {
        strGender = kGenderOther
        setSelectedGender()
    }

    @IBAction func btnNextAction(_ sender : UIButton) {

        if (txtFullName.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyUsername)
        }
        else if (txtDob.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyDOB)
        }
        else if (txtBio.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyBio)
        }
        else {
            callWebServiceForSignup()
        }
    }
    
    @IBAction func btnViewSensitiveDataAction(_ sender : UIButton) {
        if strSesitive == kAllowSensitive {
            strSesitive = kNotAllowSensitive
            imgSensitiveCheckbox.image = #imageLiteral(resourceName: "uncheck")
        }
        else {
            strSesitive = kAllowSensitive
            imgSensitiveCheckbox.image = #imageLiteral(resourceName: "check-purple")
        }
    }
    
    @IBAction func btnTermsAndConditionsAction(_ sender : UIButton) {
        guard let url = URL(string: kTermsOfServicesURL) else { return }
        UIApplication.shared.openURL(url)
    }
    
    @IBAction func btnPrivacyPolicyAction(_ sender : UIButton) {
        guard let url = URL(string: kPrivacyPolicyURL) else { return }
        UIApplication.shared.openURL(url)
    }

    // MARK: - UIImagePickerControllerDelegate Methods
    /**
     This is pickercontroller delegate which is called when user selects any image from gallery/or saves any camera image
     */
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            //get the original image & set it as feedback image
            imgProfile.image = image
            self.dismiss(animated: false, completion: nil)
        }
        else{
            //case when image is not supported
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.titleNoSupportedImage)
            picker.popViewController(animated: true)
            print("Something went wrong in  image")
        }
    }
    
    /**
     This is pickercontroller delegate which is called when user cancel the image picker view
     */
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }

    //MARK: - WebService Methods
    func callWebServiceForSignup() {
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [
            "name":txtFullName.text ?? "",
            "userName":txtUsername.text ?? "",
            "password":strPassword,
            "dob":txtDob.text ?? "",
            "bio":txtBio.text ?? "",
            "gender": strGender,
            "type": strType,
            "countryId": strCountryID,
            "sensitivePostsAllowed": strSesitive,
            "profileImage": self.convertImageToBase64(image: imgProfile.image!),
            "deviceId": UIDevice.current.identifierForVendor?.uuidString ?? "",
            "fcmToken": UserDefaults.standard.value(forKey: "fcm_token") == nil ? "" : (UserDefaults.standard.value(forKey: "fcm_token") as! String)
        ]
        
        let paramToPrint = [
            "name":txtFullName.text ?? "",
            "userName":txtUsername.text ?? "",
            "password":strPassword,
            "dob":txtDob.text ?? "",
            "bio":txtBio.text ?? "",
            "gender": strGender,
            "type": strType,
            "countryId": strCountryID,
            "sensitivePostsAllowed": strSesitive,
            "profileImage": "",
            "deviceId": UIDevice.current.identifierForVendor?.uuidString ?? "",
            "fcmToken": UserDefaults.standard.value(forKey: "fcm_token") == nil ? "" : (UserDefaults.standard.value(forKey: "fcm_token") as! String)
        ]
        
        print(paramToPrint)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.signup, param: param, withHeader: false ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    AuthParser.parseAuthenticationDetails(response: response!) { (model) in
                        
                         let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "MainTab")
                        self.navigationController?.pushViewController(objVC, animated: true)

//                        let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogoutViewController") as!  LogoutViewController
//                        self.navigationController?.pushViewController(objVC, animated: true)
                    }
                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
