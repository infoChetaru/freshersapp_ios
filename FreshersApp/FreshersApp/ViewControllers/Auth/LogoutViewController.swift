//
//  LogoutViewController.swift
//  FreshersApp
//
//  Created by Apple on 09/04/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class LogoutViewController: UIViewController {

    //MARK: - Variables
    let panel = JKNotificationPanel()

    override func viewDidLoad() {
        super.viewDidLoad()

        //hide navigation bar
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    //MARK: - Custom Methods
    func logout() {
        let strFcmToken = UserDefaults.standard.value(forKey: "fcm_token") as! String
        let defaults = UserDefaults.standard
        defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.set(strFcmToken, forKey: "fcm_token")

        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let centerVC = mainStoryBoard.instantiateViewController(withIdentifier: "LoginNav")
        appDel.window!.rootViewController = centerVC
        appDel.window!.makeKeyAndVisible()

    }
    
    //MARK: - IBAction Methods
    @IBAction func btnLogoutAction(_ sender : UIButton) {
        let alert = UIAlertController(title: nil, message: StringConstant.titleSureLogout, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: StringConstant.titleYes, style: .default, handler: { (action) in
            self.callWebServiceForLogout()
        }))
        alert.addAction(UIAlertAction(title: StringConstant.titleNo, style: .cancel, handler: { (action) in
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - WebService Methods
    func callWebServiceForLogout() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.logout, param: [:], withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    self.logout()
                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
