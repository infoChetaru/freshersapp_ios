//
//  LoginViewController.swift
//  FreshersApp
//
//  Created by Apple on 03/04/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

class LoginViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!

    //MARK: - Variables
    let panel = JKNotificationPanel()

    override func viewDidLoad() {
        super.viewDidLoad()

        //setup ui
        setupData()
    }
    
    //MARK: - Custom Methods
    /**
     * Set ups the UI
     */
    func setupData() {
        
        //hide navigation bar
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        //hide keyboard on tuch on screen
        self.hideKeyboardWhenTappedAround()
        
        //set leftimage of both textfields
        txtUsername.setIcon(#imageLiteral(resourceName: "username"))
        txtPassword.setIcon(#imageLiteral(resourceName: "password"))
        
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnBackAction(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnNextAction(_ sender : UIButton) {
        
        if (txtUsername.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyPhoneNumberEmail)
        }
        else if (txtPassword.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyPassword)
        }
        else {
            callWebServiceForLogin()
        }
    }

    @IBAction func btnSignUpAction(_ sender : UIButton) {
          let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VerifyViewController") as!  VerifyViewController
         self.navigationController?.pushViewController(objVC, animated: true)
    }

    @IBAction func btnForgotPasswordAction(_ sender : UIButton) {
          let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForgotPasswordViewController") as!  ForgotPasswordViewController
         self.navigationController?.pushViewController(objVC, animated: true)
    }

    //MARK: - WebService Methods
    func callWebServiceForLogin() {        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [
            "userName":txtUsername.text ?? "",
            "password":txtPassword.text ?? "",
            "role":"2",
            "deviceId": UIDevice.current.identifierForVendor?.uuidString ?? "",
            "fcmToken": UserDefaults.standard.value(forKey: "fcm_token") == nil ? "" : (UserDefaults.standard.value(forKey: "fcm_token") as! String)
        ]
           
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.login, param: param, withHeader: false ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    
                    AuthParser.parseAuthenticationDetails(response: response!) { (model) in
                         let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "MainTab")
                        self.navigationController?.pushViewController(objVC, animated: true)

//                          let objVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LogoutViewController") as!  LogoutViewController
//                         self.navigationController?.pushViewController(objVC, animated: true)

                        let strProfile = model.strProfileImage + ","
                        
                        let strUsername = self.txtUsername.text! + ","

                        let strPassword = self.txtPassword.text
                        
                        let strRem = strProfile + strUsername + strPassword!

                        var rememberArray = UserDefaults.standard.stringArray(forKey: kRememberArray) ?? [String]()

                        if !rememberArray.contains(strRem) {
                            rememberArray.append(strRem)
                            UserDefaults.standard.set(rememberArray, forKey: kRememberArray)
                        }
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                    self.txtPassword.text = ""
                }
            }
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
