//
//  ForgotPasswordViewController.swift
//  FreshersApp
//
//  Created by Apple on 07/04/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

class ForgotPasswordViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtVerificationCode: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!

    @IBOutlet weak var verificationCodeVerticalSpace: NSLayoutConstraint!
    @IBOutlet weak var verificationCodeHeight: NSLayoutConstraint!
    @IBOutlet weak var newPasswordVerticalSpace: NSLayoutConstraint!
    @IBOutlet weak var newPasswordHeight: NSLayoutConstraint!
    @IBOutlet weak var confirmPasswordVerticalSpace: NSLayoutConstraint!
    @IBOutlet weak var confirmPasswordHeight: NSLayoutConstraint!

    //MARK: - Variables
    let panel = JKNotificationPanel()
    var strVerificationCode = ""
    var strToken = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        setupData()
    }
    
    //MARK: - Custom Methods
    /**
     * Set ups the UI
     */
    func setupData() {
                
        //set leftimage of both textfields
        txtUsername.setIcon(#imageLiteral(resourceName: "username"))
        txtVerificationCode.setIcon(#imageLiteral(resourceName: "Verificationcode"))
        txtPassword.setIcon(#imageLiteral(resourceName: "password"))
        txtConfirmPassword.setIcon(#imageLiteral(resourceName: "password"))

    }
    //MARK: - IBAction Methods
    @IBAction func btnNextAction(_ sender : UIButton) {
        
        if (txtUsername.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyPhoneNumberEmail)
        }
        else {
            if verificationCodeHeight.constant != 0 {
                if strVerificationCode == txtVerificationCode.text {
                    newPasswordHeight.constant = 50
                    newPasswordVerticalSpace.constant = 30
                    confirmPasswordHeight.constant = 50
                    confirmPasswordVerticalSpace.constant = 30
                    verificationCodeVerticalSpace.constant = 0
                    verificationCodeHeight.constant = 0
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.validCode)
                }
            }
            else if newPasswordHeight.constant != 0 {
                if (txtPassword.text?.isEmpty)! {
                    panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyPassword)
                }
                else if (txtConfirmPassword.text?.isEmpty)! {
                    panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyConfirmPassword)
                }
                else if (txtPassword.text != txtConfirmPassword.text){
                    panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.mismatchPasswordConfirmPassword)
                }
                else {
                    callWebServiceToResetpassword()
                }
            }
            else {
                callWebServiceForForgotpassword()
            }
        }
    }
    
    @IBAction func btnBackAction(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - WebService Methods
    func callWebServiceForForgotpassword() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        //set param
        let param = ["userName": txtUsername.text!  ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.forgotpassword, param: param, withHeader: false ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    //show success message when email/verification code is sent successfully
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: response!["message"].stringValue)
                    
                    if (response!["data"]["type"].stringValue == kPhoneUsername) {
                        
                        self.strVerificationCode = response!["data"]["randomNumber"].stringValue
                        self.strToken = response!["data"]["token"].stringValue
                        self.txtUsername.tintColor = UIColor.lightGray
                        self.txtUsername.textColor = UIColor.lightGray
                        self.txtUsername.isEnabled = false
                        
                        self.verificationCodeHeight.constant = 50
                        self.verificationCodeVerticalSpace.constant = 30
                    }
                }
                else {
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToResetpassword() {
        
        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        //set param
        let param = ["token": strToken,
                     "userName": txtUsername.text ?? "",
                     "password": txtPassword.text ?? ""
            ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.resetPassword, param: param, withHeader: false ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    //show success message when email/verification code is sent successfully
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: response!["message"].stringValue)
                    
                    self.navigationController?.popViewController(animated: true)

                }
                else {
                    //show error message in case of no response
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
