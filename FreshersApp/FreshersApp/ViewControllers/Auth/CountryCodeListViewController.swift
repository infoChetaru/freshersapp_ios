//
//  CountryCodeListViewController.swift
//  FreshersApp
//
//  Created by Apple on 07/04/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class CountryCodeListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var tblCountryCode: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!

    //MARK: - Variables
    let panel = JKNotificationPanel()
    var arrCountryCodes = [CountryCodeModel]()
    var arrFilteredCountryCodes = [CountryCodeModel]()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupData()
    }
    
    //MARK: - Custom Methods
    /**
     * Set ups the UI
     */
    func setupData() {
        searchBar.delegate = self

        tblCountryCode.tableFooterView = UIView()
        
        if arrCountryCodes.count == 0 {
            callWebServiceForCountryCode()
        }
    }
    
    //MARK: - UISearchBar Delegates
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        
        arrFilteredCountryCodes = searchText.isEmpty ? arrCountryCodes : arrCountryCodes.filter { (item: CountryCodeModel) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.strCountryName.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }

        tblCountryCode.reloadData()
    }

    //MARK: - IBAction Methods
    @IBAction func btnCancelAction(_ sender : UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - TableView data source and delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrFilteredCountryCodes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : CountryCodeTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CountryCodeTableViewCell") as! CountryCodeTableViewCell
        
        cell.lblCountryName.text = arrFilteredCountryCodes[indexPath.row].strCountryName
        cell.lblCountryCode.text = arrFilteredCountryCodes[indexPath.row].strCountryCode
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let navController = presentingViewController as? UINavigationController {
           let presenter = navController.topViewController as! VerifyViewController
            presenter.selectedCountryCode = self.arrFilteredCountryCodes[indexPath.row]
            presenter.refreshCountryCode()
        }
        dismiss(animated: true, completion: nil)
    }
    
    func callWebServiceForCountryCode() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData())
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.countryCodeList, param: [:], withHeader: false ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    AuthParser.parseCountryCodeList(response: response!) { (arr) in
                        self.arrCountryCodes = arr
                        self.arrFilteredCountryCodes = arr
                        
                        self.tblCountryCode.reloadData()
                        
                        /*for value in arr {
                            //                            if value.strCountryName == Locale.current.localizedString(forRegionCode: Locale.current.regionCode!) {
                            
                            if value.strCountryName == "United Kingdom" {
                                
                                self.selectedCountryCode = value
                                self.lblCountryCode.text = "+ " + self.selectedCountryCode.strCountryCode
                            }
                        }*/
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
