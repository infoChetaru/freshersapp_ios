//
//  ConnectionViewController.swift
//  FreshersApp
//
//  Created by Apple on 18/06/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class ConnectionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var tblConnections: UITableView!
    @IBOutlet weak var viewNoRecord: UIView!

    //MARK: - Variables
    let panel = JKNotificationPanel()
    var arrConnections = [ConnectionModel]()
    var currentPage = 1
    var totalPageCount = 1
    var isLoadMore = true
    var refresher:UIRefreshControl!
    var selectedConnection = ConnectionModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(loadData),
            name: NSNotification.Name(rawValue: "ChatPush"),
            object: nil)
        
        loadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name("ChatPush"), object: nil)
    }
    
    //MARK: - Custom Methods
    /**
     * Set ups the UI
     */
    func setupData() {
        tblConnections.tableFooterView = UIView()
        
        self.refresher = UIRefreshControl()
        self.tblConnections!.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor.white
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.tblConnections!.addSubview(refresher)

        currentPage = 1
//        callWebServiceForConnectionList(showLoader: arrConnections.count > 0 ? false : true, page: currentPage)
    }
    
    @objc func loadData() {
        refresher.beginRefreshing()
        stopRefresher()
        
        if arrConnections.count == 0 {
            callWebServiceForConnectionList(showLoader: false, page: 1)
        }
        else
        {
            callWebServiceForConnectionList(showLoader: false, page: 0)
        }
     }

    func stopRefresher() {
        refresher.endRefreshing()
     }
    
    func removeObjectWith(objects: [ConnectionModel]) {
        
        var arrIndexToDelete = [Int]()
        
        for (index, valueMain) in arrConnections.enumerated() {
            for value in objects {
                if value.strConnectionId == valueMain.strConnectionId {
                    arrIndexToDelete.append(index)
                }
            }
        }

        arrConnections.remove(at: arrIndexToDelete)
    }
    
    func removeObjectWith(Ids: [String]) {
        
        var arrIndexToDelete = [Int]()
        
        for (index, valueMain) in arrConnections.enumerated() {
            for value in Ids {
                if valueMain.strConnectionId == value {
                    arrIndexToDelete.append(index)
                }
            }
        }

        arrConnections.remove(at: arrIndexToDelete)
    }
    
    func removeObjectWithUser(Ids: [String]) {
        
        var arrIndexToDelete = [Int]()
        
        for (index, valueMain) in arrConnections.enumerated() {
            for value in Ids {
                if valueMain.strUserID == value {
                    arrIndexToDelete.append(index)
                }
            }
        }

        arrConnections.remove(at: arrIndexToDelete)
    }


    
    //MARK: - TableView data source and delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrConnections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : ConnectionsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ConnectionsTableViewCell") as! ConnectionsTableViewCell
        
        cell.selectionStyle = .none
        
        cell.imgView!.kf.setImage(with: URL(string: arrConnections[indexPath.row].strUserImgUrl), placeholder: nil)
        cell.lblName.text = arrConnections[indexPath.row].strUserName
        cell.lblDate.text = arrConnections[indexPath.row].strLastMsgDate

        if arrConnections[indexPath.row].strFileType == "1" {
            cell.lblDesc.text = arrConnections[indexPath.row].strLastMessage
            cell.photoView.isHidden = true
            cell.lblDesc.isHidden = false
        }
        else if arrConnections[indexPath.row].strFileType == "2" {
            cell.photoView.isHidden = false
            cell.lblDesc.isHidden = true
        }
        else {
            cell.lblDesc.text = "Say Hi to your new connection."
            cell.photoView.isHidden = true
            cell.lblDesc.isHidden = false
        }
        
        if arrConnections[indexPath.row].strMessageCount == "" || arrConnections[indexPath.row].strMessageCount == "0" {
            
            cell.lblMessageCount.isHidden = true
        }
        else {
            cell.lblMessageCount.isHidden = false
            cell.lblMessageCount.text = arrConnections[indexPath.row].strMessageCount
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        arrConnections[indexPath.row].strMessageCount = "0"
        selectedConnection = arrConnections[indexPath.row]
        tblConnections.reloadData()
        self.performSegue(withIdentifier: "toChat", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        //case of archived notification view
        if self.currentPage == self.totalPageCount {
            //case when no more items are there to load
            self.isLoadMore = false
        }
        
        //get last element index
        let lastElement = arrConnections.count - 1
        
        if indexPath.row == lastElement && isLoadMore {
            // handle your logic here to get more items, add it to dataSource and reload tableview
            
            self.currentPage = self.currentPage + 1
            callWebServiceForConnectionList(showLoader: false, page: currentPage)

        }
    }

    
    //MARK: - WebService Methods
    func callWebServiceForConnectionList(showLoader: Bool, page: Int) {
        
        if showLoader {
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        }
                
        var date = ""
        if arrConnections.count > 0 && page == 0 {
            date = arrConnections[0].strUpdatedAt
        }

        let param = [
            "page": page,
            "date": date
            ] as [String : Any]

        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Connect.connectionList, param: param, withHeader: true) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                if self.arrConnections.count == 0 {
                    self.viewNoRecord.isHidden = false
                }
            }
            else{
                if response!["status"].boolValue == true {
                    self.viewNoRecord.isHidden = true

                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    ConnectionParser.parseConnectionList(response: response!) { (arr, totalPages, arrOfUnfrindConnections, arrOfDeletedUsers) in
                        
                        if page == 1 {
                            //if first time then set array
                            self.arrConnections = arr
                        }
                        else {
                            if page == 0 {
                                self.removeObjectWith(objects: arr)
                                self.removeObjectWith(Ids: arrOfUnfrindConnections)
                                self.removeObjectWithUser(Ids: arrOfDeletedUsers)
                                self.arrConnections.insert(contentsOf: arr, at: 0)
                            }
                            else {
                                //if next pages are loaded append the arrays to previous
                                self.arrConnections.append(contentsOf: arr)
                            }
                        }
                        
                        if page != 0 {
                            //save total page count
                            self.totalPageCount = Int(totalPages)!
                        }
                        
                        if self.arrConnections.count == 0 {
                            self.viewNoRecord.isHidden = false
                        }

                        self.tblConnections.reloadData()
                    }
                    
                }
                else {
                    if self.arrConnections.count == 0 {
                        self.viewNoRecord.isHidden = false
                    }
//                    if self.currentPage == 1 {
//                        self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
//                    }
                }
            }
        }
    }


    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        let objVC = segue.destination as! ChatViewController
        objVC.objConnectionModel = selectedConnection
        self.navigationController?.present(objVC, animated: true, completion: nil)

    }
}
