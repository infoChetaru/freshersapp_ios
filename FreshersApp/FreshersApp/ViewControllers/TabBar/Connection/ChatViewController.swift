//
//  ChatViewController.swift
//  FreshersApp
//
//  Created by Apple on 19/06/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import GrowingTextView
import JKNotificationPanel
import NVActivityIndicatorView

let kImageType = "2"
let kMessageType = "1"

class ChatViewController: UIViewController, GrowingTextViewDelegate, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var tblViewChat:UITableView!
    @IBOutlet weak var txtMessage:GrowingTextView!
    @IBOutlet weak var lblUserName:UILabel!
    @IBOutlet weak var imgUser:UIImageView!
    @IBOutlet weak var bottomView:UIView!
    @IBOutlet weak var btnMore:UIButton!

    //MARK: - Variable
    let panel = JKNotificationPanel()
    var objConnectionModel = ConnectionModel()
    var currentPage = 1
    var isLoadMore = true
    var totalPageCount = 1
    var arrChatSections = [ChatModel]()
    var selectedImage = UIImage()
    var imagePicker = UIImagePickerController()
    var strImgUrlForFullScreen = ""
    var strUserConnectionStatus = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        automaticallyAdjustsScrollViewInsets = false
        setupData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name("ChatPush"), object: nil)
    }
        
    //MARK: - Custom Methods
    func setupData() {

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(refreshChatView),
            name: NSNotification.Name(rawValue: "ChatPush"),
            object: nil)

        tblViewChat.transform = CGAffineTransform(scaleX: 1, y: -1)
        imagePicker.delegate = self
        lblUserName.text = objConnectionModel.strUserName
        imgUser.kf.setImage(with: URL(string: objConnectionModel.strUserImgUrl), placeholder: nil)

        callWebServiceForMessages(showLoader: true, page: currentPage)
    }
    
    @objc func refreshChatView() {
        callWebServiceForMessages(showLoader: false, page: 0)
    }
    
    /**
     This function opens the camera of device
     */
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            //case when device has a camera
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            //present camera
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            //case when device has no camera
            let alert  = UIAlertController(title: StringConstant.titleWarning, message: StringConstant.titleNoCamera, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: StringConstant.titleOK, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     This function opens the gallery of device
     */
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        imagePicker.allowsEditing = true
        //present gallery
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    /**
     This function converts images into base64 and keep them into string
     */
    func convertImageToBase64(image: UIImage) -> String {
        var imgData = UIImageJPEGRepresentation(image, 1.0)!
        if let imageData = selectedImage.jpeg(.medium) {
            print(imageData.count)
            imgData = imageData
        }
        return imgData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    @objc func btnImgAction(_ sender: UIButton) {
        
        var superview = sender.superview
        while let view = superview, !(view is UITableViewCell) {
            superview = view.superview
        }
        guard let cell = superview as? UITableViewCell else {
            print("button is not contained in a table view cell")
            return
        }
        guard let indexPath = tblViewChat.indexPath(for: cell) else {
            print("failed to get index path for cell containing button")
            return
        }
        // We've got the index path for the cell that contains the button, now do something with it.
        print("button is in row \(indexPath.row) and section \(indexPath.section)")

        strImgUrlForFullScreen = arrChatSections[indexPath.section].arrMessages[indexPath.row].strMessage

        self.performSegue(withIdentifier: "toFullScreen", sender: nil)

    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    /**
     This is pickercontroller delegate which is called when user selects any image from gallery/or saves any camera image
     */
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            //get the original image & set it as feedback image
            selectedImage = image
            self.callWebServiceForSendMessage(type: kImageType)
            self.dismiss(animated: false, completion: nil)
        }
        else{
            //case when image is not supported
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.titleNoSupportedImage)
            picker.popViewController(animated: true)
            print("Something went wrong in  image")
        }
    }
    
    /**
     This is pickercontroller delegate which is called when user cancel the image picker view
     */
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }


    //MARK: - IBAction Methods
    @IBAction func btnCameraAction(_ sender : UIButton) {
            //create alert view for choice of Camera or Gallery
            let alert = UIAlertController(title: StringConstant.titleSelectOptionMedia, message: nil, preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: StringConstant.titleCamera, style: .default, handler: { _ in
                //open camera
                self.openCamera()
            }))
            
            alert.addAction(UIAlertAction(title: StringConstant.titleGallery, style: .default, handler: { _ in
                //open gallery
                self.openGallary()
            }))
            
            alert.addAction(UIAlertAction.init(title: StringConstant.titleCancel, style: .cancel, handler: nil))
            
            /*If you want work actionsheet on ipad
             then you have to use popoverPresentationController to present the actionsheet,
             otherwise app will crash on iPad */
            switch UIDevice.current.userInterfaceIdiom {
            case .pad:
                alert.popoverPresentationController?.sourceView = sender as? UIView
                alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
                alert.popoverPresentationController?.permittedArrowDirections = .up
            default:
                break
            }
            
            self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnSendMessageAction(_ sender : UIButton) {
        if (txtMessage.text?.isEmpty)! {
            return
        }
        else {
            self.callWebServiceForSendMessage(type: kMessageType)
        }
    }
    
    @IBAction func btnBackAction(_ sender : UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnMoreAction(_ sender : UIButton) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.addAction(UIAlertAction(title: StringConstant.titleDisconnect, style: .default, handler: { (action) in
            self.callWebServiceForConnectionStatusChage(type: kUnfriend)
        }))
        
        alert.addAction(UIAlertAction(title: StringConstant.titleBlock, style: .default, handler: { (action) in
            self.callWebServiceToBlock()
        }))

        alert.addAction(UIAlertAction(title: StringConstant.titleCancel, style: .cancel, handler: { (action) in
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - GrowingTextView Delegate
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
       UIView.animate(withDuration: 0.2) {
           self.view.layoutIfNeeded()
       }
    }
    
    //MARK: - UITableViewDelegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrChatSections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrChatSections[section].arrMessages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if arrChatSections[indexPath.section].arrMessages[indexPath.row].strFileType == kImageType {
            
            var identifier = ""
            if arrChatSections[indexPath.section].arrMessages[indexPath.row].strUserID == AuthModel.sharedInstance.strID {
                identifier = "imageMe"
            }
            else {
                identifier = "imageOther"
            }
            
            let cell : MessageTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier) as! MessageTableViewCell
            cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
            cell.selectionStyle = .none

            cell.imgMsg!.kf.setImage(with: URL(string: arrChatSections[indexPath.section].arrMessages[indexPath.row].strMessage), placeholder: nil)
            cell.lblDate.text = arrChatSections[indexPath.section].arrMessages[indexPath.row].strDate
            cell.btnImage.tag = indexPath.row
            cell.btnImage.addTarget(self, action: #selector(btnImgAction(_:)), for: .touchUpInside)

            return cell
        }
        else {
            var identifier = ""
            if arrChatSections[indexPath.section].arrMessages[indexPath.row].strUserID == AuthModel.sharedInstance.strID {
                identifier = "messageMe"
            }
            else {
                identifier = "messageOther"
            }
            
            let cell : MessageTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier) as! MessageTableViewCell
            cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
            cell.selectionStyle = .none
            
            cell.lblMessage.text = arrChatSections[indexPath.section].arrMessages[indexPath.row].strMessage
            cell.lblDate.text = arrChatSections[indexPath.section].arrMessages[indexPath.row].strDate
            
            return cell
        }        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        //case of archived notification view
        if self.currentPage == self.totalPageCount {
            //case when no more items are there to load
            self.isLoadMore = false
        }
        
        //get last element index
        let lastElement = (arrChatSections.last?.arrMessages.count)! - 1 //arrChatSections.count - 1
        
        if indexPath.row == lastElement && isLoadMore {
            // handle your logic here to get more items, add it to dataSource and reload tableview
            
            self.currentPage = self.currentPage + 1
            callWebServiceForMessages(showLoader: false, page: currentPage)
            
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        (view as! UITableViewHeaderFooterView).contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
        (view as! UITableViewHeaderFooterView).contentView.backgroundColor = ColorCodeConstant.themeColor
        (view as! UITableViewHeaderFooterView).textLabel?.textColor = UIColor.white
        (view as! UITableViewHeaderFooterView).textLabel?.textAlignment = .center

    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return arrChatSections[section].strSectionDate
    }

    //MARK: - WebService Methods
    func callWebServiceForMessages(showLoader: Bool, page: Int) {
        
        if showLoader {
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        }
                
        var date = ""
        if arrChatSections.count > 0 && (arrChatSections.first?.arrMessages.count)! > 0 {
            date = (arrChatSections.first?.arrMessages.first!.strCreatedAt)!
        }
        
        let param = [
            "connectionId": objConnectionModel.strConnectionId,
            "page": page,
            "date": date //objConnectionModel.strCreatedAt
            ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Connect.getChatMessages, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    let arrMSG = [MessageModel]()
                    ConnectionParser.parseMessageList(response: response!, arrAllMessageModel: self.arrChatSections.count > 0 ? self.arrChatSections.last!.arrMessages : arrMSG) { (arr, totalPages) in
                        
                        if page == 1 {
                            //if first time then set array
                            self.arrChatSections = arr
                        }
                        else {
                            if page == 0 {
                                
                                var found = false
                                for (_, value) in self.arrChatSections.enumerated() {
                                    if value.strSectionDate == arr[0].strSectionDate {
                                        value.arrMessages.insert(contentsOf: arr[0].arrMessages, at: 0)
                                        found = true
                                        break
                                    }
                                }
                                
                                if !found {
                                    self.arrChatSections.insert(contentsOf: arr, at: 0)
                                }

                            }
                            else {
                                //if next pages are loaded append the arrays to previous
                                var indexNotToInsert = [Int]()
                                for (_, value) in self.arrChatSections.enumerated() {
                                    
                                    for (index,valueInner) in arr.enumerated() {
                                        if value.strSectionDate == valueInner.strSectionDate {
                                            value.arrMessages.append(contentsOf: valueInner.arrMessages)
                                            indexNotToInsert.append(index)
                                            break
                                        }
                                    }
                                }
                                
                                var arr = arr
                                arr.remove(at: indexNotToInsert)
                                if arr.count > 0 {
                                    self.arrChatSections.append(contentsOf: arr)
                                }

                            }
                        }
                        
                        if page != 0 {
                            //save total page count
                            self.totalPageCount = totalPages == "" ? 1 :  Int(totalPages)!
                        }
                        
                        self.tblViewChat.reloadData()
                    }
                }
                else {
//                        self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForSendMessage(type: String) {
                  
        let param = [
            "sendFrom": AuthModel.sharedInstance.strID,
            "sendTo": objConnectionModel.strUserID,
            "connectionId": objConnectionModel.strConnectionId,
            "fileType": type,
            "message": type == kImageType ? convertImageToBase64(image: selectedImage) : txtMessage.text
            ] as [String : Any]
                
        if type == kMessageType {
            print(param)
        }
        
        self.txtMessage.text = ""

        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Connect.sendMessage, param: param, withHeader: true ) { (response, errorMsg) in
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    self.txtMessage.text = ""
                    
                    let arrMSG = [MessageModel]()
                    ConnectionParser.parseMessageList(response: response!, arrAllMessageModel: self.arrChatSections.count > 0 ? self.arrChatSections.last!.arrMessages : arrMSG) { (arr, totalPages) in
                        
                        var found = false
                        for (_, value) in self.arrChatSections.enumerated() {
                            if value.strSectionDate == arr[0].strSectionDate {
                                value.arrMessages.insert(contentsOf: arr[0].arrMessages, at: 0)
                                found = true
                                break
                            }
                        }
                        
                        if !found {
                            self.arrChatSections.insert(contentsOf: arr, at: 0)
                        }
                        
                        self.tblViewChat.reloadData()
                    }
                }
                else {
                        self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    func callWebServiceForConnectionStatusChage(type: String) {
                
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)

        let param = [
            "connectionId": objConnectionModel.strConnectionId,
            "requestStatus": type
            ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Connect.acceptRejectRequest, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    self.bottomView.isHidden = true
                    self.btnMore.isHidden = true
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToBlock() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [
            "blockUserId": objConnectionModel.strUserID,
            "type": "1"
            ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Feed.blockUser, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    self.bottomView.isHidden = true
                    self.btnMore.isHidden = true
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toFullScreen" {
            let vc = segue.destination as! FullScreenView
            vc.strImageUrl = strImgUrlForFullScreen
        }
        
        if segue.identifier == "toUserProfile" {
            let vc = segue.destination as! UserProfileContainer
            vc.strUserID = objConnectionModel.strUserID
            vc.strUserConnectionStatus = kAccepted
            vc.objConnectionModel = objConnectionModel
            vc.dismissConnectOnConversation = true
        }
    }

}
