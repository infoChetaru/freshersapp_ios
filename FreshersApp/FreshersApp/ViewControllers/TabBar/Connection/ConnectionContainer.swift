//
//  ConnectionContainer.swift
//  FreshersApp
//
//  Created by Apple on 18/06/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import PolioPager

class ConnectionContainer: PolioPagerViewController {

    //MARK: - Variables

    override func viewDidLoad() {
        super.viewDidLoad()

        self.needSearchTab = false
        self.selectedBarHeight = 8
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Commons.callWebServiceToGetNotificationCount(tabBarController: self.tabBarController!, index: "4")

        if let tabItems = self.tabBarController?.tabBar.items {
            // In this case we want to modify the badge number of the fourth tab:
            let tabItem = tabItems[4]
            tabItem.badgeValue = nil
        }
    }
    
    override func tabItems()-> [TabItem] {
        return [
            TabItem(title: "Connections"),
            TabItem(title: "Requests")]
    }
    
    override func viewControllers()-> [UIViewController]
    {

        //storyboard changed
       let viewController1 = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "ConnectionViewController")

        //storyboard changed
        let viewController2 = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "RequestsViewController") as! RequestsViewController
        
        return [viewController1, viewController2]
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
