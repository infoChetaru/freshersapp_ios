//
//  RequestPopup.swift
//  FreshersApp
//
//  Created by Apple on 19/06/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class RequestPopup: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var imgUser:UIImageView!
    @IBOutlet weak var lblOpeningMessage:UILabel!

    //MARK: - Variable
    let panel = JKNotificationPanel()
    var objRequest = RequestModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupData()
    }
    
    //MARK: - Custom Methods
    func setupData() {
        imgUser.kf.setImage(with: URL(string: objRequest.strImageUrl), placeholder: nil)
        lblOpeningMessage.text = objRequest.strOpeningMessage
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnAcceptAction(_ sender : UIButton) {
        callWebServiceForAcceptReject(status: kAccept)
    }
    
    @IBAction func btnRejectAction(_ sender : UIButton) {
        callWebServiceForAcceptReject(status: kReject)
    }

    @IBAction func btnViewProfileAction(_ sender : UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - WebService Methods
    func callWebServiceForAcceptReject(status: String) {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["connectionId" : objRequest.strID,
                     "requestStatus": status]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Connect.acceptRejectRequest, param: param, withHeader: true) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
