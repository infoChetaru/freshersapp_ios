//
//  ChatTabViewController.swift
//  FreshersApp
//
//  Created by Apple on 12/08/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import GrowingTextView
import JKNotificationPanel
import NVActivityIndicatorView

class ChatTabViewController: UIViewController, GrowingTextViewDelegate, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var tblViewChat:UITableView!
    @IBOutlet weak var txtMessage:GrowingTextView!
    @IBOutlet weak var bottomView:UIView!
    @IBOutlet weak var btnMore:UIButton!
    
//    @IBOutlet weak var headerViewConnect:UIView!
//    @IBOutlet weak var headerViewOtherReq:UIView!
    @IBOutlet weak var heightViewConnect:NSLayoutConstraint!
    @IBOutlet weak var heightViewOtherReq:NSLayoutConstraint!

    @IBOutlet weak var txtOpeningMessage: UITextView!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    @IBOutlet weak var lblHeaderConnectTitle: UILabel!
    @IBOutlet weak var btnConnect: UIButton!
    @IBOutlet weak var btnOther: UIButton!
    @IBOutlet weak var btnCancel: UIButton!


    //MARK: - Variable
    let panel = JKNotificationPanel()
    var objConnectionModel = ConnectionModel()
    var currentPage = 1
    var isLoadMore = true
    var totalPageCount = 1
    var arrChatSections = [ChatModel]()
    var selectedImage = UIImage()
    var imagePicker = UIImagePickerController()
    var strImgUrlForFullScreen = ""
    var strUserConnectionStatus = ""
    var strblockedID = ""
    var dismissConnectOnConversation = false
    var objContainer = UserProfileContainer()

    var strDaysRemaingToResend = ""
    var strConnectionID = ""
//    var objContainer = UserProfileContainer()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        automaticallyAdjustsScrollViewInsets = false
        setupData()
        
        txtOpeningMessage.contentInset = UIEdgeInsets(top: 8, left: 4, bottom: 8, right: 4)
                
        if !UIDevice.current.hasNotch {
            //... consider notch
            btnCancel.isHidden = false
        }

        callWebServiceForUserConnectStatus(showLoader: true)

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        callWebServiceForUserConnectStatus(showLoader: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name("ChatPush"), object: nil)
    }
    
    //MARK: - Custom Methods
    func setupData() {
        
        if strUserConnectionStatus == kReceivedRequest {
            bottomView.isHidden = true
        }
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(refreshChatView),
            name: NSNotification.Name(rawValue: "ChatPush"),
            object: nil)
        
        tblViewChat.transform = CGAffineTransform(scaleX: 1, y: -1)
        imagePicker.delegate = self
//        callWebServiceForMessages(showLoader: true, page: currentPage)
        
    }
    
    func setUI() {
//        tblViewChat.tableFooterView = nil
        heightViewConnect.constant = 0
        heightViewOtherReq.constant = 0

        switch self.strUserConnectionStatus {
        case kCanSend, kUnfriend, kCancel:
            heightViewConnect.constant = 500
            heightViewOtherReq.constant = 0
            arrChatSections.removeAll()
            tblViewChat.reloadData()
            bottomView.isHidden = true
        case kPending:
            heightViewConnect.constant = 0
            heightViewOtherReq.constant = 220
            btnOther.isHidden = false
            btnOther.setTitle("Cancel Connection", for: .normal)
            lblHeaderTitle.text = "Connection sent."
            arrChatSections.removeAll()
            tblViewChat.reloadData()
            bottomView.isHidden = true
        case kDecline:
            btnOther.isHidden = true
            lblHeaderTitle.text = "Your connection request is rejected. You can reconnect after " + strDaysRemaingToResend + " days."
            arrChatSections.removeAll()
            tblViewChat.reloadData()
            bottomView.isHidden = true
            heightViewConnect.constant = 0
            heightViewOtherReq.constant = 300
        case kYouDeclined:
            heightViewConnect.constant = 500
            heightViewOtherReq.constant = 0
            lblHeaderConnectTitle.text = "You rejected user's request previously. Do you want to reconnect?"
            arrChatSections.removeAll()
            tblViewChat.reloadData()
            bottomView.isHidden = true
        case kAccepted:
            /*heightViewConnect.constant = 0
            heightViewOtherReq.constant = 0
            arrChatSections.removeAll()
            isLoadMore = true
            currentPage = 1
            callWebServiceForMessages(showLoader: true, page: 1)
            bottomView.isHidden = false*/
            heightViewConnect.constant = 0
            heightViewOtherReq.constant = 220
            btnOther.isHidden = false
            btnOther.setTitle("Return To Conversation", for: .normal)
            lblHeaderTitle.text = "You are connected."
            arrChatSections.removeAll()
            tblViewChat.reloadData()
            bottomView.isHidden = true
        case kBlocked:
            if strblockedID != AuthModel.sharedInstance.strID {
                //you blocked
                lblHeaderTitle.text = "User Blocked."
            }
            else {
                //you are blocked
                lblHeaderTitle.text = "Your are blocked by the user."
            }
            heightViewConnect.constant = 0
            heightViewOtherReq.constant = 220
            arrChatSections.removeAll()
            tblViewChat.reloadData()
            bottomView.isHidden = true
            btnOther.isHidden = true
        case kReceivedRequest:
            bottomView.isHidden = true
            currentPage = 1
            callWebServiceForMessages(showLoader: true, page: currentPage)
        default:
            print("Default")
        }
    }
    
    @objc func refreshChatView() {
        callWebServiceForMessages(showLoader: false, page: 0)
    }
    
    /**
     This function opens the camera of device
     */
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            //case when device has a camera
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            //present camera
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            //case when device has no camera
            let alert  = UIAlertController(title: StringConstant.titleWarning, message: StringConstant.titleNoCamera, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: StringConstant.titleOK, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     This function opens the gallery of device
     */
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        imagePicker.allowsEditing = true
        //present gallery
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    /**
     This function converts images into base64 and keep them into string
     */
    func convertImageToBase64(image: UIImage) -> String {
        var imgData = UIImageJPEGRepresentation(image, 1.0)!
        if let imageData = selectedImage.jpeg(.medium) {
            print(imageData.count)
            imgData = imageData
        }
        return imgData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    @objc func btnImgAction(_ sender: UIButton) {
        
        var superview = sender.superview
        while let view = superview, !(view is UITableViewCell) {
            superview = view.superview
        }
        guard let cell = superview as? UITableViewCell else {
            print("button is not contained in a table view cell")
            return
        }
        guard let indexPath = tblViewChat.indexPath(for: cell) else {
            print("failed to get index path for cell containing button")
            return
        }
        // We've got the index path for the cell that contains the button, now do something with it.
        print("button is in row \(indexPath.row) and section \(indexPath.section)")
        
        strImgUrlForFullScreen = arrChatSections[indexPath.section].arrMessages[indexPath.row].strMessage
        
        self.performSegue(withIdentifier: "toFullScreen", sender: nil)
        
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    /**
     This is pickercontroller delegate which is called when user selects any image from gallery/or saves any camera image
     */
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            //get the original image & set it as feedback image
            selectedImage = image
            self.callWebServiceForSendMessage(type: kImageType)
            self.dismiss(animated: false, completion: nil)
        }
        else{
            //case when image is not supported
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.titleNoSupportedImage)
            picker.popViewController(animated: true)
            print("Something went wrong in  image")
        }
    }
    
    /**
     This is pickercontroller delegate which is called when user cancel the image picker view
     */
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }
    
    
    //MARK: - IBAction Methods
    @IBAction func btnConnectNowAction(_ sender : UIButton) {
        
        txtOpeningMessage.resignFirstResponder()
        
        switch self.strUserConnectionStatus {
        case kCanSend, kUnfriend:
            if (txtOpeningMessage.text?.isEmpty)! {
                panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyMessage)
            }
            else {
                callWebServiceForUserConnect()
            }
        case kPending:
            callWebServiceForConnectionStatusChage(type: kCancel)
        case kAccepted:
//            callWebServiceForConnectionStatusChage(type: kUnfriend)
            
            if dismissConnectOnConversation {
                objContainer.btnDismissAction(sender)
            }
            else {
                heightViewConnect.constant = 0
                heightViewOtherReq.constant = 0
                arrChatSections.removeAll()
                isLoadMore = true
                currentPage = 1
                callWebServiceForMessages(showLoader: true, page: 1)
                bottomView.isHidden = false
            }
        case kReceivedRequest:
            callWebServiceForConnectionStatusChage(type: kAccepted)
        default:
            if (txtOpeningMessage.text?.isEmpty)! {
                panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyMessage)
            }
            else {
                callWebServiceForUserConnect()
            }
        }
    }

    @IBAction func btnCameraAction(_ sender : UIButton) {
        //create alert view for choice of Camera or Gallery
        let alert = UIAlertController(title: StringConstant.titleSelectOptionMedia, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: StringConstant.titleCamera, style: .default, handler: { _ in
            //open camera
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: StringConstant.titleGallery, style: .default, handler: { _ in
            //open gallery
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: StringConstant.titleCancel, style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender as? UIView
            alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnSendMessageAction(_ sender : UIButton) {
        if (txtMessage.text?.isEmpty)! {
            return
        }
        else {
            self.callWebServiceForSendMessage(type: kMessageType)
        }
    }
        
    @IBAction func btnMoreAction(_ sender : UIButton) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        if strUserConnectionStatus == kAccepted {
            alert.addAction(UIAlertAction(title: StringConstant.titleDisconnect, style: .default, handler: { (action) in
                self.callWebServiceForConnectionStatusChage(type: kUnfriend)
            }))
        }
        
        if strUserConnectionStatus == kBlocked && strblockedID != AuthModel.sharedInstance.strID {
            alert.addAction(UIAlertAction(title: StringConstant.titleUnBlock, style: .default, handler: { (action) in
                self.callWebServiceToBlock(type: "2")
            }))
        }
        else if strUserConnectionStatus != kBlocked{
            alert.addAction(UIAlertAction(title: StringConstant.titleBlock, style: .default, handler: { (action) in
                self.callWebServiceToBlock(type: "1")
            }))
        }
        
        alert.addAction(UIAlertAction(title: StringConstant.titleCancel, style: .cancel, handler: { (action) in
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func btnAcceptAction(_ sender : UIButton) {
        callWebServiceForConnectionStatusChage(type: kAccept)
    }

    @objc func btnRejectAction(_ sender : UIButton) {
        callWebServiceForConnectionStatusChage(type: kDecline)
    }

    
    //MARK: - GrowingTextView Delegate
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK: - UITableViewDelegate Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrChatSections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if strUserConnectionStatus == kReceivedRequest ||  strUserConnectionStatus == kReject {
            return arrChatSections[section].arrMessages.count + 1
        }
        
        return arrChatSections[section].arrMessages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if strUserConnectionStatus == kReceivedRequest && indexPath.row == arrChatSections[indexPath.section].arrMessages.count {
            
            let cell : ConnectionReceivedTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ConnectionReceivedTableViewCell") as! ConnectionReceivedTableViewCell
            cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
            cell.selectionStyle = .none
            cell.btnAccept.tag = indexPath.row
            cell.btnAccept.addTarget(self, action: #selector(btnAcceptAction(_:)), for: .touchUpInside)
            cell.btnReject.tag = indexPath.row
            cell.btnReject.addTarget(self, action: #selector(btnRejectAction(_:)), for: .touchUpInside)

            return cell
        }
        
        if strUserConnectionStatus == kReject && indexPath.row == arrChatSections[indexPath.section].arrMessages.count {
            
            let cell : ConnectionReceivedTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ConnectionReceivedTableViewCell") as! ConnectionReceivedTableViewCell
            cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
            cell.selectionStyle = .none
            cell.btnAccept.isHidden = true
            cell.btnReject.isHidden = true
            cell.lblMessage.text = "Connection Request Rejected."
            
            return cell
        }

        if arrChatSections[indexPath.section].arrMessages[indexPath.row].strFileType == kImageType {
            
            var identifier = ""
            if arrChatSections[indexPath.section].arrMessages[indexPath.row].strUserID == AuthModel.sharedInstance.strID {
                identifier = "imageMe"
            }
            else {
                identifier = "imageOther"
            }
            
            let cell : MessageTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier) as! MessageTableViewCell
            cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
            cell.selectionStyle = .none
            
            cell.imgMsg!.kf.setImage(with: URL(string: arrChatSections[indexPath.section].arrMessages[indexPath.row].strMessage), placeholder: nil)
            cell.lblDate.text = arrChatSections[indexPath.section].arrMessages[indexPath.row].strDate
            cell.btnImage.tag = indexPath.row
            cell.btnImage.addTarget(self, action: #selector(btnImgAction(_:)), for: .touchUpInside)
            
            return cell
        }
        else {
            var identifier = ""
            if arrChatSections[indexPath.section].arrMessages[indexPath.row].strUserID == AuthModel.sharedInstance.strID {
                identifier = "messageMe"
            }
            else {
                identifier = "messageOther"
            }
            
            let cell : MessageTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier) as! MessageTableViewCell
            cell.contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
            cell.selectionStyle = .none
            
            cell.lblMessage.text = arrChatSections[indexPath.section].arrMessages[indexPath.row].strMessage
            cell.lblDate.text = arrChatSections[indexPath.section].arrMessages[indexPath.row].strDate
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        //case of archived notification view
        if self.currentPage == self.totalPageCount {
            //case when no more items are there to load
            self.isLoadMore = false
        }
        
        //get last element index
        let lastElement = arrChatSections.count - 1
        
        if indexPath.row == lastElement && isLoadMore {
            // handle your logic here to get more items, add it to dataSource and reload tableview
            
            self.currentPage = self.currentPage + 1
            callWebServiceForMessages(showLoader: false, page: currentPage)
            
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        (view as! UITableViewHeaderFooterView).contentView.transform = CGAffineTransform(scaleX: 1, y: -1)
        (view as! UITableViewHeaderFooterView).contentView.backgroundColor = ColorCodeConstant.themeColor
        (view as! UITableViewHeaderFooterView).textLabel?.textColor = UIColor.white
        (view as! UITableViewHeaderFooterView).textLabel?.textAlignment = .center
        
    }
    
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return arrChatSections[section].strSectionDate
    }
    
    //MARK: - WebService Methods
    func callWebServiceForMessages(showLoader: Bool, page: Int) {
        
        if showLoader {
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        }
        
        var date = ""
        if arrChatSections.count > 0 && (arrChatSections.first?.arrMessages.count)! > 0 {
            date = (arrChatSections.first?.arrMessages.first!.strCreatedAt)!
        }
        
        let param = [
            "connectionId": objConnectionModel.strConnectionId,
            "page": page,
            "date": date, //objConnectionModel.strCreatedAt
            "connectionStatus": strUserConnectionStatus
            ] as [String : Any]
        
        print("++++++++++++++",param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Connect.getChatMessages, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    let arrMSG = [MessageModel]()
                    ConnectionParser.parseMessageList(response: response!, arrAllMessageModel: self.arrChatSections.count > 0 ? self.arrChatSections.last!.arrMessages : arrMSG) { (arr, totalPages) in
                        
                        if page == 1 {
                            //if first time then set array
                            self.arrChatSections = arr
                        }
                        else {
                            if page == 0 {
                                
                                var found = false
                                for (_, value) in self.arrChatSections.enumerated() {
                                    if value.strSectionDate == arr[0].strSectionDate {
                                        value.arrMessages.insert(contentsOf: arr[0].arrMessages, at: 0)
                                        found = true
                                        break
                                    }
                                }
                                
                                if !found {
                                    self.arrChatSections.insert(contentsOf: arr, at: 0)
                                }
                                
                            }
                            else {
                                //if next pages are loaded append the arrays to previous
                                var indexNotToInsert = [Int]()
                                for (_, value) in self.arrChatSections.enumerated() {
                                    
                                    for (index,valueInner) in arr.enumerated() {
                                        if value.strSectionDate == valueInner.strSectionDate {
                                            value.arrMessages.append(contentsOf: valueInner.arrMessages)
                                            indexNotToInsert.append(index)
                                            break
                                        }
                                    }
                                }
                                
                                var arr = arr
                                arr.remove(at: indexNotToInsert)
                                if arr.count > 0 {
                                    self.arrChatSections.append(contentsOf: arr)
                                }
                                
                            }
                        }
                        
                        if page != 0 {
                            //save total page count
                            self.totalPageCount = totalPages == "" ? 1 :  Int(totalPages)!
                        }
                        
                        self.tblViewChat.reloadData()
                    }
                }
                else {
                    //                        self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForSendMessage(type: String) {
        
        let param = [
            "sendFrom": AuthModel.sharedInstance.strID,
            "sendTo": objConnectionModel.strUserID,
            "connectionId": objConnectionModel.strConnectionId,
            "fileType": type,
            "message": type == kImageType ? convertImageToBase64(image: selectedImage) : txtMessage.text
            ] as [String : Any]
        
        if type == kMessageType {
            print(param)
        }
        
        self.txtMessage.text = ""

        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Connect.sendMessage, param: param, withHeader: true ) { (response, errorMsg) in
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    self.txtMessage.text = ""
                    
                    let arrMSG = [MessageModel]()
                    ConnectionParser.parseMessageList(response: response!, arrAllMessageModel: self.arrChatSections.count > 0 ? self.arrChatSections.last!.arrMessages : arrMSG) { (arr, totalPages) in
                        
                        var found = false
                        for (_, value) in self.arrChatSections.enumerated() {
                            if value.strSectionDate == arr[0].strSectionDate {
                                value.arrMessages.insert(contentsOf: arr[0].arrMessages, at: 0)
                                found = true
                                break
                            }
                        }
                        
                        if !found {
                            self.arrChatSections.insert(contentsOf: arr, at: 0)
                        }
                        
                        self.tblViewChat.reloadData()
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForConnectionStatusChage(type: String) {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [
            "connectionId": objConnectionModel.strConnectionId,
            "requestStatus": type
            ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Connect.acceptRejectRequest, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    self.strUserConnectionStatus = response!["data"]["connectionStatus"].stringValue
                    
                    self.setUI()

//                    self.strUserConnectionStatus = type
//
//                    switch self.strUserConnectionStatus {
//                    case kUnfriend:
//                        self.bottomView.isHidden = true
//                        self.btnMore.isHidden = true
//                    case kAccept:
//                        self.bottomView.isHidden = false
//                        self.btnMore.isHidden = false
//                        self.callWebServiceForMessages(showLoader: true, page: 1)
//                    case kReject:
//                        self.tblViewChat.reloadData()
//                        self.bottomView.isHidden = true
//                    default:
//                        print("Default")
//                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToBlock(type: String) {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [
            "blockUserId": objConnectionModel.strUserID,
            "type": type
            ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Feed.blockUser, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    self.bottomView.isHidden = true
                    
                    self.callWebServiceForUserConnectStatus(showLoader: true)
                    self.btnMore.isHidden = true
//                    self.setUI()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForUserConnect() {
                
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)

        let param = [
            "toUserId": objConnectionModel.strUserID,
            "openingMessage": txtOpeningMessage.text!
            ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Connect.userConnect, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: response!["message"].stringValue)
                    self.txtOpeningMessage.text = ""
                    
                    self.strConnectionID = response!["connectionId"].stringValue
                    self.strUserConnectionStatus = response!["connection_status"].stringValue
                    self.setUI()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForUserConnectStatus(showLoader: Bool) {
                
        if showLoader {
           NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        }

        let param = [
            "userId": objConnectionModel.strUserID
            ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Connect.getConnectionStatus, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    self.strUserConnectionStatus = response!["data"]["connectionStatus"].stringValue
                    self.strDaysRemaingToResend = response!["data"]["remainingDays"].stringValue
                    self.strConnectionID = response!["data"]["connectionId"].stringValue

                    self.strblockedID = response!["data"]["blockedUserId"].stringValue

                    self.setUI()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }


    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toFullScreen" {
            let vc = segue.destination as! FullScreenView
            vc.strImageUrl = strImgUrlForFullScreen
        }
    }
}
