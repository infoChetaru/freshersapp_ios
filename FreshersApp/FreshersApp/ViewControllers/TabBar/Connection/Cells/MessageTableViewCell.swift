//
//  MessageTableViewCell.swift
//  FreshersApp
//
//  Created by Apple on 22/06/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var imgMsg: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnImage: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        if imgMsg != nil {
//            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
//            imgMsg.isUserInteractionEnabled = true
//            imgMsg.addGestureRecognizer(tapGestureRecognizer)
//        }
    }

    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView

        // Your action
        let newImageView = UIImageView(image: tappedImage.image!)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage(sender:)))
        newImageView.addGestureRecognizer(tap)
        
        UIView.transition(with: newImageView, duration: 0.25, options: .curveEaseOut, animations: {
            (UIApplication.shared.delegate as! AppDelegate).window!.addSubview(newImageView)
        }, completion: nil)
    }
    
    @objc func dismissFullscreenImage(sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
