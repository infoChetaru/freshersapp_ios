//
//  RequestCollectionViewCell.swift
//  FreshersApp
//
//  Created by Apple on 19/06/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class RequestCollectionViewCell: UICollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblOpeningMessage: UILabel!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
}
