//
//  RequestsViewController.swift
//  FreshersApp
//
//  Created by Apple on 18/06/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

let kAccept = "2"
let kReject = "3"

class RequestsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var viewNoRecord: UIView!

    //MARK: - Variable
    let panel = JKNotificationPanel()
    var arrRequests = [RequestModel]()
    var currentPage = 1
    var isLoadMore = true
    var totalPageCount = 1
    var refresher:UIRefreshControl!
    var strSelectedUserProfile = ""
    var strSelectedConnectionID = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        setupData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
                
        loadData()
    }
    
    //MARK: - Custom Methods
    func setupData() {
        let flow = collectionView.collectionViewLayout as! UICollectionViewFlowLayout // If you create collectionView programmatically then just create this flow by UICollectionViewFlowLayout() and init a collectionView by this flow.

        let itemSpacing: CGFloat = 3
        let itemsInOneLine: CGFloat = 3
        flow.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let width = UIScreen.main.bounds.size.width - itemSpacing * CGFloat(itemsInOneLine - 1) //collectionView.frame.width is the same as  UIScreen.main.bounds.size.width here.
        flow.itemSize = CGSize(width: floor(width/itemsInOneLine), height: width/itemsInOneLine)
        flow.minimumInteritemSpacing = 5
        flow.minimumLineSpacing = 5
        collectionView.collectionViewLayout = flow

        self.refresher = UIRefreshControl()
        self.collectionView!.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor.white
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.collectionView!.addSubview(refresher)
        
        callWebServiceForRequestList(showLoader: arrRequests.count > 0 ? false : true, page: currentPage)
    }
        
    @objc func loadData() {
        refresher.beginRefreshing()
        stopRefresher()
        
        if arrRequests.count == 0 {
            callWebServiceForRequestList(showLoader: true, page: 1)
        }
        else
        {
            callWebServiceForRequestList(showLoader: false, page: 0)
        }
     }

    func stopRefresher() {
        refresher.endRefreshing()
     }
    
    func removeObjectWith(objects: [RequestModel]) {
        
        var arrIndexToDelete = [Int]()
        
        for (index, valueMain) in arrRequests.enumerated() {
            for value in objects {
                if value.strID == valueMain.strID {
                    arrIndexToDelete.append(index)
                }
            }
        }

        arrRequests.remove(at: arrIndexToDelete)
    }
    
    func removeObjectWith(Ids: [String]) {
        
        var arrIndexToDelete = [Int]()
        
        for (index, valueMain) in arrRequests.enumerated() {
            for value in Ids {
                if valueMain.strID == value {
                    arrIndexToDelete.append(index)
                }
            }
        }

        arrRequests.remove(at: arrIndexToDelete)
    }
    
    //MARK: - IBAction Methods
    @objc func btnAcceptAction(_ sender : UIButton) {
        callWebServiceForAcceptReject(selectedIndex: sender.tag, status: kAccept)
    }

    @objc func btnRejectAction(_ sender : UIButton) {
        callWebServiceForAcceptReject(selectedIndex: sender.tag, status: kReject)
    }
    
    @IBAction func btnDismissAction(_ sender : UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - UICollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrRequests.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: RequestCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "RequestCollectionViewCell", for: indexPath) as! RequestCollectionViewCell
        
        cell.imgView.kf.setImage(with: URL(string: arrRequests[indexPath.row].strImageUrl), placeholder: nil)
        cell.lblOpeningMessage.text = arrRequests[indexPath.row].strOpeningMessage
        cell.lblUserName.text = arrRequests[indexPath.row].strName
        cell.btnAccept.tag = indexPath.row
        cell.btnAccept.addTarget(self, action: #selector(btnAcceptAction(_:)), for: .touchUpInside)
        cell.btnReject.tag = indexPath.row
        cell.btnReject.addTarget(self, action: #selector(btnRejectAction(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/2.0 - 5
        
        return CGSize(width: yourWidth, height: 225)
    }
        
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        //case of archived notification view
        if self.currentPage == self.totalPageCount {
            //case when no more items are there to load
            self.isLoadMore = false
        }
        
        //get last element index
        let lastElement = arrRequests.count - 1
        
        if indexPath.row == lastElement && isLoadMore {
            // handle your logic here to get more items, add it to dataSource and reload tableview
            
            self.currentPage = self.currentPage + 1
            callWebServiceForRequestList(showLoader: false, page: currentPage)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        strSelectedUserProfile = arrRequests[indexPath.row].strUserID
        strSelectedConnectionID = arrRequests[indexPath.row].strID

        self.performSegue(withIdentifier: "toUserProfile", sender: nil)
    }

    //MARK: - WebService Methods
    func callWebServiceForRequestList(showLoader: Bool, page: Int) {
        if showLoader {
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        }

        var date = ""

        if arrRequests.count > 0 && page == 0 {
            date = arrRequests[0].strUpdatedAt
        }
        
        let param = [
            "page": page,
            "date": date
            ] as [String : Any]

        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Connect.requestList, param: param, withHeader: true) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                if self.arrRequests.count == 0 {
                    self.viewNoRecord.isHidden = false
                }
            }
            else{
                if response!["status"].boolValue == true {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    self.viewNoRecord.isHidden = true

                    ConnectionParser.parseRequestList(response: response!) { (arr, totalPages, arrOfUnfrindConnections) in
                        
                        if page == 1 {
                            //if first time then set array
                            self.arrRequests = arr
                        }
                        else {
                            if page == 0 {
                                self.removeObjectWith(objects: arr)
                                self.removeObjectWith(Ids: arrOfUnfrindConnections)
                                self.arrRequests.insert(contentsOf: arr, at: 0)
                            }
                            else {
                                //if next pages are loaded append the arrays to previous
                                self.arrRequests.append(contentsOf: arr)
                            }
                        }
                        
                        if page != 0 {
                            //save total page count
                            self.totalPageCount = Int(totalPages)!
                        }
                        
                        if self.arrRequests.count == 0 {
                            self.viewNoRecord.isHidden = false
                        }

                        self.collectionView.reloadData()
                    }
                }
                else {
                    if self.arrRequests.count == 0 {
                        self.viewNoRecord.isHidden = false
                    }

              //      self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForAcceptReject(selectedIndex: Int,status: String) {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["connectionId" : arrRequests[selectedIndex].strID,
                     "requestStatus": status]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Connect.acceptRejectRequest, param: param, withHeader: true) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    
                    self.arrRequests.remove(at: selectedIndex)
                    
                    if self.arrRequests.count == 0 {
                        self.viewNoRecord.isHidden = false
                    }

                    self.collectionView.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "toUserProfile" {
            let vc = segue.destination as! UserProfileContainer
            vc.strUserID = strSelectedUserProfile
            vc.strUserConnectionStatus = kReceivedRequest
            vc.showIndexConnect = true
            
            let objConnectionModel = ConnectionModel()
            objConnectionModel.strConnectionId = strSelectedConnectionID
            objConnectionModel.strUserID = strSelectedUserProfile

            vc.objConnectionModel = objConnectionModel

        }
    }

}
