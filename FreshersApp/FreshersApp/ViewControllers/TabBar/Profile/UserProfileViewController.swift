//
//  UserProfileViewController.swift
//  FreshersApp
//
//  Created by Apple on 16/06/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

class UserProfileViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtDob: UITextField!
    @IBOutlet weak var txtBio: UITextView!
    @IBOutlet weak var imgGender: UIImageView!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var btnCancel: UIButton!

    //MARK: - Variable
    let panel = JKNotificationPanel()
    var objUserProfileModel = UserProfileModel()
    var strUserID = ""
    var objContainer = UserProfileContainer()

    override func viewDidLoad() {
        super.viewDidLoad()

        txtUsername.setIcon(#imageLiteral(resourceName: "username"))
        txtFullName.setIcon(#imageLiteral(resourceName: "username"))
        txtDob.setIcon(#imageLiteral(resourceName: "Dateofbirth"))
        txtLocation.setIcon(#imageLiteral(resourceName: "location-1"))

        setupData()
        callWebServiceForUserProfile(showLoader: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        callWebServiceForUserProfile(showLoader: false)
    }
    
    //MARK: - Custom Methods
    func setupData() {
        imgProfile.kf.setImage(with: URL(string: objUserProfileModel.strProfileImage), placeholder:#imageLiteral(resourceName: "Default_Profile"))
        txtUsername.text = objUserProfileModel.strUsername
        txtFullName.text = objUserProfileModel.strName
        txtLocation.text = objUserProfileModel.strLocaleName

        txtDob.text = objUserProfileModel.strDob
        txtBio.text = objUserProfileModel.strBio
        
        if !UIDevice.current.hasNotch {
            //... consider notch
            btnCancel.isHidden = false
        }

        setSelectedGender()
    }

    func setSelectedGender() {
        
        switch objUserProfileModel.strGenderID {
        case kGenderMale:
            imgGender.image = #imageLiteral(resourceName: "male_selected")
            lblGender.text = "Male"
        case kGenderFemale:
            imgGender.image = #imageLiteral(resourceName: "female_selected")
            lblGender.text = "Female"
        case kGenderOther:
            imgGender.image = #imageLiteral(resourceName: "Other_selected")
            lblGender.text = "Other"
        default:
            imgGender.image = #imageLiteral(resourceName: "male_selected")
            lblGender.text = "Male"
        }
    }
    
    func openImageOnFullscreen() {
        
        self.performSegue(withIdentifier: "toFullScreen", sender: nil)
        
//        let newImageView = UIImageView(image: imgProfile.image!)
//        newImageView.frame = UIScreen.main.bounds
//        newImageView.backgroundColor = .black
//        newImageView.isOpaque = false
//        newImageView.contentMode = .scaleAspectFit
//        newImageView.isUserInteractionEnabled = true
//        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage(sender:)))
//        newImageView.addGestureRecognizer(tap)
//
//        UIView.transition(with: newImageView, duration: 0.25, options: .curveEaseOut, animations: {
//            self.appDelegate.window!.addSubview(newImageView)
//        }, completion: nil)

    }
    
    @objc func dismissFullscreenImage(sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnCameraAction(_ sender : UIButton) {
        self.openImageOnFullscreen()
    }
    
    @IBAction func btnCancelAction(_ sender : UIButton) {
        objContainer.btnDismissAction(sender)
    }

    //MARK: - WebService Methods
    func callWebServiceForUserProfile(showLoader: Bool) {
                
        if showLoader {
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        }

        let param = [
            "userId": strUserID
            ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Profile.userProfile, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    AuthParser.parseUserProfileDetails(response: response!) { ( model ) in
                        self.objUserProfileModel = model
                        self.setupData()
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "toFullScreen" {
            let vc = segue.destination as! FullScreenView
            vc.strImageUrl = objUserProfileModel.strProfileImage
        }
    }
    

}
