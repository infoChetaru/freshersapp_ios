//
//  MyProfileContainer.swift
//  FreshersApp
//
//  Created by Apple on 02/06/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import PolioPager

class MyProfileContainer: PolioPagerViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.needSearchTab = false
        self.selectedBarHeight = 8
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        Commons.callWebServiceToGetNotificationCount(tabBarController: self.tabBarController!, index: "")
    }
    
    override func tabItems()-> [TabItem] {
        return [
            TabItem(title: "My Profile"),
            TabItem(title: "My Uploads"),
            TabItem(title: "My Likes")]
    }
    
    override func viewControllers()-> [UIViewController]
    {

        //storyboard changed
        let viewController1 = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "MyProfileViewControllerNav")
        let viewController2 = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "UploadsViewController") as? UploadsViewController
        viewController2?.strUserID = AuthModel.sharedInstance.strID
        
        let viewController3 = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "MyLikesViewController")

        let navNotPremium = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "NonPremiumUserPopUpNav") as? UINavigationController
        let vcNotPremium = navNotPremium?.topViewController as! NonPremiumUserPopUp
        vcNotPremium.shwoMiddleUpgrade = true

        if Commons.isPremiumUser() {
            return [viewController1, viewController2!, viewController3]
        }
        else {
            return [viewController1, viewController2!, navNotPremium!]
        }

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
