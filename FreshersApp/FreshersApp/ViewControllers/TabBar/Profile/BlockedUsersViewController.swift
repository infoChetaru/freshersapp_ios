//
//  BlockedUsersViewController.swift
//  FreshersApp
//
//  Created by Apple on 09/07/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

class BlockedUsersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var tblBlockedUsers: UITableView!
    @IBOutlet weak var viewNoRecord: UIView!

    //MARK: - Variables
    let panel = JKNotificationPanel()
    var arrBlockedUsers = [UserProfileModel]()

    override func viewDidLoad() {
        super.viewDidLoad()

        tblBlockedUsers.tableFooterView = UIView()

        // Do any additional setup after loading the view.
        callWebServiceForBlockedUsers()
    }
    
    //MARK: - Custom Methods
    @objc func btnUnblockAction(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: StringConstant.titleSureUnblock, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.addAction(UIAlertAction(title: StringConstant.titleUnBlock, style: .default, handler: { (action) in
            self.callWebServiceForUnblock(index: sender.tag)
        }))
        alert.addAction(UIAlertAction(title: StringConstant.titleCancel, style: .cancel, handler: { (action) in
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnBackAction(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - TableView data source and delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrBlockedUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : BlockedUsersTableViewCell = tableView.dequeueReusableCell(withIdentifier: "BlockedUsersTableViewCell") as! BlockedUsersTableViewCell
        
        cell.selectionStyle = .none
        
        cell.imgUser!.kf.setImage(with: URL(string: arrBlockedUsers[indexPath.row].strProfileImage), placeholder: nil)
        cell.lblName.text = arrBlockedUsers[indexPath.row].strName
        cell.btnUnblock.tag = indexPath.row
        cell.btnUnblock.addTarget(self, action: #selector(btnUnblockAction(_:)), for: .touchUpInside)

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
    
    //MARK: - WebService Methods
    func callWebServiceForBlockedUsers() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [
            "": ""
            ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Profile.blockedUsers, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                if self.arrBlockedUsers.count == 0 {
                    self.viewNoRecord.isHidden = false
                }
            }
            else{
                if response!["status"].boolValue == true {
                    self.viewNoRecord.isHidden = true

                    ProfileParser.parseBlockedList(response: response!) { (arr) in
                        self.arrBlockedUsers = arr
                    }

                    self.tblBlockedUsers.reloadData()
                    
                    if self.arrBlockedUsers.count == 0 {
                        self.viewNoRecord.isHidden = false
                    }

                }
                else {
//                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                    if self.arrBlockedUsers.count == 0 {
                        self.viewNoRecord.isHidden = false
                    }
                }
            }
        }
    }
    
    func callWebServiceForUnblock(index: Int) {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [
            "blockUserId": arrBlockedUsers[index].strID,
            "type": "2"
            ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Feed.blockUser, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                if self.arrBlockedUsers.count == 0 {
                    self.viewNoRecord.isHidden = false
                }
            }
            else{
                if response!["status"].boolValue == true {
                    self.viewNoRecord.isHidden = true

                    self.arrBlockedUsers.remove(at: index)
                    self.tblBlockedUsers.reloadData()
                    
                    if self.arrBlockedUsers.count == 0 {
                        self.viewNoRecord.isHidden = false
                    }
                }
                else {
                    //                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                    if self.arrBlockedUsers.count == 0 {
                        self.viewNoRecord.isHidden = false
                    }
                }
            }
        }
    }
        
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
