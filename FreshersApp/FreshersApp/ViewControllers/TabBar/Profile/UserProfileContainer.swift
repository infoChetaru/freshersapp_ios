//
//  UserProfileContainer.swift
//  FreshersApp
//
//  Created by Apple on 16/06/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import PolioPager

class UserProfileContainer: PolioPagerViewController {

    //MARK: - Variable
    var strUserID = ""
    var strUserConnectionStatus = ""
    var objConnectionModel = ConnectionModel()
    var showIndexConnect = false
    var dismissConnectOnConversation = false

    override func viewDidLoad() {
        super.viewDidLoad()

        self.needSearchTab = false
        self.selectedBarHeight = 8
    }
        
    override func tabItems()-> [TabItem] {
        /*if showIndexConnect {
            return [
                TabItem(title: "Connect"),
                TabItem(title: "User Profile"),
                TabItem(title: "User Uploads")]
        }
        else {
            return [
                TabItem(title: "User Profile"),
                TabItem(title: "Connect"),
                TabItem(title: "User Uploads")]
        }*/
        
        if showIndexConnect {
            return [
                TabItem(title: "Connect"),
                TabItem(title: "User Profile"),
                TabItem(title: "User Uploads")]
        }
        return [
            TabItem(title: "User Profile"),
            TabItem(title: "User Uploads"),
            TabItem(title: "Connect")]

    }
    
    override func viewControllers()-> [UIViewController]
    {

        //storyboard changed
        let userProfileVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController
        userProfileVC.strUserID = strUserID
        userProfileVC.objContainer = self
        
        let userConnectVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "UserConnectViewController") as! UserConnectViewController
        userConnectVC.strUserID = strUserID
        userConnectVC.objContainer = self

        let userUploadsVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "UploadsViewController") as? UploadsViewController
        userUploadsVC!.showNavView = false
        userUploadsVC!.strUserID = strUserID

        let navNotPremium = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "NonPremiumUserPopUpNav") as? UINavigationController
        let vcNotPremium = navNotPremium?.topViewController as! NonPremiumUserPopUp
        vcNotPremium.shwoMiddleUpgrade = true
        vcNotPremium.strTitle = "You are not yet connected to this user."
        vcNotPremium.strDesc = "You must have a premium membership to view their previous posts."

//        let chatVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "ChatViewController") as? ChatViewController
//        chatVC?.strUserConnectionStatus = strUserConnectionStatus

        let chatVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "ChatTabViewController") as? ChatTabViewController
        chatVC?.strUserConnectionStatus = strUserConnectionStatus
        chatVC?.objConnectionModel = objConnectionModel
        chatVC?.dismissConnectOnConversation = dismissConnectOnConversation
        chatVC?.objContainer = self

        /*if showIndexConnect {
            if Commons.isPremiumUser() {
                return [viewController2, viewController1, viewControllerUploads!]
            }
            else {
                return [viewController2, viewController1, navNotPremium!]
            }
        }
        else {
            if Commons.isPremiumUser() {
                return [viewController1, viewController2, viewControllerUploads!]
            }
            else {
                return [viewController1, viewController2, navNotPremium!]
            }
        }*/
        
        //        if Commons.isPremiumUser() {
        //            return [viewController1, viewControllerUploads!, viewController2]
        //        }
        //        else {
        //            return [viewController1, navNotPremium!, viewController2]
        //        }

        var arrVC = [UIViewController]()
        
        arrVC.append(userProfileVC)
                
        if Commons.isPremiumUser() || strUserConnectionStatus == kAccepted {
            arrVC.append(userUploadsVC!)
        }
        else {
            arrVC.append(navNotPremium!)
        }
        
//        if strUserConnectionStatus == kAccepted || strUserConnectionStatus == kReceivedRequest {
        if showIndexConnect {
            arrVC.insert(chatVC!, at: 0)
        }
        else {
            arrVC.append(chatVC!)
        }
//        }
//        else {
//            arrVC.append(userConnectVC)
//        }
        
        return arrVC
    }
    
    func btnDismissAction(_ sender : UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UIPageViewController {

    func goToNextPage() {
       guard let currentViewController = self.viewControllers?.first else { return }
       guard let nextViewController = dataSource?.pageViewController( self, viewControllerAfter: currentViewController ) else { return }
       setViewControllers([nextViewController], direction: .forward, animated: false, completion: nil)
    }

    func goToPreviousPage() {
       guard let currentViewController = self.viewControllers?.first else { return }
       guard let previousViewController = dataSource?.pageViewController( self, viewControllerBefore: currentViewController ) else { return }
       setViewControllers([previousViewController], direction: .reverse, animated: false, completion: nil)
    }

}
