//
//  UserConnectViewController.swift
//  FreshersApp
//
//  Created by Apple on 16/06/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

let kCanSend = "0"
let kPending = "1"
let kAccepted = "2"
let kDecline = "3"
let kUnfriend = "4"
let kBlocked = "5"
let kCancel = "6"
let kYouDeclined = "7"
let kReceivedRequest = "8"

class UserConnectViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var heightTxtMessage: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var verticalBtnConnect: NSLayoutConstraint!
    @IBOutlet weak var btnBottom: UIButton!
    @IBOutlet weak var btnReject: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var heightRejectButton: NSLayoutConstraint!

    //MARK: - Variable
    let panel = JKNotificationPanel()
    var strUserID = ""
    var strConnectionStatus = ""
    var strDaysRemaingToResend = ""
    var strConnectionID = ""
    var objContainer = UserProfileContainer()

    override func viewDidLoad() {
        super.viewDidLoad()

        txtMessage.contentInset = UIEdgeInsets(top: 8, left: 4, bottom: 8, right: 4)
                
        if !UIDevice.current.hasNotch {
            //... consider notch
            btnCancel.isHidden = false
        }

        callWebServiceForUserConnectStatus(showLoader: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        callWebServiceForUserConnectStatus(showLoader: false)
    }
    
    //MARK: - Custom Methods
    func setUpUI() {

        btnBottom.isHidden = false
        heightRejectButton.constant = 0

        switch self.strConnectionStatus {
        case kCanSend, kUnfriend, kCancel:
            heightTxtMessage.constant = 200
            verticalBtnConnect.constant = 30
            btnBottom.setTitle("Connect Now", for: .normal)
            lblTitle.text = "Send an opening message:"
        case kPending:
            heightTxtMessage.constant = 0
            verticalBtnConnect.constant = 0
            btnBottom.setTitle("Cancel Connection", for: .normal)
            lblTitle.text = "Conection sent."
        case kAccepted:
            heightTxtMessage.constant = 0
            verticalBtnConnect.constant = 0
            btnBottom.setTitle("Disconnect", for: .normal)
            lblTitle.text = "You are connected."
        case kDecline:
            heightTxtMessage.constant = 0
            verticalBtnConnect.constant = 0
            btnBottom.isHidden = true
            lblTitle.text = "Your connection request is rejected. You can reconnect after " + strDaysRemaingToResend + " days."
        case kBlocked:
            heightTxtMessage.constant = 0
            verticalBtnConnect.constant = 0
            btnBottom.isHidden = true
            lblTitle.text = "Your are blocked by the user."
        case kYouDeclined:
            heightTxtMessage.constant = 200
            verticalBtnConnect.constant = 30
            btnBottom.setTitle("Connect Now", for: .normal)
            lblTitle.text = "You rejected user's request previously. Do you want to reconnect?"
        case kReceivedRequest:
            heightTxtMessage.constant = 0
            verticalBtnConnect.constant = 0
            heightRejectButton.constant = 50
            btnBottom.setTitle("Accept", for: .normal)
            lblTitle.text = "You received connection request."
        default:
            heightTxtMessage.constant = 200
            verticalBtnConnect.constant = 30
            btnBottom.setTitle("Connect Now", for: .normal)
            lblTitle.text = "Send an opening message:"
        }
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnConnectNowAction(_ sender : UIButton) {
        
        txtMessage.resignFirstResponder()
        
        switch self.strConnectionStatus {
        case kCanSend, kUnfriend:
            if (txtMessage.text?.isEmpty)! {
                panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyMessage)
            }
            else {
                callWebServiceForUserConnect()
            }
        case kPending:
            callWebServiceForConnectionStatusChage(type: kCancel)
        case kAccepted:
            callWebServiceForConnectionStatusChage(type: kUnfriend)
        case kReceivedRequest:
            callWebServiceForConnectionStatusChage(type: kAccepted)
        default:
            if (txtMessage.text?.isEmpty)! {
                panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyMessage)
            }
            else {
                callWebServiceForUserConnect()
            }
        }
    }
    
    @IBAction func btnRejectAction(_ sender : UIButton) {
        callWebServiceForConnectionStatusChage(type: kDecline)
    }
    
    @IBAction func btnCancelAction(_ sender : UIButton) {
        objContainer.btnDismissAction(sender)
    }
    
    //MARK: - WebService Methods
    func callWebServiceForUserConnect() {
                
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)

        let param = [
            "toUserId": strUserID,
            "openingMessage": txtMessage.text!
            ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Connect.userConnect, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: response!["message"].stringValue)
                    self.txtMessage.text = ""
                    
                    self.strConnectionID = response!["connectionId"].stringValue
                    self.strConnectionStatus = response!["connection_status"].stringValue
                    self.setUpUI()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    func callWebServiceForUserConnectStatus(showLoader: Bool) {
                
        if showLoader {
           NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        }

        let param = [
            "userId": strUserID
            ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Connect.getConnectionStatus, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    self.strConnectionStatus = response!["data"]["connectionStatus"].stringValue
                    self.strDaysRemaingToResend = response!["data"]["remainingDays"].stringValue
                    self.strConnectionID = response!["data"]["connectionId"].stringValue

                    self.setUpUI()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForConnectionStatusChage(type: String) {
                
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)

        let param = [
            "connectionId": strConnectionID,
            "requestStatus": type
            ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Connect.acceptRejectRequest, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    self.strConnectionStatus = response!["data"]["connectionStatus"].stringValue
                    self.setUpUI()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
