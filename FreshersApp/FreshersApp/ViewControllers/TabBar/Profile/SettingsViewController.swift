//
//  SettingsViewController.swift
//  FreshersApp
//
//  Created by Apple on 24/07/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var tblSettings: UITableView!

    //MARK: - Variables
    let panel = JKNotificationPanel()
    var arrNames = [String]()
    var arrImages = [UIImage]()
    var objProfile = ProfileViewController()
    var arrFeeds = [FeedModel]()
    var strSesitive = kNotAllowSensitive
    var selectedFeed = FeedModel()
    var selectedIndexPath:IndexPath?

    let strEditProfile = "Edit Profile"
    let strAccountDetails = "My Details"
    let strMyUploads = "My Uploads"
    let strMyLikes = "My Likes"
    let strPremiumMember = "You are Premium Member"
    let strTodayTopLikedPost = "Today’s Top Liked Post"
    let strBlockedUsers = "Blocked Users"
    let strTermsOfUse = "Terms Of Use"
    let strPrivacyPolicy = "Privacy Policy"
    let strContactUs = "Contact Us"
    let strLogout = "Logout"
    let strUpgradeToPremium = "Upgrade To Premium"
    let strShowTopLikedPost = "Show Post"
    let strShowAccountDetails = "Show My Detail"

    let imgEditProfile = #imageLiteral(resourceName: "Edit_Profile")
    let imgAccountDetails = #imageLiteral(resourceName: "login_details")
    let imgMyUploads = #imageLiteral(resourceName: "my_uploads")
    let imgMyLikes = #imageLiteral(resourceName: "my_likes")
    let imgPremiumMember = #imageLiteral(resourceName: "Upgrade_To_Premium")
    let imgTodayTopLikedPost = #imageLiteral(resourceName: "Todays_Top_Liked_Post")
    let imgBlockedUsers = #imageLiteral(resourceName: "Blocked_Users")
    let imgTermsOfUse = #imageLiteral(resourceName: "Terms_of_Use")
    let imgPrivacyPolicy = #imageLiteral(resourceName: "Privacy_Policy")
    let imgContactUs = #imageLiteral(resourceName: "Contact_Us")
    let imgLogout = #imageLiteral(resourceName: "Logout-1")
    let imgUpgradeToPremium =  #imageLiteral(resourceName: "Upgrade_To_Premium")
    let imgShowTopLikedPost =  #imageLiteral(resourceName: "Upgrade_To_Premium")
    let imgShowAccountDetails =  #imageLiteral(resourceName: "Upgrade_To_Premium")

    override func viewDidLoad() {
        super.viewDidLoad()

        setupData()
    }

    //MARK: - Custom Methods
    /**
     * Set ups the UI
     */
    func setupData() {
        tblSettings.tableFooterView = UIView()
        
        if Commons.isPremiumUser() {
            arrNames = [strEditProfile, strAccountDetails , strMyUploads, strMyLikes, strPremiumMember, strTodayTopLikedPost, strBlockedUsers, strTermsOfUse, strPrivacyPolicy, strContactUs, strLogout]
        }
        else {
            arrNames = [strEditProfile, strAccountDetails, strMyUploads, strMyLikes, strUpgradeToPremium, strTodayTopLikedPost, strBlockedUsers, strTermsOfUse, strPrivacyPolicy, strContactUs, strLogout]
        }
        
        arrImages = [imgEditProfile, imgAccountDetails, imgMyUploads, imgMyLikes, imgPremiumMember, imgTodayTopLikedPost, imgBlockedUsers, imgTermsOfUse, imgPrivacyPolicy, imgContactUs, imgLogout]
        
        strSesitive = AuthModel.sharedInstance.strSensitivePostAllowed
    }
    
    func logout() {
        let strFcmToken = UserDefaults.standard.value(forKey: "fcm_token") as! String
        let arrRem = UserDefaults.standard.stringArray(forKey: kRememberArray) ?? [String]()

        let defaults = UserDefaults.standard
        defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.set(strFcmToken, forKey: "fcm_token")
        UserDefaults.standard.set(arrRem, forKey: kRememberArray)

        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let centerVC = mainStoryBoard.instantiateViewController(withIdentifier: "LoginNav")
        appDel.window!.rootViewController = centerVC
        appDel.window!.makeKeyAndVisible()
        
    }
    
    func viewTopLikedPhoto(selectedFeed: FeedModel, selectedIndexPath: IndexPath) {
        self.selectedFeed = selectedFeed
        self.selectedIndexPath = selectedIndexPath
        self.performSegue(withIdentifier: "toPostDetail", sender: nil)
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnBackAction(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnSensitiveContentAction() {
        if strSesitive == kAllowSensitive {
            strSesitive = kNotAllowSensitive
            tblSettings.reloadData()
            callWebServiceToUpdateSesitivePostStatus()
        }
        else {
            strSesitive = kAllowSensitive
            tblSettings.reloadData()
            callWebServiceToUpdateSesitivePostStatus()
        }
    }
        
    //MARK: - TableView data source and delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        let cell : UserTableViewCell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell") as! UserTableViewCell
        
        cell.selectionStyle = .none
        
        switch arrNames[indexPath.row] {
        case strShowTopLikedPost:
            if AuthModel.sharedInstance.strTopPostID == "" {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "TopPostText")
                
                cell!.selectionStyle = .none
                
                let lbl = cell?.viewWithTag(10) as? UILabel
                lbl!.text = "No posts for the day."
                
                return cell!
                
            }
            else if AuthModel.sharedInstance.strTopPostLikeCount == "0" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TopPostText")
                
                cell!.selectionStyle = .none
                
                let lbl = cell?.viewWithTag(10) as? UILabel
                lbl!.text = "No top liked posts for the day."
                
                return cell!
            }
            else {
                let cell : TopLikedPostTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TopLikedPostTableViewCell") as! TopLikedPostTableViewCell
                
                cell.selectionStyle = .none
                
                cell.arrFeeds = arrFeeds
                cell.objSetting = self
                cell.collectionView.reloadData()
                
                return cell
            }
        case strShowAccountDetails:
            let cell : MyDetailsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyDetailsTableViewCell") as! MyDetailsTableViewCell

            cell.selectionStyle = .none
            
            cell.lblLoginDetail.text = AuthModel.sharedInstance.strUsername
            
            if strSesitive == kAllowSensitive {
                cell.imgCheckBox.image = #imageLiteral(resourceName: "check-purple")
            }
            else {
                cell.imgCheckBox.image = #imageLiteral(resourceName: "uncheck")
            }
            
            cell.btnSensitive.addTarget(self, action: #selector(btnSensitiveContentAction), for: .touchUpInside)

            return cell
        case strTodayTopLikedPost:
            cell.imgDropDown.isHidden = false
            if arrNames.contains(strShowTopLikedPost) {
                cell.imgDropDown.image = #imageLiteral(resourceName: "up")
            }
            else {
                cell.imgDropDown.image = #imageLiteral(resourceName: "down")
            }
        case strAccountDetails:
            cell.imgDropDown.isHidden = false
            if arrNames.contains(strShowAccountDetails) {
                cell.imgDropDown.image = #imageLiteral(resourceName: "up")
            }
            else {
                cell.imgDropDown.image = #imageLiteral(resourceName: "down")
            }
        default:
            cell.imgDropDown.isHidden = true
        }
        
        cell.lblName.text = arrNames[indexPath.row]
        cell.imgUser.image = arrImages[indexPath.row]

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if arrNames[indexPath.row] == strShowTopLikedPost {
            if AuthModel.sharedInstance.strTopPostID != "" && AuthModel.sharedInstance.strTopPostLikeCount != "0" {
                return 200
            }
        }
        
        if arrNames[indexPath.row] == strShowAccountDetails {
            return 100
        }
        
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch arrNames[indexPath.row] {
        case strEditProfile:
            objProfile.editProfileAction()
            self.navigationController?.popViewController(animated: true)
        case strAccountDetails:
            if arrNames.contains(strShowAccountDetails) {
                
                let indexToRemove = arrNames.index(of: strShowAccountDetails)!
                arrNames.remove(at: indexToRemove)
                arrImages.remove(at: indexToRemove)
            }
            else {
                
                arrNames.insert(strShowAccountDetails, at: (arrNames.index(of: strAccountDetails)! + 1) )
                arrImages.insert(imgShowAccountDetails, at: (arrImages.index(of: imgAccountDetails)! + 1) )
            }
            
            tblSettings.reloadData()
        case strMyUploads:
            let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "UploadsViewController") as? UploadsViewController
            objVC?.strUserID = AuthModel.sharedInstance.strID
            self.navigationController?.pushViewController(objVC!, animated: true)
        case strMyLikes:
            if Commons.isPremiumUser() {
                let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "MyLikesViewController") as? MyLikesViewController
                self.navigationController?.pushViewController(objVC!, animated: true)
            }
            else {
                let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "NonPremiumUserPopUp") as! NonPremiumUserPopUp
//                objVC.showNavView = true
                objVC.strTitle = "Upgrade To Premium"
                objVC.strDesc = "You must have a premium membership to view your previous liked posts."
                self.navigationController?.pushViewController(objVC, animated: false)
            }
        case strUpgradeToPremium, strPremiumMember:
            let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "GoPremiumPopUp") as! GoPremiumPopUp
            objVC.showNavView = true
            self.navigationController?.pushViewController(objVC, animated: false)
        case strTodayTopLikedPost:
            if Commons.isPremiumUser() {
                if arrNames.contains(strShowTopLikedPost) {
                    
                    let indexToRemove = arrNames.index(of: strShowTopLikedPost)!
                    arrNames.remove(at: indexToRemove)
                    arrImages.remove(at: indexToRemove)
                }
                else {
                    
                    arrNames.insert(strShowTopLikedPost, at: (arrNames.index(of: strTodayTopLikedPost)! + 1) )
                    arrImages.insert(imgShowTopLikedPost, at: (arrImages.index(of: imgTodayTopLikedPost)! + 1) )
                }
                tblSettings.reloadData()
            }
            else {
                let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "NonPremiumUserPopUp") as! NonPremiumUserPopUp
                objVC.strTitle = "Upgrade To Premium"
                objVC.strDesc = "You must have a premium membership to view the top liked post."
                self.navigationController?.pushViewController(objVC, animated: false)
            }
            
        case strBlockedUsers:
            let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "BlockedUsersViewController") as? BlockedUsersViewController
            self.navigationController?.pushViewController(objVC!, animated: true)
        case strLogout:
            let alert = UIAlertController(title: nil, message: StringConstant.titleSureLogout, preferredStyle: UIAlertControllerStyle.actionSheet)
            alert.addAction(UIAlertAction(title: StringConstant.titleLogout, style: .destructive, handler: { (action) in
                self.callWebServiceForLogout()
            }))
            alert.addAction(UIAlertAction(title: StringConstant.titleCancel, style: .cancel, handler: { (action) in
            }))
            
            self.present(alert, animated: true, completion: nil)
        case strTermsOfUse:
            guard let url = URL(string: kTermsOfServicesURL) else { return }
            UIApplication.shared.openURL(url)
        case strPrivacyPolicy:
            guard let url = URL(string: kPrivacyPolicyURL) else { return }
            UIApplication.shared.openURL(url)
        case strContactUs:
            let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "ContactUsPopUp") as! ContactUsPopUp
            objVC.modalPresentationStyle = .overCurrentContext
            self.present(objVC, animated: false, completion: nil)
        default:
            print("Default")
        }
    }
    
    //MARK: - WebService Methods
    func callWebServiceForLogout() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["fcmId":AuthModel.sharedInstance.strFcmId] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.logout, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    self.logout()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToUpdateSesitivePostStatus() {
//        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["sensitivePostsAllowed": strSesitive] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Profile.updateSesitiveStatus, param: param, withHeader: true ) { (response, errorMsg) in
            
//            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
//                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    AuthModel.sharedInstance.strSensitivePostAllowed = self.strSesitive
                    UserDefaults.standard.set(self.strSesitive, forKey: "sensitivePostsAllowed")
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toPostDetail" {
            let vc = segue.destination as! PostDetailViewController
            vc.strPostID = selectedFeed.strID
//            vc.arrFeedModel = arrFeeds
//            vc.selectedIndexPath = selectedIndexPath
        }
    }

}
