//
//  MyProfileViewController.swift
//  FreshersApp
//
//  Created by Apple on 02/06/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

class MyProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtUsername: UITextField!
//    @IBOutlet weak var txtDob: UITextField!
    @IBOutlet weak var txtBio: UITextView!
    @IBOutlet weak var imgMale: UIImageView!
    @IBOutlet weak var imgFemale: UIImageView!
    @IBOutlet weak var imgOther: UIImageView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtLocation: UITextField!
    @IBOutlet weak var lblTopLikedPhoto: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnPremium: UIButton!
    @IBOutlet weak var imgSelectedGender: UIImageView!
    @IBOutlet weak var lblSelectedGender: UILabel!
    @IBOutlet weak var selectedGenderView: UIView!
    @IBOutlet weak var selectGenderView: UIView!
    @IBOutlet weak var heightTopLikedCollectionView: NSLayoutConstraint!
    @IBOutlet weak var lblNoTopLikedPost: UILabel!
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var imgTopLikeArrow:UIImageView!

    //MARK: - Variable
    var imagePicker = UIImagePickerController()
    let panel = JKNotificationPanel()
    var objAuthModel = AuthModel()
    var strGender = kGenderMale
    var arrLocale = [LocaleModel]()
    let localePickerView = UIPickerView()
    var strSelectedLocale = ""
    var isEditMode = false
    var isImageChanged = false
    var arrFeeds = [FeedModel]()
    var selectedFeed = FeedModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        txtUsername.setIcon(#imageLiteral(resourceName: "username"))
        txtFullName.setIcon(#imageLiteral(resourceName: "username"))
//        txtDob.setIcon(#imageLiteral(resourceName: "Dateofbirth"))
        txtLocation.setIcon(#imageLiteral(resourceName: "location-1"))
        imagePicker.delegate = self

        let flow = collectionView.collectionViewLayout as! UICollectionViewFlowLayout // If you create collectionView programmatically then just create this flow by UICollectionViewFlowLayout() and init a collectionView by this flow.

        let itemSpacing: CGFloat = 3
        let itemsInOneLine: CGFloat = 3
        flow.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let width = UIScreen.main.bounds.size.width - itemSpacing * CGFloat(itemsInOneLine - 1) //collectionView.frame.width is the same as  UIScreen.main.bounds.size.width here.
        flow.itemSize = CGSize(width: floor(width/itemsInOneLine), height: width/itemsInOneLine)
        flow.minimumInteritemSpacing = 5
        flow.minimumLineSpacing = 5
        collectionView.collectionViewLayout = flow

        setupData()
        callWebServiceForUserProfile(showLoader: true)
        createPickerView()
        setEditMode()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        txtLocation.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))

        callWebServiceForUserProfile(showLoader: false)
    }
    
    //MARK: - Custom Methods
    func setupData() {

        imgProfile.kf.setImage(with: URL(string: AuthModel.sharedInstance.strProfileImage), placeholder:#imageLiteral(resourceName: "Default_Profile"))
        txtUsername.text = AuthModel.sharedInstance.strUsername
        txtFullName.text = AuthModel.sharedInstance.strName
        txtLocation.text = AuthModel.sharedInstance.strLocaleName

        for (index,value) in arrLocale.enumerated() {
            if value.strID == AuthModel.sharedInstance.strLocaleID {
                localePickerView.selectRow(index, inComponent: 0, animated: false)
            }
        }

        lblNoTopLikedPost.isHidden = true
        if Commons.isPremiumUser() {
            txtLocation.textColor = UIColor.black
            callWebServiceForLocaleList()
            txtLocation.isUserInteractionEnabled = true
        }
        
//        txtDob.text = AuthModel.sharedInstance.strDob
        txtBio.text = AuthModel.sharedInstance.strBio
        strGender = AuthModel.sharedInstance.strGenderID
        
        arrFeeds.removeAll()
        for (index,value) in AuthModel.sharedInstance.strTopPostID.components(separatedBy: ",").enumerated() {
            
            let model = FeedModel()
            model.strID = value
            model.strImgUrl = AuthModel.sharedInstance.strTopPostImgUrl.components(separatedBy: ",")[index]
            model.strLikeCount = AuthModel.sharedInstance.strTopPostLikeCount
            
            arrFeeds.append(model)
        }
        
        collectionView.reloadData()
        
        setSelectedGender()
    }
    
    @objc func doneButtonClicked(_ sender: Any) {
            //your code when clicked on done
        print("Yuhuuuu")
        
        if strSelectedLocale == "" {
            strSelectedLocale = arrLocale[0].strID
            txtLocation.text = arrLocale[0].strName
        }
                
        Commons.callWebServiceForUpdateUserLocation(strLocaleID: strSelectedLocale, strLocaleName: txtLocation.text!) {
            print("Updated")
        }
    }
    
    func setEditMode() {
        if isEditMode {
            txtFullName.isUserInteractionEnabled = true
//            txtDob.isUserInteractionEnabled = true
            txtBio.isUserInteractionEnabled = true
            
            btnEdit.setImage(UIImage(named: "Done"), for: .normal)
            btnPremium.setImage(UIImage(named: "cancel-1"), for: .normal)
            
            selectedGenderView.isHidden = true
            selectGenderView.isHidden = false
            
//            if Commons.isPremiumUser() {
//                txtLocation.isUserInteractionEnabled = true
//            }
        }
        else {
//            txtLocation.isUserInteractionEnabled = false
            txtFullName.isUserInteractionEnabled = false
//            txtDob.isUserInteractionEnabled = false
            txtBio.isUserInteractionEnabled = false
            
            btnEdit.setImage(UIImage(named: "Edit"), for: .normal)
            btnPremium.setImage(UIImage(named: "premium-1"), for: .normal)
            
            selectedGenderView.isHidden = false
            selectGenderView.isHidden = true
        }
    }
    
    func setSelectedGender() {
        
        switch strGender {
        case kGenderMale:
            imgMale.image = #imageLiteral(resourceName: "male_selected")
            imgFemale.image = #imageLiteral(resourceName: "female_not_selected")
            imgOther.image = #imageLiteral(resourceName: "Other_not_selected")
            imgSelectedGender.image = #imageLiteral(resourceName: "male_selected")
            lblSelectedGender.text = "Male"
        case kGenderFemale:
            imgMale.image = #imageLiteral(resourceName: "male_not_selected")
            imgFemale.image = #imageLiteral(resourceName: "female_selected")
            imgOther.image = #imageLiteral(resourceName: "Other_not_selected")
            imgSelectedGender.image = #imageLiteral(resourceName: "female_selected")
            lblSelectedGender.text = "Female"
        case kGenderOther:
            imgMale.image = #imageLiteral(resourceName: "male_not_selected")
            imgFemale.image = #imageLiteral(resourceName: "female_not_selected")
            imgOther.image = #imageLiteral(resourceName: "Other_selected")
            imgSelectedGender.image = #imageLiteral(resourceName: "Other_selected")
            lblSelectedGender.text = "Other"
        default:
            imgMale.image = #imageLiteral(resourceName: "male_not_selected")
            imgFemale.image = #imageLiteral(resourceName: "female_not_selected")
            imgOther.image = #imageLiteral(resourceName: "Other_not_selected")
            imgSelectedGender.image = #imageLiteral(resourceName: "male_selected")
            lblSelectedGender.text = "Male"
        }
    }
    
    func createPickerView() {
                
        localePickerView.delegate = self
        txtLocation.inputView = localePickerView
    }
    
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        txtLocation.inputAccessoryView = toolBar
    }
    
    @objc func action() {
        view.endEditing(true)
    }
    
    /**
     This function opens the camera of device
     */
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            //case when device has a camera
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            //present camera
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            //case when device has no camera
            let alert  = UIAlertController(title: StringConstant.titleWarning, message: StringConstant.titleNoCamera, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: StringConstant.titleOK, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     This function opens the gallery of device
     */
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        imagePicker.allowsEditing = true
        //present gallery
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    /**
     This function converts images into base64 and keep them into string
     */
    func convertImageToBase64(image: UIImage) -> String {
        var imgData = UIImageJPEGRepresentation(imgProfile.image!, 1.0)!
        if let imageData = imgProfile.image!.jpeg(.medium) {
            print(imageData.count)
            imgData = imageData
        }
        return imgData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    func openImageOnFullscreen() {
        
        self.performSegue(withIdentifier: "toFullScreen", sender: nil)
        
//        let newImageView = UIImageView(image: imgProfile.image!)
//        newImageView.frame = UIScreen.main.bounds
//        newImageView.backgroundColor = .black
//        newImageView.contentMode = .scaleAspectFit
//        newImageView.isUserInteractionEnabled = true
//        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage(sender:)))
//        newImageView.addGestureRecognizer(tap)
//
//        UIView.transition(with: newImageView, duration: 0.25, options: .curveEaseOut, animations: {
//            self.appDelegate.window!.addSubview(newImageView)
//        }, completion: nil)

    }
    
    @objc func dismissFullscreenImage(sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    func logout() {
        let strFcmToken = UserDefaults.standard.value(forKey: "fcm_token") as! String
        let defaults = UserDefaults.standard
        defaults.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.set(strFcmToken, forKey: "fcm_token")

        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let centerVC = mainStoryBoard.instantiateViewController(withIdentifier: "LoginNav")
        appDel.window!.rootViewController = centerVC
        appDel.window!.makeKeyAndVisible()

    }
    
    //MARK: - IBAction Methods
    @IBAction func btnCameraAction(_ sender : UIButton) {
        if isEditMode {
            //create alert view for choice of Camera or Gallery
            let alert = UIAlertController(title: StringConstant.titleSelectOptionMedia, message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: StringConstant.titleView, style: .default, handler: { _ in
                //open camera
                self.openImageOnFullscreen()
            }))

            alert.addAction(UIAlertAction(title: StringConstant.titleCamera, style: .default, handler: { _ in
                //open camera
                self.openCamera()
            }))
            
            alert.addAction(UIAlertAction(title: StringConstant.titleGallery, style: .default, handler: { _ in
                //open gallery
                self.openGallary()
            }))
            
            alert.addAction(UIAlertAction.init(title: StringConstant.titleCancel, style: .cancel, handler: nil))
            
            /*If you want work actionsheet on ipad
             then you have to use popoverPresentationController to present the actionsheet,
             otherwise app will crash on iPad */
            switch UIDevice.current.userInterfaceIdiom {
            case .pad:
                alert.popoverPresentationController?.sourceView = sender as? UIView
                alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
                alert.popoverPresentationController?.permittedArrowDirections = .up
            default:
                break
            }
            
            self.present(alert, animated: true, completion: nil)
        }
        else {
            openImageOnFullscreen()
        }
    }

    @IBAction func btnEditAction(_ sender : UIButton) {
        if isEditMode {
            if (txtFullName.text?.isEmpty)! {
                panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyUsername)
            }
//            else if (txtDob.text?.isEmpty)! {
//                panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyDOB)
//            }
            else if (txtBio.text?.isEmpty)! {
                panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyBio)
            }
            else {
                isEditMode = !isEditMode
                setEditMode()
                callWebServiceForUpdateUserProfile()
            }
        }
        else {
            isEditMode = !isEditMode
            setEditMode()
        }
    }

    @IBAction func btnPremiumAction(_ sender : UIButton) {
        if isEditMode {
            isEditMode = !isEditMode
            setEditMode()
            setupData()
            self.isImageChanged = false
        }
        else {
            let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "GoPremiumPopUp") as! GoPremiumPopUp
            self.navigationController?.pushViewController(objVC, animated: false)
        }
    }
    
    @IBAction func btnMaleAction(_ sender : UIButton) {
        if isEditMode {
            strGender = kGenderMale
            setSelectedGender()
        }
    }
    
    @IBAction func btnFemaleAction(_ sender : UIButton) {
        if isEditMode {
            strGender = kGenderFemale
            setSelectedGender()
        }
    }

    @IBAction func btnOtherAction(_ sender : UIButton) {
        if isEditMode {
            strGender = kGenderOther
            setSelectedGender()
        }
    }
    
    @IBAction func btnLogoutAction(_ sender : UIButton) {
        let alert = UIAlertController(title: nil, message: StringConstant.titleSureLogout, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.addAction(UIAlertAction(title: StringConstant.titleLogout, style: .destructive, handler: { (action) in
            self.callWebServiceForLogout()
        }))
        alert.addAction(UIAlertAction(title: StringConstant.titleCancel, style: .cancel, handler: { (action) in
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnTopLikedAction(_ sender : UIButton) {
        if Commons.isPremiumUser() {
            if AuthModel.sharedInstance.strTopPostID == "" {
                lblNoTopLikedPost.isHidden = !lblNoTopLikedPost.isHidden
                lblNoTopLikedPost.text = "No posts for the day."
                
                if lblNoTopLikedPost.isHidden {
                    imgTopLikeArrow.image = #imageLiteral(resourceName: "down")
                }
                else {
                    imgTopLikeArrow.image = #imageLiteral(resourceName: "up")
                }
            }
            else if AuthModel.sharedInstance.strTopPostLikeCount == "0" {
                lblNoTopLikedPost.isHidden = !lblNoTopLikedPost.isHidden
                lblNoTopLikedPost.text = "No top liked posts for the day."
                
                if lblNoTopLikedPost.isHidden {
                    imgTopLikeArrow.image = #imageLiteral(resourceName: "down")
                }
                else {
                    imgTopLikeArrow.image = #imageLiteral(resourceName: "up")
                }

            }
            else {
                if heightTopLikedCollectionView.constant == 0 {
                    heightTopLikedCollectionView.constant = 200
                    imgTopLikeArrow.image = #imageLiteral(resourceName: "up")
                }
                else {
                    heightTopLikedCollectionView.constant = 0
                    imgTopLikeArrow.image = #imageLiteral(resourceName: "down")
                }
            }
        }
        else {
            
            let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "NonPremiumUserPopUp") as! NonPremiumUserPopUp
            self.navigationController?.pushViewController(objVC, animated: false)
        }
    }

    @IBAction func btnOpenPostAction(_ sender : UIButton) {
        self.performSegue(withIdentifier: "toPostDetail", sender: nil)
    }
    
    //MARK: - UICollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrFeeds.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: MyUploadsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyUploadsCollectionViewCell", for: indexPath) as! MyUploadsCollectionViewCell
        
        cell.imgUpload.kf.setImage(with: URL(string: arrFeeds[indexPath.row].strImgUrl), placeholder: nil)
        cell.lblLikeCount.text = arrFeeds[indexPath.row].strLikeCount
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let yourWidth = collectionView.bounds.width/3.0 - 5
//        let yourHeight = yourWidth
        
        return CGSize(width: 200, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedFeed = arrFeeds[indexPath.row]
        self.performSegue(withIdentifier: "toPostDetail", sender: nil)
    }
    
    //MARK: - UIPickerView Delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1 // number of session
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return arrLocale.count // number of dropdown items
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrLocale[row].strName // dropdown item
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            strSelectedLocale = arrLocale[row].strID // selected item
            txtLocation.text = arrLocale[row].strName
//            callWebServiceForUpdateUserLocation()
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    /**
     This is pickercontroller delegate which is called when user selects any image from gallery/or saves any camera image
     */
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            //get the original image & set it as feedback image
            imgProfile.image = image
            self.isImageChanged = true
            self.dismiss(animated: false, completion: nil)
        }
        else{
            //case when image is not supported
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.titleNoSupportedImage)
            picker.popViewController(animated: true)
            print("Something went wrong in  image")
        }
    }
    
    /**
     This is pickercontroller delegate which is called when user cancel the image picker view
     */
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }

    //MARK: - WebService Methods
    func callWebServiceForUserProfile(showLoader: Bool) {
               
        if showLoader {
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        }

        let param = [
            "userId": AuthModel.sharedInstance.strID
            ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Profile.userProfile, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    AuthParser.parseProfileDetails(response: response!) { (_) in
                        self.setupData()
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForUpdateUserProfile() {
                
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)

        let param = [
            "name": txtFullName.text ?? "",
            "dob": AuthModel.sharedInstance.strDob, //txtDob.text ?? "",
            "bio": txtBio.text,
            "gender": strGender,
            "localeId": strSelectedLocale,
            "profileImage": isImageChanged ? convertImageToBase64(image: imgProfile.image!) : ""
            ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Profile.updateProfile, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    AuthParser.parseProfileDetails(response: response!) { (_) in
                        self.setupData()
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForLocaleList() {
                
//        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)

        let param = [:] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Profile.localeList, param: param, withHeader: true ) { (response, errorMsg) in
            
//            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    ProfileParser.parseLocaleList(response: response!) { (arr) in
                        self.arrLocale = arr
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForLogout() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["fcmId":AuthModel.sharedInstance.strFcmId] as [String : Any]

        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Auth.logout, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    self.logout()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "toFullScreen" {
            let vc = segue.destination as! FullScreenView
            vc.strImageUrl = AuthModel.sharedInstance.strProfileImage
        }
        
        if segue.identifier == "toPostDetail" {
            let vc = segue.destination as! PostDetailViewController
            vc.strPostID = selectedFeed.strID
        }
    }
    
}
