//
//  MyUploadsViewController.swift
//  FreshersApp
//
//  Created by Apple on 15/06/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

class UploadsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var viewNoRecord: UIView!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var heightNavView: NSLayoutConstraint!

    //MARK: - Variable
    let panel = JKNotificationPanel()
    var objTagModel = TagModel()
    var arrFeeds = [FeedModel]()
    var currentPage = 1
    var isLoadMore = true
    var totalPageCount = 1
    var refresher:UIRefreshControl!
    var selectedFeed = FeedModel()
    var strUserID = ""
    var showNavView = true
    var selectedIndexPath:IndexPath?

    override func viewDidLoad() {
        super.viewDidLoad()

        setupData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadData()
    }
    
    //MARK: - Custom Methods
    func setupData() {
        
        if showNavView {
            heightNavView.constant = 50
        }
        else {
            heightNavView.constant = 0
        }
        
        let flow = collectionView.collectionViewLayout as! UICollectionViewFlowLayout // If you create collectionView programmatically then just create this flow by UICollectionViewFlowLayout() and init a collectionView by this flow.

        let itemSpacing: CGFloat = 3
        let itemsInOneLine: CGFloat = 3
        flow.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let width = UIScreen.main.bounds.size.width - itemSpacing * CGFloat(itemsInOneLine - 1) //collectionView.frame.width is the same as  UIScreen.main.bounds.size.width here.
        flow.itemSize = CGSize(width: floor(width/itemsInOneLine), height: width/itemsInOneLine)
        flow.minimumInteritemSpacing = 5
        flow.minimumLineSpacing = 5
        collectionView.collectionViewLayout = flow

        self.refresher = UIRefreshControl()
        self.collectionView!.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor.white
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.collectionView!.addSubview(refresher)

        callWebServiceForMyUploads(showLoader: true, page: currentPage)
    }
    
    @objc func btnDeleteAction(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.addAction(UIAlertAction(title: StringConstant.titleDelete, style: .destructive, handler: { (action) in
            self.callWebServiceForDeleteUpload(index: sender.tag)
        }))
        alert.addAction(UIAlertAction(title: StringConstant.titleCancel, style: .cancel, handler: { (action) in
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func loadData() {
        refresher.beginRefreshing()
        stopRefresher()
        
        if arrFeeds.count == 0 {
            callWebServiceForMyUploads(showLoader: true, page: 1)
        }
        else
        {
            callWebServiceForMyUploads(showLoader: false, page: 0)
        }
     }

    func stopRefresher() {
        refresher.endRefreshing()
     }
    
    //MARK: - IBAction Methods
    @IBAction func btnBackAction(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    //MARK: - UICollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrFeeds.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: MyUploadsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyUploadsCollectionViewCell", for: indexPath) as! MyUploadsCollectionViewCell
        
        cell.imgUpload.kf.setImage(with: URL(string: arrFeeds[indexPath.row].strImgUrl), placeholder: nil)
        cell.lblLikeCount.text = arrFeeds[indexPath.row].strLikeCount
        
        if strUserID == AuthModel.sharedInstance.strID {
            cell.btnDelete.tag = indexPath.row
            cell.btnDelete.addTarget(self, action: #selector(btnDeleteAction(_:)), for: .touchUpInside)
        }
        else {
            cell.btnDelete.isHidden = true
            cell.likeView.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/3.0 - 5
        let yourHeight = yourWidth
        
        return CGSize(width: yourWidth, height: yourHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedFeed = arrFeeds[indexPath.row]
        selectedIndexPath = indexPath
        self.performSegue(withIdentifier: "toPostDetail", sender: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        //case of archived notification view
        if self.currentPage == self.totalPageCount {
            //case when no more items are there to load
            self.isLoadMore = false
        }
        
        //get last element index
        let lastElement = arrFeeds.count - 1
        
        if indexPath.row == lastElement && isLoadMore {
            // handle your logic here to get more items, add it to dataSource and reload tableview
            
            self.currentPage = self.currentPage + 1
            callWebServiceForMyUploads(showLoader: false, page: currentPage)
        }
    }
    
    //MARK: - WebService Methods
    func callWebServiceForMyUploads(showLoader: Bool, page: Int) {
        
        if showLoader {
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        }
        
        var date = ""
        if arrFeeds.count > 0 && page == 0 {
            date = arrFeeds[0].strDate
        }
        
        let param = [
            "userId": strUserID,
            "page": page,
            "date": date
            ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Profile.userUploads, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                if self.arrFeeds.count == 0 {
                    self.viewNoRecord.isHidden = false
                }
            }
            else{
                if response!["status"].boolValue == true {
                    self.viewNoRecord.isHidden = true

                    FeedParser.parseHashTagFeedsList(response: response!) { (arr, totalPages, _) in

                        if page == 1 {
                            //if first time then set array
                            self.arrFeeds = arr
                        }
                        else {
                            if page == 0 {
                                self.arrFeeds.insert(contentsOf: arr, at: 0)
                            }
                            else {
                                //if next pages are loaded append the arrays to previous
                                self.arrFeeds.append(contentsOf: arr)
                            }
                        }

                        if page != 0 {
                            self.totalPageCount = Int(totalPages)!
                        }

                        if self.arrFeeds.count == 0 {
                            self.viewNoRecord.isHidden = false
                        }

                    }
                    self.collectionView.reloadData()
                }
                else {
//                    if page != 0 {
//                        self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
//                    }
                    if self.arrFeeds.count == 0 {
                        self.viewNoRecord.isHidden = false
                    }
                }
            }
        }
    }
    
    func callWebServiceForDeleteUpload(index: Int) {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [
            "postId": arrFeeds[index].strID
            ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Profile.deleteUpload, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    self.arrFeeds.remove(at: index)
                    self.collectionView.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }



    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        let objVC = segue.destination as! PostDetailViewController
//        objVC.strPostID = selectedFeed.strID
        objVC.arrFeedModel = arrFeeds
        objVC.selectedIndexPath = selectedIndexPath
    }
    

}
