//
//  ProfileViewController.swift
//  FreshersApp
//
//  Created by Apple on 24/07/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

        //MARK: - IBOutlets
        @IBOutlet weak var imgProfile: UIImageView!
        @IBOutlet weak var txtUsername: UITextField!
        @IBOutlet weak var txtBio: UITextView!
        @IBOutlet weak var imgMale: UIImageView!
        @IBOutlet weak var imgFemale: UIImageView!
        @IBOutlet weak var imgOther: UIImageView!
        @IBOutlet weak var tblView: UITableView!
        @IBOutlet weak var txtFullName: UITextField!
        @IBOutlet weak var txtLocation: UITextField!
        @IBOutlet weak var imgSelectedGender: UIImageView!
        @IBOutlet weak var lblSelectedGender: UILabel!
        @IBOutlet weak var selectedGenderView: UIView!
        @IBOutlet weak var selectGenderView: UIView!
        @IBOutlet weak var heightEditViewButtons:NSLayoutConstraint!
        //@IBOutlet weak var VerticalSpaceEditViewButtons:NSLayoutConstraint!

        //MARK: - Variable
        var imagePicker = UIImagePickerController()
        let panel = JKNotificationPanel()
        var objAuthModel = AuthModel()
        var strGender = kGenderMale
        var arrLocale = [LocaleModel]()
        let localePickerView = UIPickerView()
        var strSelectedLocale = ""
        var isEditMode = false
        var isImageChanged = false
        var arrFeeds = [FeedModel]()
        var selectedFeed = FeedModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtUsername.setIcon(#imageLiteral(resourceName: "username"))
        txtFullName.setIcon(#imageLiteral(resourceName: "username"))
        txtLocation.setIcon(#imageLiteral(resourceName: "location-1"))
        imagePicker.delegate = self
                
        heightEditViewButtons.constant = 0
        //VerticalSpaceEditViewButtons.constant = 0
        
        setupData()
        callWebServiceForUserProfile(showLoader: true)
        createPickerView()
        setEditMode()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Commons.callWebServiceToGetNotificationCount(tabBarController: self.tabBarController!, index: "")

        txtLocation.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        
        callWebServiceForUserProfile(showLoader: false)
    }
    
    //MARK: - Custom Methods
    func setupData() {
        
        imgProfile.kf.setImage(with: URL(string: AuthModel.sharedInstance.strProfileImage), placeholder:#imageLiteral(resourceName: "Default_Profile"))
        txtUsername.text = AuthModel.sharedInstance.strUsername
        txtFullName.text = AuthModel.sharedInstance.strName
        txtLocation.text = AuthModel.sharedInstance.strLocaleName
        
        for (index,value) in arrLocale.enumerated() {
            if value.strID == AuthModel.sharedInstance.strLocaleID {
                localePickerView.selectRow(index, inComponent: 0, animated: false)
            }
        }
        
        if Commons.isPremiumUser() {
            txtLocation.textColor = UIColor.black
            callWebServiceForLocaleList()
            txtLocation.isUserInteractionEnabled = true
        }
        
        //        txtDob.text = AuthModel.sharedInstance.strDob
        txtBio.text = AuthModel.sharedInstance.strBio
        strGender = AuthModel.sharedInstance.strGenderID
        
        arrFeeds.removeAll()
        for (index,value) in AuthModel.sharedInstance.strTopPostID.components(separatedBy: ",").enumerated() {
            
            let model = FeedModel()
            model.strID = value
            model.strImgUrl = AuthModel.sharedInstance.strTopPostImgUrl.components(separatedBy: ",")[index]
            model.strLikeCount = AuthModel.sharedInstance.strTopPostLikeCount
            
            arrFeeds.append(model)
        }
            
        setSelectedGender()
    }
    
    @objc func doneButtonClicked(_ sender: Any) {
        //your code when clicked on done
        print("Yuhuuuu")
        
        if strSelectedLocale == "" {
            strSelectedLocale = arrLocale[0].strID
            txtLocation.text = arrLocale[0].strName
        }
        
        Commons.callWebServiceForUpdateUserLocation(strLocaleID: strSelectedLocale, strLocaleName: txtLocation.text!) {
            print("Updated")
            UserDefaults.standard.set(true, forKey: "resetFeed")
//            UserDefaults.standard.set(1, forKey: kCurrentPageFeed)
//            UserDefaults.standard.set("-1", forKey: kSelectedFeedID)
        }
    }
    
    func setEditMode() {
        if isEditMode {
            txtFullName.isUserInteractionEnabled = true
            txtBio.isUserInteractionEnabled = true
            selectedGenderView.isHidden = true
            selectGenderView.isHidden = false
            heightEditViewButtons.constant = 50
            //VerticalSpaceEditViewButtons.constant = 15
        }
        else {
            txtFullName.isUserInteractionEnabled = false
            txtBio.isUserInteractionEnabled = false
            selectedGenderView.isHidden = false
            selectGenderView.isHidden = true
            heightEditViewButtons.constant = 0
            //VerticalSpaceEditViewButtons.constant = 0
        }
    }
    
    func setSelectedGender() {
        
        switch strGender {
        case kGenderMale:
            imgMale.image = #imageLiteral(resourceName: "male_selected")
            imgFemale.image = #imageLiteral(resourceName: "female_not_selected")
            imgOther.image = #imageLiteral(resourceName: "Other_not_selected")
            imgSelectedGender.image = #imageLiteral(resourceName: "male_selected")
            lblSelectedGender.text = "Male"
        case kGenderFemale:
            imgMale.image = #imageLiteral(resourceName: "male_not_selected")
            imgFemale.image = #imageLiteral(resourceName: "female_selected")
            imgOther.image = #imageLiteral(resourceName: "Other_not_selected")
            imgSelectedGender.image = #imageLiteral(resourceName: "female_selected")
            lblSelectedGender.text = "Female"
        case kGenderOther:
            imgMale.image = #imageLiteral(resourceName: "male_not_selected")
            imgFemale.image = #imageLiteral(resourceName: "female_not_selected")
            imgOther.image = #imageLiteral(resourceName: "Other_selected")
            imgSelectedGender.image = #imageLiteral(resourceName: "Other_selected")
            lblSelectedGender.text = "Other"
        default:
            imgMale.image = #imageLiteral(resourceName: "male_not_selected")
            imgFemale.image = #imageLiteral(resourceName: "female_not_selected")
            imgOther.image = #imageLiteral(resourceName: "Other_not_selected")
            imgSelectedGender.image = #imageLiteral(resourceName: "male_selected")
            lblSelectedGender.text = "Male"
        }
    }
    
    func createPickerView() {
        
        localePickerView.delegate = self
        txtLocation.inputView = localePickerView
    }
    
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        txtLocation.inputAccessoryView = toolBar
    }
    
    @objc func action() {
        view.endEditing(true)
    }
    
    /**
     This function opens the camera of device
     */
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            //case when device has a camera
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            //present camera
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            //case when device has no camera
            let alert  = UIAlertController(title: StringConstant.titleWarning, message: StringConstant.titleNoCamera, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: StringConstant.titleOK, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     This function opens the gallery of device
     */
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
        imagePicker.allowsEditing = true
        //present gallery
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    /**
     This function converts images into base64 and keep them into string
     */
    func convertImageToBase64(image: UIImage) -> String {
        var imgData = UIImageJPEGRepresentation(imgProfile.image!, 1.0)!
        if let imageData = imgProfile.image!.jpeg(.medium) {
            print(imageData.count)
            imgData = imageData
        }
        return imgData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    func openImageOnFullscreen() {
        
        self.performSegue(withIdentifier: "toFullScreen", sender: nil)
        
        //        let newImageView = UIImageView(image: imgProfile.image!)
        //        newImageView.frame = UIScreen.main.bounds
        //        newImageView.backgroundColor = .black
        //        newImageView.contentMode = .scaleAspectFit
        //        newImageView.isUserInteractionEnabled = true
        //        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage(sender:)))
        //        newImageView.addGestureRecognizer(tap)
        //
        //        UIView.transition(with: newImageView, duration: 0.25, options: .curveEaseOut, animations: {
        //            self.appDelegate.window!.addSubview(newImageView)
        //        }, completion: nil)
        
    }
    
    @objc func dismissFullscreenImage(sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
        
    func editProfileAction() {
        isEditMode = true
        setEditMode()
    }
    
//    @IBAction func btnEditAction(_ sender : UIButton) {
//        if isEditMode {
//            if (txtFullName.text?.isEmpty)! {
//                panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyUsername)
//            }
//            else if (txtBio.text?.isEmpty)! {
//                panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyBio)
//            }
//            else {
//                isEditMode = !isEditMode
//                setEditMode()
//                callWebServiceForUpdateUserProfile()
//            }
//        }
//        else {
//            isEditMode = !isEditMode
//            setEditMode()
//        }
//    }

    
    //MARK: - IBAction Methods
    @IBAction func btnCameraAction(_ sender : UIButton) {
        if isEditMode {
            //create alert view for choice of Camera or Gallery
            let alert = UIAlertController(title: StringConstant.titleSelectOptionMedia, message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: StringConstant.titleView, style: .default, handler: { _ in
                //open camera
                self.openImageOnFullscreen()
            }))
            
            alert.addAction(UIAlertAction(title: StringConstant.titleCamera, style: .default, handler: { _ in
                //open camera
                self.openCamera()
            }))
            
            alert.addAction(UIAlertAction(title: StringConstant.titleGallery, style: .default, handler: { _ in
                //open gallery
                self.openGallary()
            }))
            
            alert.addAction(UIAlertAction.init(title: StringConstant.titleCancel, style: .cancel, handler: nil))
            
            /*If you want work actionsheet on ipad
             then you have to use popoverPresentationController to present the actionsheet,
             otherwise app will crash on iPad */
            switch UIDevice.current.userInterfaceIdiom {
            case .pad:
                alert.popoverPresentationController?.sourceView = sender as? UIView
                alert.popoverPresentationController?.sourceRect = (sender as AnyObject).bounds
                alert.popoverPresentationController?.permittedArrowDirections = .up
            default:
                break
            }
            
            self.present(alert, animated: true, completion: nil)
        }
        else {
            openImageOnFullscreen()
        }
    }
        
    @IBAction func btnDoneAction(_ sender : UIButton) {
        if (txtFullName.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyUsername)
        }
        else if (txtBio.text?.isEmpty)! {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyBio)
        }
        else {
            callWebServiceForUpdateUserProfile()
        }
    }
    
    @IBAction func btnCancelAction(_ sender : UIButton) {
        isEditMode = !isEditMode
        setEditMode()
        setupData()
        self.isImageChanged = false
    }

    
    @IBAction func btnMaleAction(_ sender : UIButton) {
        if isEditMode {
            strGender = kGenderMale
            setSelectedGender()
        }
    }
    
    @IBAction func btnFemaleAction(_ sender : UIButton) {
        if isEditMode {
            strGender = kGenderFemale
            setSelectedGender()
        }
    }
    
    @IBAction func btnOtherAction(_ sender : UIButton) {
        if isEditMode {
            strGender = kGenderOther
            setSelectedGender()
        }
    }
            
    @IBAction func btnOpenPostAction(_ sender : UIButton) {
        self.performSegue(withIdentifier: "toPostDetail", sender: nil)
    }
    
    @IBAction func btnSettingAction(_ sender : UIButton) {
        let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        objVC.objProfile = self
        objVC.arrFeeds = arrFeeds
        self.navigationController?.pushViewController(objVC, animated: false)
    }
        
    //MARK: - UIPickerView Delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1 // number of session
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrLocale.count // number of dropdown items
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrLocale[row].strName // dropdown item
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        strSelectedLocale = arrLocale[row].strID // selected item
        txtLocation.text = arrLocale[row].strName
        //            callWebServiceForUpdateUserLocation()
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    /**
     This is pickercontroller delegate which is called when user selects any image from gallery/or saves any camera image
     */
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            //get the original image & set it as feedback image
            imgProfile.image = image
            self.isImageChanged = true
            self.dismiss(animated: false, completion: nil)
        }
        else{
            //case when image is not supported
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.titleNoSupportedImage)
            picker.popViewController(animated: true)
            print("Something went wrong in  image")
        }
    }
    
    /**
     This is pickercontroller delegate which is called when user cancel the image picker view
     */
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }
    
    //MARK: - WebService Methods
    func callWebServiceForUserProfile(showLoader: Bool) {
        
        if showLoader {
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        }
        
        let param = [
            "userId": AuthModel.sharedInstance.strID
            ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Profile.userProfile, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    AuthParser.parseProfileDetails(response: response!) { (_) in
                        self.setupData()
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForUpdateUserProfile() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [
            "name": txtFullName.text ?? "",
            "dob": AuthModel.sharedInstance.strDob, //txtDob.text ?? "",
            "bio": txtBio.text,
            "gender": strGender,
            "localeId": strSelectedLocale,
            "profileImage": isImageChanged ? convertImageToBase64(image: imgProfile.image!) : ""
            ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Profile.updateProfile, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    AuthParser.parseProfileDetails(response: response!) { (_) in
                        self.setupData()
                    }
                    
                    self.isEditMode = !self.isEditMode
                    self.setEditMode()
                    self.view.endEditing(true)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForLocaleList() {
        
        //        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [:] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Profile.localeList, param: param, withHeader: true ) { (response, errorMsg) in
            
            //            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    ProfileParser.parseLocaleList(response: response!) { (arr) in
                        self.arrLocale = arr
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
        
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "toFullScreen" {
            let vc = segue.destination as! FullScreenView
            vc.strImageUrl = AuthModel.sharedInstance.strProfileImage
        }
        
        if segue.identifier == "toPostDetail" {
            let vc = segue.destination as! PostDetailViewController
            vc.strPostID = selectedFeed.strID
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
