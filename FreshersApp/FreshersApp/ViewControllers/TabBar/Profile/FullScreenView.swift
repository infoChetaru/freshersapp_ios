//
//  FullScreenView.swift
//  FreshersApp
//
//  Created by Apple on 01/07/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class FullScreenView: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var imgViewForFullScreen: UIImageView!

    //MARK: - Variable
    var strImageUrl = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imgViewForFullScreen.kf.setImage(with: URL(string: strImageUrl), placeholder: nil)
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnBackAction(_ sender : UIButton) {
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
