//
//  MyDetailsTableViewCell.swift
//  FreshersApp
//
//  Created by Apple on 19/08/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class MyDetailsTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var imgCheckBox: UIImageView!
    @IBOutlet weak var lblLoginDetail: UILabel!
    @IBOutlet weak var btnSensitive: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
