//
//  MyUploadsCollectionViewCell.swift
//  FreshersApp
//
//  Created by Apple on 15/06/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class MyUploadsCollectionViewCell: UICollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var imgUpload: UIImageView!
    @IBOutlet weak var lblLikeCount: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var likeView: UIView!

}
