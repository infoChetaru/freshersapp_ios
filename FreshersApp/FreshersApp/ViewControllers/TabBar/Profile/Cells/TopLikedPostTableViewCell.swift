//
//  TopLikedPostTableViewCell.swift
//  FreshersApp
//
//  Created by Apple on 27/07/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class TopLikedPostTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var collectionView:UICollectionView!

    //MARK: - Variable
    var arrFeeds = [FeedModel]()
    var selectedFeed = FeedModel()
    var objSetting = SettingsViewController()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let flow = collectionView.collectionViewLayout as! UICollectionViewFlowLayout // If you create collectionView programmatically then just create this flow by UICollectionViewFlowLayout() and init a collectionView by this flow.
        
        let itemSpacing: CGFloat = 3
        let itemsInOneLine: CGFloat = 3
        flow.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let width = UIScreen.main.bounds.size.width - itemSpacing * CGFloat(itemsInOneLine - 1) //collectionView.frame.width is the same as  UIScreen.main.bounds.size.width here.
        flow.itemSize = CGSize(width: floor(width/itemsInOneLine), height: width/itemsInOneLine)
        flow.minimumInteritemSpacing = 5
        flow.minimumLineSpacing = 5
        collectionView.collectionViewLayout = flow

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - UICollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrFeeds.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: MyUploadsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyUploadsCollectionViewCell", for: indexPath) as! MyUploadsCollectionViewCell
        
        cell.imgUpload.kf.setImage(with: URL(string: arrFeeds[indexPath.row].strImgUrl), placeholder: nil)
        cell.lblLikeCount.text = arrFeeds[indexPath.row].strLikeCount
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        let yourWidth = collectionView.bounds.width/3.0 - 5
        //        let yourHeight = yourWidth
        
        return CGSize(width: 200, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedFeed = arrFeeds[indexPath.row]
//        self.performSegue(withIdentifier: "toPostDetail", sender: nil)
        objSetting.viewTopLikedPhoto(selectedFeed: selectedFeed, selectedIndexPath: indexPath)
    }

}
