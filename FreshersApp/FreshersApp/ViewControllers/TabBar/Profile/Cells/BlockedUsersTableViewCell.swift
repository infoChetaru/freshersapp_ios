//
//  BlockedUsersTableViewCell.swift
//  FreshersApp
//
//  Created by Apple on 09/07/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class BlockedUsersTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnUnblock: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
