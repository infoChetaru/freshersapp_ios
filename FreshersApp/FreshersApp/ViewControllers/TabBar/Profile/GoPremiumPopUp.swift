//
//  GoPremiumPopUp.swift
//  FreshersApp
//
//  Created by Apple on 30/06/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import StoreKit
import NVActivityIndicatorView
import JKNotificationPanel

let kStoreUrl = "https://buy.itunes.apple.com/verifyReceipt"
let kSandboxUrl = "https://sandbox.itunes.apple.com/verifyReceipt"

class GoPremiumPopUp: UIViewController, SKProductsRequestDelegate, SKPaymentTransactionObserver {

    //MARK: - IBOutlets
    @IBOutlet weak var navView:UIView!
    @IBOutlet weak var noPremium:UIView!
    @IBOutlet weak var yesPremium:UIView!
    @IBOutlet weak var syncPremium:UIView!
    @IBOutlet weak var lblPurchaseDate:UILabel!
    @IBOutlet weak var lblExpireDate:UILabel!
    @IBOutlet weak var lblDefaultLocaleText:UILabel!
    @IBOutlet weak var heightDefaultLocaleText:NSLayoutConstraint!
    @IBOutlet weak var lblAsterisk:UILabel!
    @IBOutlet weak var heightGoPremiumText:NSLayoutConstraint!

    //MARK: - Variables
    var showNavView = false
    var productIdentifier = "com.chetaru.FreshersApp.premiumUsers" //Get it from iTunes connect
    var productID = ""
    var productsRequest = SKProductsRequest()
    var iapProducts = [SKProduct]()
    let panel = JKNotificationPanel()
    var purchaseSuccess = false
    var strOriginalTransactionID = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        if showNavView {
            navView.isHidden = false
            heightGoPremiumText.constant = 0
        }
        else {
            navView.isHidden = true
            heightGoPremiumText.constant = 26
        }
        
        if Commons.isPremiumUser() {
            
            lblExpireDate.text = "Expiry Date: " + Commons.convertDateFormatter(date: AuthModel.sharedInstance.strExpiryDate, changeInto: "dd MMM yyyy", ChangeFrom: "yyyy-MM-dd HH:mm:ss")
            lblPurchaseDate.text = "Purchase Date: " + Commons.convertDateFormatter(date: AuthModel.sharedInstance.strSubscriptionDate, changeInto: "dd MMM yyyy", ChangeFrom: "yyyy-MM-dd HH:mm:ss")
            
            let date1 = Date()
            let date2 = Commons.convertStringToDate(date: AuthModel.sharedInstance.strExpiryDate, changeInto: "yyyy-MM-dd HH:mm:ss", ChangeFrom: "yyyy-MM-dd HH:mm:ss")

            yesPremium.isHidden = false
            noPremium.isHidden = true
            syncPremium.isHidden = true

//            if (date1 == date2) || (date1 < date2) {
//                //active, show already premium user screen
//                yesPremium.isHidden = false
//                noPremium.isHidden = true
//            }
//            else if date1 > date2 {
//                //expired, please check the receipt
//                callWebServiceForVerifyReceipt(url: kStoreUrl, restore: false)
//            }
            
            callWebServiceoGetPremiumStatusOfStatus()

        }
        else if UserDefaults.standard.value(forKey: "purchaseSuccess") != nil && UserDefaults.standard.value(forKey: "purchaseSuccess") as! Bool {
            yesPremium.isHidden = true
            noPremium.isHidden = true
            syncPremium.isHidden = false
        }
        else {
            yesPremium.isHidden = true
            noPremium.isHidden = false
            syncPremium.isHidden = true
            callWebServiceoGetDefaultLocale()
            
            callWebServiceoGetPremiumStatusOfStatus()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SKPaymentQueue.default().remove(self)
    }
        
    // MARK: - SKProductsRequest delegate
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        
        print(response.products)
        let count : Int = response.products.count
        if (count>0) {
            
            let validProduct: SKProduct = response.products[0] as SKProduct
            if (validProduct.productIdentifier == productIdentifier as String) {
                print(validProduct.localizedTitle)
                print(validProduct.localizedDescription)
                print(validProduct.price)
                self.purchaseProduct(product: validProduct)
            } else {
                print(validProduct.productIdentifier)
            }
        } else {
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            print("nothing")
        }
    }
    
    //If an error occurs, the code will go to this function
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        // Show some alert
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        for transaction:AnyObject in transactions {
            if let trans:SKPaymentTransaction = transaction as? SKPaymentTransaction{
                
                //self.dismissPurchaseBtn.isEnabled = true
                //self.restorePurchaseBtn.isEnabled = true
                //self.buyNowBtn.isEnabled = true
                switch trans.transactionState {
                case .purchased:
                    print("Product Purchased")
                    //Do unlocking etc stuff here in case of new purchase
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    self.purchaseSuccess = true
//                    self.callWebServiceForSaveTransaction(transactionID: trans.transactionIdentifier!)
                    self.callWebServiceForVerifyReceipt(url: kStoreUrl, restore: false)
                    break;
                case .failed:
                    print("Purchased Failed");
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Purchase Failed")
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                    NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                    break;
                case .restored:
                    print("Already Purchased")
                    //Do unlocking etc stuff here in case of restor
                    SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
//                    self.callWebServiceForSaveTransaction(transactionID: trans.transactionIdentifier!)
                    self.callWebServiceForVerifyReceipt(url: kStoreUrl, restore: true)
                default:
                    break;
                }
            }
            else {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            }
        }
        
    }

    // MARK: - Make purchase of a product
    func buyNowAction() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        if (SKPaymentQueue.canMakePayments()) {
            let productID:NSSet = NSSet(array: [productIdentifier as NSString]);
            let productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>);
            productsRequest.delegate = self;
            productsRequest.start();
            print("Fetching Products");
        } else {
            print("can't make purchases");
        }
    }

//    func fetchAvailableProducts() {
//        // Put here your IAP Products ID's
//        let productIdentifiers = NSSet(objects:
//            productIdentifier
//        )
//        guard let identifier = productIdentifiers as? Set<String> else { return }
//        productsRequest = SKProductsRequest(productIdentifiers: identifier)
//        productsRequest.delegate = self
//        productsRequest.start()
//    }

    func canMakePurchases() -> Bool {
        return SKPaymentQueue.canMakePayments()
    }
    
    func purchaseProduct(product: SKProduct) {
        if self.canMakePurchases() {
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(self)
            SKPaymentQueue.default().add(payment)
            productID = product.productIdentifier //also show loader
        }
        
//        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
    }
    
    func refreshReceipt() {
        let request = SKReceiptRefreshRequest()
        request.delegate = self // to be able to receive the results of this request, check the SKRequestDelegate protocol
        request.start()
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnCancelAction(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: false, completion: nil)
    }

    @IBAction func btnUpgradeAction(_ sender : UIButton) {
        //        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        //        fetchAvailableProducts()
        
        let alert = UIAlertController(title: nil, message: "Please select an option below to continue:", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (alertAction) in
            
        }
        
        let restoreAction = UIAlertAction(title: "Restore", style: .default) { (alertAction) in
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
            
            if (SKPaymentQueue.canMakePayments()) {
                SKPaymentQueue.default().add(self)
                SKPaymentQueue.default().restoreCompletedTransactions()
//                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                //show already premium user screen
//                self.yesPremium.isHidden = false
//                self.noPremium.isHidden  = true
            } else {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                // show error
                //show upgrade screen
                self.yesPremium.isHidden = true
                self.noPremium.isHidden  = false
                self.syncPremium.isHidden = true
            }
        }
        
        let buyAction = UIAlertAction(title: "Buy", style: .default) { (alertAction) in
            self.buyNowAction()
        }
        
        alert.addAction(buyAction)
        alert.addAction(restoreAction)
        
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
        
//        self.appDelegate.window?.rootViewController?.present(alert, animated:true, completion: nil)
        
    }
    
    @IBAction func btnBackAction(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSyncPaymentDetailsAction(_ sender : UIButton) {
        if strOriginalTransactionID != "" {
            callWebServiceForSaveTransaction(transactionID: strOriginalTransactionID)
        }
        else {
            self.callWebServiceForVerifyReceipt(url: kStoreUrl, restore: false)
        }
    }
    
    //MARK: - WebService Methods
    func callWebServiceForVerifyReceipt(url: String, restore: Bool) {
        
        if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
            FileManager.default.fileExists(atPath: appStoreReceiptURL.path) {

        //start loader
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
                    
        let SUBSCRIPTION_SECRET = "cb9e0c8f5bb545ba89528729a4855e81"
        let receiptPath = Bundle.main.appStoreReceiptURL?.path
        if FileManager.default.fileExists(atPath: receiptPath!){
            var receiptData:NSData?
            do{
                receiptData = try NSData(contentsOf: Bundle.main.appStoreReceiptURL!, options: NSData.ReadingOptions.alwaysMapped)
            }
            catch{
                print("ERROR: " + error.localizedDescription)
            }
            //let receiptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
            let base64encodedReceipt = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions.endLineWithCarriageReturn)

        //set param
        let param = ["receipt-data":base64encodedReceipt!,"password":SUBSCRIPTION_SECRET]

        WebServiceHandler.postWebService(url: url, param: param, withHeader: false ) { (response, errorMsg) in
            
            //stop loader
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                //show error message in case of no response
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                
                if self.purchaseSuccess {
                    //show retry button
                    UserDefaults.standard.set(true, forKey: "purchaseSuccess")
                }
            }
            else{
                //print(response)
                //"https://sandbox.itunes.apple.com/verifyReceipt"
                if response!["status"].stringValue == "21007" {
                    self.callWebServiceForVerifyReceipt(url: kSandboxUrl, restore: restore)
                }
                else {
                    print("got")
                    self.purchaseSuccess = false
                    UserDefaults.standard.set(false, forKey: "purchaseSuccess")

                    if response!["pending_renewal_info"].arrayValue.count > 0 {
                        
                        self.callWebServiceForSaveTransaction(transactionID: response!["pending_renewal_info"].arrayValue[0]["original_transaction_id"].stringValue)

                        /*var expDateArr = response!["latest_receipt_info"].arrayValue.last! ["expires_date"].stringValue.components(separatedBy: " ")
                        expDateArr.removeLast()
                        
                        var originalPurchaseDate = response!["latest_receipt_info"].arrayValue.last! ["original_purchase_date"].stringValue.components(separatedBy: " ")
                        originalPurchaseDate.removeLast()
                        
                        if response!["pending_renewal_info"].arrayValue[0]["expiration_intent"].exists() {
                            //subscription expired
                            
                            self.yesPremium.isHidden = true
                            self.noPremium.isHidden = false

//                            self.callWebServiceForSaveTransaction(transactionID: response!["pending_renewal_info"].arrayValue[0]["original_transaction_id"].stringValue, expiryDate: expDateArr.joined(separator: " "), paymentStatus: "2", purchaseDate: originalPurchaseDate.joined(separator: " "))
                            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "There is no active subscription plan for your account. Please upgrade your plan.")

                        }
                        else {
                            //active subscription

                            self.yesPremium.isHidden = false
                            self.noPremium.isHidden = true

//                            self.callWebServiceForSaveTransaction(transactionID: response!["pending_renewal_info"].arrayValue[0]["original_transaction_id"].stringValue, expiryDate: expDateArr.joined(separator: " "), paymentStatus: "1", purchaseDate: originalPurchaseDate.joined(separator: " "))
                        }*/
                    }
                }
            }
            }
        }
        }
        else {
            print("No receipt found even though there is an indication something has been purchased before")
            print("^No receipt found. Need to refresh receipt.")
            self.refreshReceipt()
            self.yesPremium.isHidden = true
            self.noPremium.isHidden = false
            self.syncPremium.isHidden = true
            
            if purchaseSuccess {
                //show retry button
                UserDefaults.standard.set(true, forKey: "purchaseSuccess")
                self.yesPremium.isHidden = true
                self.noPremium.isHidden = true
                self.syncPremium.isHidden = false
            }
        }
    }
    
//    func callWebServiceForSaveTransaction(transactionID: String, expiryDate: String, paymentStatus: String, purchaseDate: String) {
    func callWebServiceForSaveTransaction(transactionID: String) {

        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)

        let param = [
            "transactionId": transactionID
            ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Payment.goPremium, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                
                if self.purchaseSuccess {
                    //show retry button with transaction id
                    self.strOriginalTransactionID = transactionID
                    UserDefaults.standard.set(true, forKey: "purchaseSuccess")
                }
            }
            else{
                print(response as Any)
                if response!["status"].boolValue == true {
                    
                    self.purchaseSuccess = false
                    UserDefaults.standard.set(false, forKey: "purchaseSuccess")

                    self.yesPremium.isHidden = false
                    self.noPremium.isHidden = true
                    self.syncPremium.isHidden = true
                    AuthModel.sharedInstance.isPremiumUser = true

                    self.lblPurchaseDate.text = "It may take up to 30 minutes for premium"//29
                    self.lblExpireDate.text = " features to become available to you."//48

                    /* if paymentStatus == "2" {
                        //expired move to upgrade screen
                        self.yesPremium.isHidden = true
                        self.noPremium.isHidden = false
                        AuthModel.sharedInstance.isPremiumUser = false
                    }
                    else {
                        //payment success move to already premium user screen
                        AuthModel.sharedInstance.strExpiryDate = expiryDate
                        AuthModel.sharedInstance.strSubscriptionDate = purchaseDate
                        AuthModel.sharedInstance.isPremiumUser = true

                        self.yesPremium.isHidden = false
                        self.noPremium.isHidden = true
                        
                        self.lblExpireDate.text = "Expiry Date: " + Commons.convertDateFormatter(date: expiryDate, changeInto: "dd MMM yyyy", ChangeFrom: "yyyy-MM-dd HH:mm:ss")
                        self.lblPurchaseDate.text = "Purchase Date: " + Commons.convertDateFormatter(date: purchaseDate, changeInto: "dd MMM yyyy", ChangeFrom: "yyyy-MM-dd HH:mm:ss")

                    }*/
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    func callWebServiceoGetDefaultLocale() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [:] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Payment.getDefaultLocale, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                        
                    if response!["data"].stringValue == "" {
                        self.lblAsterisk.isHidden = true
                        self.lblDefaultLocaleText.text = ""
                    }
                    else {
                        self.lblAsterisk.isHidden = false
                        self.lblDefaultLocaleText.text = response!["data"].stringValue + " is currently the only available location"
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceoGetPremiumStatusOfStatus() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [:] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Payment.getPremiumStatusOfUser, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    if response!["data"]["primeUser"].stringValue == "1" && response!["data"]["transactionId"].stringValue != ""{
                        //late response from apple server
                        self.yesPremium.isHidden = false
                        self.noPremium.isHidden = true
                        self.syncPremium.isHidden = true
                        AuthModel.sharedInstance.isPremiumUser = true

                        self.lblPurchaseDate.text = "It may take up to 30 minutes for premium"//29
                        self.lblExpireDate.text = " features to become available to you."//48
                    }
                    else if response!["data"]["primeUser"].stringValue == "1" {
                        //expired move to upgrade screen
                        self.yesPremium.isHidden = true
                        self.noPremium.isHidden = false
                        self.syncPremium.isHidden = true
                        AuthModel.sharedInstance.isPremiumUser = false
                    }
                    else {
                        //payment success move to already premium user screen
                        AuthModel.sharedInstance.strExpiryDate = response!["data"]["expiryDate"].stringValue
                        AuthModel.sharedInstance.strSubscriptionDate = response!["data"]["purchaseDate"].stringValue
                        AuthModel.sharedInstance.isPremiumUser = true
                        
                        self.yesPremium.isHidden = false
                        self.noPremium.isHidden = true
                        self.syncPremium.isHidden = true
                        
                        self.lblExpireDate.text = "Expiry Date: " + Commons.convertDateFormatter(date: response!["data"]["expiryDate"].stringValue, changeInto: "dd MMM yyyy", ChangeFrom: "yyyy-MM-dd HH:mm:ss")
                        self.lblPurchaseDate.text = "Purchase Date: " + Commons.convertDateFormatter(date: response!["data"]["purchaseDate"].stringValue, changeInto: "dd MMM yyyy", ChangeFrom: "yyyy-MM-dd HH:mm:ss")
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
