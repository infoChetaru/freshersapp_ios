//
//  CameraViewController.swift
//  FreshersApp
//
//  Created by Apple on 17/04/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import AVFoundation

class CameraViewController: UIViewController, UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var cameraContainerView:UIView!
    @IBOutlet weak var btnFlash:UIButton!

    //MARK: - Variable
    var imagePicker = UIImagePickerController()
    let panel = JKNotificationPanel()
    var strOriginalPostID = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        //hide navigation bar
        self.navigationController?.setNavigationBarHidden(true, animated: false)

        imagePicker.delegate = self
        
        addImagePickerToContainerView()
        
        print("strOriginalPostID: ", strOriginalPostID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        Commons.callWebServiceToGetNotificationCount(tabBarController: self.tabBarController!, index: "")
    }
    
    //MARK: - Custom Functions
    func addImagePickerToContainerView(){

        imagePicker = UIImagePickerController()
        if UIImagePickerController.isCameraDeviceAvailable( UIImagePickerControllerCameraDevice.front) {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera

            //add as a childviewcontroller
            addChildViewController(imagePicker)

            // Add the child's View as a subview
            self.cameraContainerView.addSubview((imagePicker.view)!)
            imagePicker.view.frame = cameraContainerView.bounds
            imagePicker.allowsEditing = false
            imagePicker.showsCameraControls = false
            imagePicker.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        }
    }

    /**
     *This function opens camera of device
     */
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            //when type is camera
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            //case when device has no camera
            let alert  = UIAlertController(title: StringConstant.titleWarning, message: StringConstant.titleNoCamera, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: StringConstant.titleOK, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    /**
     *This function opens gallery of device
     */
    func openGallery()
    {
        let imagePicker = UIImagePickerController()

        imagePicker.delegate = self
        imagePicker.view.changeTintColor(color: ColorCodeConstant.themeColor)
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    /**
     * ImagePicker delegate method called when user selects any image from Camera/Gallery
     */
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        //get original image selected
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                        
             let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "PostFeedViewController") as! PostFeedViewController
            objVC.strImage = image
            objVC.strOriginalPostID = strOriginalPostID
            self.navigationController?.pushViewController(objVC, animated: true)
            
            //dismiss the picker
            self.dismiss(animated: false, completion: nil)
        }
            
        else{
            //show error if image not supported
            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.titleNoSupportedImage)
            picker.popViewController(animated: true)
            print("Something went wrong in  image")
        }
        
        
    }
    
    /**
     * ImagePicker delegate method called when user cancels  image selection view
     */
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated:  true, completion: nil)
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnCameraClickAction(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            imagePicker.takePicture()
        }else{
            //Camera not available.
        }
    }
    
    @IBAction func btnFlashAction(_ sender: Any) {
        if imagePicker.cameraFlashMode == .on {
            imagePicker.cameraFlashMode = .off
            btnFlash.setImage(#imageLiteral(resourceName: "flashoff"), for: .normal)
        }
        else {
            imagePicker.cameraFlashMode = .on
            btnFlash.setImage(#imageLiteral(resourceName: "flash"), for: .normal)
        }
    }
    
    @IBAction func btnFlipCameraAction(_ sender: Any) {

        if imagePicker.cameraDevice == .front {
            imagePicker.cameraDevice = .rear
        }
        else {
            imagePicker.cameraDevice = .front
        }
    }

    @IBAction func btnGalleryAction(_ sender: Any) {
        openGallery()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
