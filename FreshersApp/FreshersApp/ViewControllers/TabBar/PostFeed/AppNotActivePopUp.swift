//
//  AppNotActivePopUp.swift
//  FreshersApp
//
//  Created by Apple on 21/04/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel

class AppNotActivePopUp: UIViewController {

    //MARK: - Variables
    let panel = JKNotificationPanel()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnHomeAction(_ sender : UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }

    @IBAction func btnUpgradeAction(_ sender : UIButton) {
        self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: "Coming Soon!")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
