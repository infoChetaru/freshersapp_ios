//
//  PostFeedViewController.swift
//  FreshersApp
//
//  Created by Apple on 30/04/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView
import CoreLocation

class PostFeedViewController: UIViewController, SJTwitterTagInputTextViewDelegate, CLLocationManagerDelegate {

    //MARK: - IBOutlets
    @IBOutlet var tagInputView : SJTwitterTagInputTextView!
    @IBOutlet weak var imgPost: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var imgPostBlur: UIImageView!
    @IBOutlet weak var heightConstraintForTopSpacing: NSLayoutConstraint!

    //MARK: - Variable
    let panel = JKNotificationPanel()
    var strImage = UIImage()
    var arrTagPeople = [TagModel]()
    var arrFilteredTagPeople = [TagModel]()
    var arrHashTag = [TagModel]()
    var arrFilteredHashTag = [TagModel]()
    let locationManager = CLLocationManager()
    var strOriginalPostID = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        tagInputView.delegate = self
        setupData()
    }
    
    //MARK: - Custom Methods
    /**
     * Set ups the UI
     */
    func setupData() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }

        imgPost.contentMode = .scaleAspectFit
        imgPost.clipsToBounds = true
        imgPost.image = strImage
        imgPostBlur.image = strImage

        if UIDevice.current.hasNotch {
            //... consider notch
            if heightConstraintForTopSpacing != nil {
                heightConstraintForTopSpacing.constant = 80
            }
        } else {
            //... don't have to consider notch
            if heightConstraintForTopSpacing != nil {
                heightConstraintForTopSpacing.constant = 50
            }
        }

        callWebServiceForTagListing()                
    }

    /**
     This function converts images into base64 and keep them into string
     */
    func convertImageToBase64(image: UIImage) -> String {
        var imgData = UIImageJPEGRepresentation(imgPost.image!, 1.0)!
        if let imageData = imgPost.image!.jpeg(.medium) {
            print(imageData.count)
            imgData = imageData
        }
        return imgData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }

    //MARK: - SJTwitterTagInputTextViewDelegate
    func didSearchWithAtTag(tagString: String) {
        arrFilteredTagPeople = arrTagPeople.filter { (item: TagModel) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.strName.range(of: tagString, options: .caseInsensitive, range: nil, locale: nil) != nil
        }

        tagInputView.allObjectList = arrFilteredTagPeople
        
    }
    
    func didSearchWithHashTag(tagString: String) {
        arrFilteredHashTag = arrHashTag.filter { (item: TagModel) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.strName.range(of: tagString, options: .caseInsensitive, range: nil, locale: nil) != nil
        }

        tagInputView.allObjectList = arrFilteredHashTag
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("All @ Tags : ", tagInputView.getAllAtTags())
        print("All # Tags : ", tagInputView.getAllHashTags())
        
        print("All @ Tags IDS : ", tagInputView.getAllAtTagsIDS())
        print("All # Tags IDS : ", tagInputView.getAllHashTagsIDS())
        
        print(tagInputView.textView.text)

    }
    
    //MARK: - IBAction Methods
    @IBAction func btnBackAction(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnPostAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        if !Commons.isPremiumUser() {
            // initialise a pop up for using later
            let alertController = UIAlertController(title: StringConstant.titleUserLocation, message: StringConstant.titleGoToSettingsPost, preferredStyle: .alert)
            let settingsAction = UIAlertAction(title: StringConstant.titleSettings, style: .default) { (_) -> Void in
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in })
                 }
            }
            let cancelAction = UIAlertAction(title: StringConstant.titleCancel, style: .default, handler: nil)
            alertController.addAction(cancelAction)
            alertController.addAction(settingsAction)

            // check the permission status
            switch(CLLocationManager.authorizationStatus()) {
                case .authorizedAlways, .authorizedWhenInUse:
                    print("Authorize.")
                    if locationManager.location != nil {
                        // get the user location
                        self.callWebServiceToPostData(lat: Double((locationManager.location?.coordinate.latitude)!), long: Double((locationManager.location?.coordinate.longitude)!))
                    }
                case .notDetermined, .restricted, .denied:
                    // redirect the users to settings
                    self.present(alertController, animated: true, completion: nil)
            }
        }
        else {
            self.callWebServiceToPostData(lat: Double(0.0), long: Double(0.0))
        }
        
    }
    
    //MARK: - CLLocationManager delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
//        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }

    //MARK: - WebService Methods
    func callWebServiceForTagListing() {
                        
        activityIndicator.startAnimating()
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.PostFeed.tagList, param: [:], withHeader: true ) { (response, errorMsg) in
            
            self.activityIndicator.stopAnimating()
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
//                self.callWebServiceForTagListing()
            }
            else{
                if response!["status"].boolValue == true {
                    FeedParser.parseTagList(response: response!) { (arrPeople, arrHash) in
                        self.arrTagPeople = arrPeople
                        self.arrHashTag = arrHash
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
//                    self.callWebServiceForTagListing()
                }
            }
        }
    }
    
    func callWebServiceToPostData(lat: Double, long: Double) {
                    
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
            
        var caption = tagInputView.textView.text ?? ""

        var arrAtTagsIds = [String]()
        arrAtTagsIds = tagInputView.getAllAtTagsIDS()
        
        var arrAtTagsNames = [String]()
        arrAtTagsNames = tagInputView.getAllAtTags()
        
        var tagUserIds = [String]()
        for value in arrAtTagsIds {
            for valueMain in arrTagPeople {
                if value.replacingOccurrences(of: "@", with: "") == valueMain.strID{
                    if arrAtTagsNames.contains("@" + valueMain.strName) {
                        tagUserIds.append(valueMain.strID)
                        caption = caption.replacingOccurrences(of: "@" + valueMain.strName, with: "@"+valueMain.strID)
                    }
                }
            }
        }
        
        var arrHashTagsIds = [String]()
        arrHashTagsIds = tagInputView.getAllHashTagsIDS()
        
        var arrHashTagsNames = [String]()
        arrHashTagsNames = tagInputView.getAllHashTags()

        var existingHashTagIds = [String]()
        for value in arrHashTagsIds {
            for valueMain in arrHashTag {
                if value.replacingOccurrences(of: "#", with: "") == valueMain.strID{
                    if arrHashTagsNames.contains("#" + valueMain.strName) {
                        
                        let index = arrHashTagsNames.index(of: "#" + valueMain.strName)
                        arrHashTagsNames.remove(at: index!)
                        existingHashTagIds.append(valueMain.strID)
                        
                    }
                }
            }
        }

        var newHashTags = [String]()
        for value in arrHashTagsNames {
                newHashTags.append(value.replacingOccurrences(of: "#", with: ""))
        }
        
        
        
        let param = [
            "caption": caption,//tagInputView.textView.text ?? "",
            "tagUserId": tagUserIds,
            "existingHashTagId": existingHashTagIds,
            "newHashTags": newHashTags,
            "postImage": self.convertImageToBase64(image: imgPost.image!),
            "latitude": String(lat),
            "longitude": String(long),
            "replyPost": strOriginalPostID == "" ? "0" : "1",
            "originalPostId": strOriginalPostID
            ] as [String : Any]
        
        let paramToprint = [
            "caption": caption,//tagInputView.textView.text ?? "",
            "tagUserId": tagUserIds,
            "existingHashTagId": existingHashTagIds,
            "newHashTags": newHashTags,
            "postImage": "",
            "latitude": String(lat),
            "longitude": String(long),
            "replyPost": strOriginalPostID == "" ? "0" : "1",
            "originalPostId": strOriginalPostID
            ] as [String : Any]

        print(paramToprint)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.PostFeed.addPost, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: response!["message"].stringValue)
                    if self.strOriginalPostID != "" {
                        self.tabBarController?.selectedIndex = 0
                    }
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    if response!["data"].stringValue == "failed-location"{
                         let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "AppNotActivePopUp") as! AppNotActivePopUp
                        self.navigationController?.pushViewController(objVC, animated: false)
                    }
                    else {
                        self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                    }
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
