//
//  CustomTableViewCell.swift
//  FreshersApp
//
//  Created by Apple on 29/04/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    var isAtTag = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // Here you can customize the appearance of your cell
    override func layoutSubviews() {
        super.layoutSubviews()
        // Customize imageView like you need
        
        if isAtTag {
            self.imageView?.frame = CGRect(x: 20,y: 5,width: 30,height: 30)
            self.imageView?.contentMode = UIViewContentMode.scaleAspectFill
            self.textLabel?.frame = CGRect(x: 60, y: 10, width: self.frame.width - 60, height: 20)
        }
        else {
            self.textLabel?.frame = CGRect(x: 20, y: 10, width: self.frame.width - 20, height: 20)
        }
    }

}
