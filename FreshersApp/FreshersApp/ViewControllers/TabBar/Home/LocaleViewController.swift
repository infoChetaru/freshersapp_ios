//
//  LocaleViewController.swift
//  FreshersApp
//
//  Created by Apple on 15/07/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class LocaleViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var tblLocale: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!

    //MARK: - Variables
    let panel = JKNotificationPanel()
    var arrLocale = [LocaleModel]()
    var arrFilteredLocale = [LocaleModel]()
    var strSelectedLocaleID = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        setupData()
        callWebServiceForLocaleList()
    }
    
    //MARK: - Custom Methods
    /**
     * Set ups the UI
     */
    func setupData() {
        searchBar.delegate = self
        searchBar.backgroundImage = UIImage()

        tblLocale.tableFooterView = UIView()
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnBackAction(_ sender : UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    //MARK: - UISearchBar Delegates
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        
        arrFilteredLocale = searchText.isEmpty ? arrLocale : arrLocale.filter { (item: LocaleModel) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.strName.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }

        tblLocale.reloadData()
    }
    
    //MARK: - TableView data source and delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrFilteredLocale.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : LocaleTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LocaleTableViewCell") as! LocaleTableViewCell
        
        cell.selectionStyle = .none
        
        if strSelectedLocaleID == arrFilteredLocale[indexPath.row].strID {
//            cell.bgView.layer.borderWidth = 1.0
//            cell.bgView.backgroundColor = UIColor.clear
            
            cell.bgView.backgroundColor = UIColor.white
            cell.lblName.textColor = ColorCodeConstant.themeColor
            cell.lblCountry.textColor = ColorCodeConstant.themeColor
            cell.imgLocation.tintColor = ColorCodeConstant.themeColor
        }
        else {
//            cell.bgView.layer.borderWidth = 0.0
//            cell.bgView.backgroundColor = UIColor.init(white: 1, alpha: 0.20)
            
            cell.bgView.backgroundColor = UIColor.init(white: 1, alpha: 0.20)
            cell.lblName.textColor = UIColor.white
            cell.lblCountry.textColor = UIColor.white
            cell.imgLocation.tintColor = UIColor.white
        }
        
        cell.lblName.text = arrFilteredLocale[indexPath.row].strName
        cell.lblCountry.text = arrFilteredLocale[indexPath.row].strCountryName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let navController = ((presentingViewController as? UITabBarController)?.selectedViewController as? UINavigationController) {
           let presenter = navController.topViewController as! HomeViewController
//            presenter.selectedLocale = self.arrFilteredLocale[indexPath.row]
            
            Commons.callWebServiceForUpdateUserLocation(strLocaleID: self.arrFilteredLocale[indexPath.row].strID, strLocaleName: self.arrFilteredLocale[indexPath.row].strName) {
                presenter.currentPage = 1
                presenter.refreshFeed()
            }
        }
        
        if let navController = (presentingViewController as? UINavigationController) {
            
            if let tabController = ((navController.topViewController as? UITabBarController)?.selectedViewController as? UINavigationController) {
                let presenter = tabController.topViewController as! HomeViewController
                
                Commons.callWebServiceForUpdateUserLocation(strLocaleID: self.arrFilteredLocale[indexPath.row].strID, strLocaleName: self.arrFilteredLocale[indexPath.row].strName) {
                    presenter.currentPage = 1
                    presenter.refreshFeed()
                }
            }
            else {
                let presenter = navController.topViewController as! HomeViewController
                //            presenter.selectedLocale = self.arrFilteredLocale[indexPath.row]
                
                Commons.callWebServiceForUpdateUserLocation(strLocaleID: self.arrFilteredLocale[indexPath.row].strID, strLocaleName: self.arrFilteredLocale[indexPath.row].strName) {
                    presenter.currentPage = 1
                    presenter.refreshFeed()
                }
            }
        }

        dismiss(animated: false, completion: nil)
    }
    
    //MARK: - WebService Methods
    func callWebServiceForLocaleList() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [:] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Profile.localeList, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    ProfileParser.parseLocaleList(response: response!) { (arr) in
                        self.arrLocale = arr
                        self.arrFilteredLocale = arr
                    }
                    
                    self.tblLocale.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
