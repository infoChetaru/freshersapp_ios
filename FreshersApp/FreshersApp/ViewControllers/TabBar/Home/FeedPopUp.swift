//
//  FeedPopupView.swift
//  FreshersApp
//
//  Created by Apple on 06/05/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class FeedPopUp: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var btnBlockImage:UIButton!
    @IBOutlet weak var lblBlock:UILabel!
    @IBOutlet weak var btnBlock:UIButton!

    //MARK: - Variable
    var objFeedModel = FeedModel()
    let panel = JKNotificationPanel()
    var objHomeModel = HomeViewController()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if objFeedModel.strUserID == AuthModel.sharedInstance.strID || objFeedModel.strRoleID == "1" {
            btnBlockImage.isHidden = true
            lblBlock.isHidden = true
            btnBlock.isHidden = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        btnCancelAction(self)
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnCancelAction(_ sender: Any) {
//        self.dismiss(animated: false, completion: {
//            self.objHomeModel.removedBlockedUserPost(blockedUserID: self.objFeedModel.strUserID)
//        })
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnBlockAction(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: (StringConstant.titleSureBlock + objFeedModel.strUsername + "?"), preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: StringConstant.titleYes, style: .default, handler: { (action) in
            self.callWebServiceToBlock()
        }))
        alert.addAction(UIAlertAction(title: StringConstant.titleNo, style: .cancel, handler: { (action) in
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func btnTermsOfUseAction(_ sender: Any) {
        guard let url = URL(string: kTermsOfServicesURL) else { return }
        UIApplication.shared.openURL(url)
    }

    @IBAction func btnPrivacyPolicyAction(_ sender: Any) {
        guard let url = URL(string: kPrivacyPolicyURL) else { return }
        UIApplication.shared.openURL(url)
    }
    
    @IBAction func btnReportAction(_ sender: Any) {
        let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "ContactUsPopUp") as! ContactUsPopUp
        objVC.objFeedModel = objFeedModel
        objVC.modalPresentationStyle = .overCurrentContext
        self.present(objVC, animated: false, completion: nil)
    }

    @IBAction func btnContactUsAction(_ sender: Any) {
        let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "ContactUsPopUp") as! ContactUsPopUp
        objVC.objFeedModel = objFeedModel
        objVC.modalPresentationStyle = .overCurrentContext
        self.present(objVC, animated: false, completion: nil)
    }

    //MARK: - WebService Methods
    func callWebServiceToBlock() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [
            "blockUserId": objFeedModel.strUserID,
            "type": "1"
            ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Feed.blockUser, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    self.panel.showNotify(withStatus: .success, inView: self.appDelegate.window!, title: response!["message"].stringValue)
                    self.btnCancelAction(self)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
