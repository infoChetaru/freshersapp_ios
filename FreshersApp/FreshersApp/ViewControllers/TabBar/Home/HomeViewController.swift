//
//  HomeViewController.swift
//  FreshersApp
//
//  Created by Apple on 05/05/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView
import CoreLocation
import ActiveLabel
import AudioToolbox.AudioServices
import AVFoundation

class HomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, CLLocationManagerDelegate, UIGestureRecognizerDelegate {
    
    //MARK: - IBOutlets
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var noPostsView:UIView!
    
    //MARK: - Variable
    let panel = JKNotificationPanel()
    let locationManager = CLLocationManager()
    var arrFeeds = [FeedModel]()
    var currentPage = 1
    var isLoadMore = true
    var totalPageCount = 1
    var lat = 0.0
    var long = 0.0
    var firstTime = true
    var strSelectedUserProfile = ""
    var isFromConnect = false
    var selectedLocale = LocaleModel()
    var showNotActive = false
    var strUserConnectStatus = ""
    var strUserConnectID = ""
    var strOriginalPostID = ""
    var selectedIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //hide navigation bar
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)

        setupData()
        
        if Commons.isPremiumUser() {
            callWebServiceForFeedListing()
        }
        
        selectedLocale.strID = AuthModel.sharedInstance.strLocaleID
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        Commons.callWebServiceToGetNotificationCount(tabBarController: self.tabBarController!, index: "")
        //        _ = self.tabBarController?.selectedIndex = 2
        
        if UserDefaults.standard.value(forKey: "resetFeed") != nil &&  UserDefaults.standard.value(forKey: "resetFeed") as! Bool{
            currentPage = 1
            collectionView.contentOffset = .zero
            self.selectedIndex = 0
            UserDefaults.standard.set(false, forKey: "resetFeed")
        }
        
        if !isLoadMore && arrFeeds.count != 0 {
            callWebServiceForFeedListingRefresh()
        }
        
        if Commons.isPremiumUser() {
            if self.selectedIndex > 0 {
                self.collectionView.scrollToItem(at:IndexPath(item: self.selectedIndex, section: 0), at: .centeredHorizontally, animated: false)
                selectedIndex = 0
            }
            else {
                collectionView.contentOffset = .zero
            }
            selectedLocale.strID = AuthModel.sharedInstance.strLocaleID
//            callWebServiceForFeedListing()
        }
//        else if selectedLocale.strID != AuthModel.sharedInstance.strLocaleID {
//
//            collectionView.contentOffset = .zero
//            selectedLocale.strID = AuthModel.sharedInstance.strLocaleID
//            callWebServiceForFeedListing()
//        }
        else {
            var msg = ""
            var title = ""
            
            if CLLocationManager.locationServicesEnabled() {
                print("CLLocationManager.locationServicesEnabled()")
                msg = StringConstant.titleAllowLocation
                title = StringConstant.titleLocationDenied
            }
            else {
                print("Not CLLocationManager.locationServicesEnabled()")
                msg = StringConstant.titleGoToSettingsView
                title = StringConstant.titleUserLocation
            }
            
            // initialise a pop up for using later
            let alertController = UIAlertController(title: title, message:msg, preferredStyle: .alert)
            let settingsAction = UIAlertAction(title: StringConstant.titleSettings, style: .default) { (_) -> Void in
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in })
                    
                }
            }
            
            let cancelAction = UIAlertAction(title: StringConstant.titleCancel, style: .default) { (_) -> Void in
                self.firstTime = false
            }
            
            alertController.addAction(cancelAction)
            alertController.addAction(settingsAction)
            
            // check the permission status
            switch(CLLocationManager.authorizationStatus()) {
            case .authorizedAlways, .authorizedWhenInUse:
                print("Authorize.")
                // get the user location
                
                self.firstTime = true
                //            self.lat = Double((locationManager.location?.coordinate.latitude)!)
                //            self.long = Double((locationManager.location?.coordinate.longitude)!)
                //            self.callWebServiceForFeedListing()
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
            case .notDetermined, .restricted, .denied:
                // redirect the users to settings
                
                if CLLocationManager.locationServicesEnabled() {
                    allowLocationPermission()
                }
                else {
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        selectedIndex = collectionView.indexPathsForVisibleItems[0].row
    }
    
    //MARK: - Custom Methods
    /**
     * Set ups the UI
     */
    func setupData() {

//        allowLocationPermission()
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = layout
        
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        
        let lpgr : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action:#selector(handleLongPress(gestureRecognizer:)))
        lpgr.minimumPressDuration = 0.5
        lpgr.delegate = self
        lpgr.delaysTouchesBegan = true
        self.collectionView?.addGestureRecognizer(lpgr)
        
    }
    
    func refreshFeed() {
        collectionView.contentOffset = .zero
        callWebServiceForFeedListing()
    }
    
    func allowLocationPermission() {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    @objc func btnLikeAction(_ sender: UIButton) {
        print("Like")
        
//        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
        AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) { }

        arrFeeds[sender.tag].isLiked = !arrFeeds[sender.tag].isLiked
        
        if arrFeeds[sender.tag].isLiked {
            sender.setBackgroundImage(#imageLiteral(resourceName: "liked"), for: .normal)
            callWebServiceForPostLike(postId: arrFeeds[sender.tag].strID, likeStatus: kLike, sender: sender)
        }
        else {
            sender.setBackgroundImage(#imageLiteral(resourceName: "like"), for: .normal)
            callWebServiceForPostLike(postId: arrFeeds[sender.tag].strID, likeStatus: kUnLike, sender: sender)
        }
    }
    
    @objc func btnReplyAction(_ sender: UIButton) {
        tabBarController?.selectedIndex = 2
        var postNowTab = (self.tabBarController?.viewControllers![2] as? UINavigationController)?.topViewController as! CameraViewController
        postNowTab.strOriginalPostID = arrFeeds[sender.tag].strID
    }
    
    @objc func btnConnectAction(_ sender: UIButton) {
        print("Connect")
//        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
        AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) { }

        self.strSelectedUserProfile = arrFeeds[sender.tag].strUserID
        self.isFromConnect = true
        self.strUserConnectStatus = arrFeeds[sender.tag].strUserConnectStatus
        self.strUserConnectID = arrFeeds[sender.tag].strConnectionID
        
        if self.strSelectedUserProfile == AuthModel.sharedInstance.strID {
            self.selectedIndex = sender.tag
            self.performSegue(withIdentifier: "toMyProfile", sender: nil)
        }
        else {
            self.selectedIndex = sender.tag
            callWebServiceForUserConnectStatus(showLoader: true, viewOriginalPost: false)
//            self.performSegue(withIdentifier: "toUserProfile", sender: nil)
        }
    }
    
    @objc func handleLongPress(gestureRecognizer : UILongPressGestureRecognizer){
        
        if (gestureRecognizer.state != UIGestureRecognizerState.ended){
            return
        }
        
        let p = gestureRecognizer.location(in: self.collectionView)
        
        if let indexPath : IndexPath = self.collectionView?.indexPathForItem(at: p){
            
            let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "FeedPopUp") as! FeedPopUp
            objVC.modalPresentationStyle = .overCurrentContext
            objVC.objFeedModel = arrFeeds[indexPath.row]
            
            if arrFeeds[indexPath.row].strUserID != AuthModel.sharedInstance.strID {
                self.navigationController?.present(objVC, animated: false, completion: nil)
            }
        }
    }
    
    @objc func btnRestartAction(_ sender: UIButton) {
        collectionView.contentOffset = .zero
        collectionView.reloadData()
    }

    @objc func btnLocationAction(_ sender: UIButton) {
        
        if Commons.isPremiumUser() {
             let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "LocaleViewController") as! LocaleViewController
//            objVC.strSelectedLocaleID = selectedLocale.strID == "" ? AuthModel.sharedInstance.strLocaleID : selectedLocale.strID
            objVC.strSelectedLocaleID = AuthModel.sharedInstance.strLocaleID
            self.navigationController?.present(objVC, animated: false, completion: nil)
        }
        else {
            let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "NonPremiumUserPopUp") as! NonPremiumUserPopUp
            objVC.strTitle = "Upgrade To Premium"
            objVC.strDesc = "You must have a premium membership to change your location."
            self.navigationController?.pushViewController(objVC, animated: false)
        }
        

        
//        pickerLocation = UIPickerView.init()
//        pickerLocation.delegate = self
//        pickerLocation.backgroundColor = UIColor.white
//        pickerLocation.setValue(UIColor.black, forKey: "textColor")
//        pickerLocation.autoresizingMask = .flexibleWidth
//        pickerLocation.contentMode = .center
//        pickerLocation.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
//        self.view.addSubview(pickerLocation)
//
//        toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))
//        toolBar.barStyle = .blackTranslucent
//        toolBar.items = [UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(onDoneButtonTapped))]
//        self.view.addSubview(toolBar)

    }
    
//    @objc func onDoneButtonTapped() {
//        toolBar.removeFromSuperview()
//        pickerLocation.removeFromSuperview()
//    }

    
    @objc func willEnterForeground() {
        // do stuff
        if lat == 0.0 {
           viewWillAppear(true)
        }
    }
    
    @objc func btnUserProfileAction(_ sender: UIButton) {
        self.strSelectedUserProfile = arrFeeds[sender.tag].strUserID
        self.strUserConnectStatus = arrFeeds[sender.tag].strUserConnectStatus
        self.strUserConnectID = arrFeeds[sender.tag].strConnectionID

        if self.strSelectedUserProfile == AuthModel.sharedInstance.strID {
            self.selectedIndex = sender.tag
            self.performSegue(withIdentifier: "toMyProfile", sender: nil)
        }
        else {
            selectedIndex = sender.tag
            callWebServiceForUserConnectStatus(showLoader: true, viewOriginalPost: false)
//            self.performSegue(withIdentifier: "toUserProfile", sender: nil)
        }
    }
    
    @objc func btnOriginalPostAction(_ sender: UIButton) {
        strOriginalPostID = arrFeeds[sender.tag].strOriginalPostID
        strSelectedUserProfile = arrFeeds[sender.tag].strOriginalPostUserID
        selectedIndex = sender.tag
        callWebServiceForUserConnectStatus(showLoader: true, viewOriginalPost: true)
//        let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
//        objVC.strPostID = self.strOriginalPostID
//        self.navigationController?.pushViewController(objVC, animated: true)
    }
    
    func removedBlockedUserPost(blockedUserID: String) {
        
        var postsToDelete = [Int]()
        
        for (index,value) in arrFeeds.enumerated() {
            if value.strUserID == blockedUserID {
                postsToDelete.append(index)
            }
        }
        
        arrFeeds.remove(at: postsToDelete)
        collectionView.reloadData()
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnLocationGlobalAction(_ sender : UIButton) {
        btnLocationAction(sender)
    }

    @IBAction func btnPostNowAction(_ sender : UIButton) {
        tabBarController?.selectedIndex = 2
    }
    
    @objc func btnUserLikeAction(_ sender: UIButton) {
        print("btnUserLikeAction")
        if Commons.isPremiumUser() {
             let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "LikedUserListViewController") as! LikedUserListViewController
            objVC.strPostID = arrFeeds[sender.tag].strID
            self.navigationController?.present(objVC, animated: true, completion: nil)
        }
        else {
             let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "NonPremiumUserPopUp") as! NonPremiumUserPopUp
            objVC.strTitle = "Upgrade To Premium"
            objVC.strDesc = "Please upgrade to premium to view the users who have liked your post."
            self.navigationController?.present(objVC, animated: true, completion: nil)
        }
    }
    
    @objc func btnUpgradeAction(sender: UIButton) {
        let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "GoPremiumPopUp") as! GoPremiumPopUp
        self.navigationController?.pushViewController(objVC, animated: false)
    }
    
    /*//MARK: - UIPickerView Delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1 // number of session
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return arrLocale.count // number of dropdown items
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrLocale[row].strName // dropdown item
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            strSelectedLocale = arrLocale[row].strID // selected item
//            txtLocation.text = arrLocale[row].strName
    }*/

    //MARK: - CLLocationManager delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
//        print("locations = \(locValue.latitude) \(locValue.longitude)")
        if firstTime {
           
            if arrFeeds.count == 0 {
                NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
                
                currentPage = 1
                self.lat = Double(locValue.latitude)
                self.long = Double(locValue.longitude)
                self.callWebServiceForFeedListing()

            }
            firstTime = false

        }
    }
    
    //MARK: - UICollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if arrFeeds.count > 0 {
            return arrFeeds.count + 1
        }
        else {
            return 1//arrFeeds.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if showNotActive {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NotActive", for: indexPath)
                        
            let btnUpgrade = cell.viewWithTag(10) as? UIButton
            btnUpgrade?.addTarget(self, action: #selector(btnUpgradeAction(sender:)), for: .touchUpInside)

            return cell
        }
        else if indexPath.row == arrFeeds.count {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LastCell", for: indexPath)
            
            let btnRestart = cell.viewWithTag(10) as? UIButton
            btnRestart?.addTarget(self, action: #selector(btnRestartAction(_:)), for: .touchUpInside)
            
            let btnLocation = cell.viewWithTag(20) as? UIButton
            btnLocation?.addTarget(self, action: #selector(btnLocationAction(_:)), for: .touchUpInside)

            return cell
        }
        else if arrFeeds[indexPath.row].strRoleID == "1"
        {
            let cell: FeedCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdminFeedCollectionViewCell", for: indexPath) as! FeedCollectionViewCell
            
            cell.lblUsername.text = arrFeeds[indexPath.row].strUsername
            cell.lblDate.text = arrFeeds[indexPath.row].strDate
            
            cell.imgPost!.kf.setImage(with: URL(string: arrFeeds[indexPath.row].strImgUrl), placeholder: nil)
            cell.imgPostBlur!.kf.setImage(with: URL(string: arrFeeds[indexPath.row].strImgUrl), placeholder: nil)
            
            cell.lblFeedText.text = arrFeeds[indexPath.row].strCaption
            cell.lblFeedText.handleHashtagTap { hashtag in
                print("Success. You just tapped the \(hashtag) hashtag")
                
                for value in self.arrFeeds[indexPath.row].arrHashTag {
                    if(value.strName.caseInsensitiveCompare(hashtag) == .orderedSame){
                        let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "HashTagFeedsViewController") as! HashTagFeedsViewController
                        objVC.objTagModel = value
                        self.navigationController?.pushViewController(objVC, animated: true)
                        break
                    }
                }
                
            }
            cell.lblFeedText.handleMentionTap({ (mention) in
                print("Success. You just tapped the \(mention) mention")
                //                self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: StringConstant.titleComingSoon)
                for value in self.arrFeeds[indexPath.row].arrUserTag {
                    if value.strName == mention {
                        self.strSelectedUserProfile = value.strID
                        self.strUserConnectStatus = value.strUserConnectStatus
                        self.strUserConnectID = value.strUserConnectionID

                        if self.strSelectedUserProfile == AuthModel.sharedInstance.strID {
                            self.selectedIndex = indexPath.row
                            self.performSegue(withIdentifier: "toMyProfile", sender: nil)
                        }
                        else {
                            self.selectedIndex = indexPath.row
                            self.callWebServiceForUserConnectStatus(showLoader: true, viewOriginalPost: false)
//                            self.performSegue(withIdentifier: "toUserProfile", sender: nil)
                        }
                    }
                }
            })
            
            /*if arrFeeds[indexPath.row].strUserID == AuthModel.sharedInstance.strID {
                cell.btnLike.isHidden = true
                cell.btnConnect.isHidden = true
                
                cell.likesView.isHidden = false
                
                if arrFeeds[indexPath.row].strLikeCount == "" ||  arrFeeds[indexPath.row].strLikeCount == "0" {
                    cell.lblUserLikes.text = "No Likes"
                }
                else if arrFeeds[indexPath.row].strLikeCount == "1" {
                    cell.lblUserLikes.text = "1 Like"
                }
                else {
                    cell.lblUserLikes.text = arrFeeds[indexPath.row].strLikeCount + " Likes"
                }
                
                cell.btnUserLike.tag = indexPath.row
                cell.btnUserLike.addTarget(self, action: #selector(btnUserLikeAction(_:)), for: .touchUpInside)
                
                cell.btnLike.isHidden = true
                //            cell.btnConnect.isHidden = true
                cell.lblDate.isHidden = false
                
            }
            else {*/
                cell.btnLike.isHidden = false
                cell.btnConnect.isHidden = false
                cell.likesView.isHidden = true
                
                cell.btnLike.tag = indexPath.row
                cell.btnLike.addTarget(self, action: #selector(btnLikeAction(_:)), for: .touchUpInside)
                
                cell.btnConnect.tag = indexPath.row
                cell.btnConnect.addTarget(self, action: #selector(btnConnectAction(_:)), for: .touchUpInside)
                arrFeeds[indexPath.row].isLiked ?  cell.btnLike.setBackgroundImage(#imageLiteral(resourceName: "liked"), for: .normal): cell.btnLike.setBackgroundImage(#imageLiteral(resourceName: "like"), for: .normal)
            //}
            
            cell.lblFeedText.enabledTypes = [.mention, .hashtag]
            cell.lblFeedText.hashtagColor = UIColor.darkGray
            cell.lblFeedText.mentionColor = UIColor.darkGray

            return cell
        }
        else {
            let cell: FeedCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedCollectionViewCell", for: indexPath) as! FeedCollectionViewCell
            
            if arrFeeds[indexPath.row].strOriginalPostID == "0" {
                cell.heightConstraintForOriginalPost.constant = 0
            }
            else {
                cell.heightConstraintForOriginalPost.constant = 25
            }
            
            cell.lblFeedText.enabledTypes = [.mention, .hashtag]
            cell.lblFeedText.hashtagColor = UIColor.init(white: 1, alpha: 0.4)
            cell.lblFeedText.mentionColor = UIColor.init(white: 1, alpha: 0.4)

            cell.lblUsername.text = arrFeeds[indexPath.row].strUsername
            cell.lblDate.text = arrFeeds[indexPath.row].strDate
            
            cell.imgUser!.kf.setImage(with: URL(string: arrFeeds[indexPath.row].strUserImage), placeholder: nil)
            cell.imgPost!.kf.setImage(with: URL(string: arrFeeds[indexPath.row].strImgUrl), placeholder: nil)
            cell.imgPostBlur!.kf.setImage(with: URL(string: arrFeeds[indexPath.row].strImgUrl), placeholder: nil)
            cell.btnUserName.tag = indexPath.row
            cell.btnUserName.addTarget(self, action: #selector(btnUserProfileAction(_:)), for: .touchUpInside)

            cell.btnOriginalPost.tag = indexPath.row
            cell.btnOriginalPost.addTarget(self, action: #selector(btnOriginalPostAction(_:)), for: .touchUpInside)

            cell.lblFeedText.text = arrFeeds[indexPath.row].strCaption
            cell.lblFeedText.handleHashtagTap { hashtag in
                print("Success. You just tapped the \(hashtag) hashtag")
                
                for value in self.arrFeeds[indexPath.row].arrHashTag {
                    if(value.strName.caseInsensitiveCompare(hashtag) == .orderedSame){
                        let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "HashTagFeedsViewController") as! HashTagFeedsViewController
                        objVC.objTagModel = value
                        self.navigationController?.pushViewController(objVC, animated: true)
                        break
                    }
                }
                
            }
            cell.lblFeedText.handleMentionTap({ (mention) in
                print("Success. You just tapped the \(mention) mention")
//                self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: StringConstant.titleComingSoon)
                for value in self.arrFeeds[indexPath.row].arrUserTag {
                    if value.strName == mention {
                        self.strSelectedUserProfile = value.strID
                        self.strUserConnectStatus = value.strUserConnectStatus
                        self.strUserConnectID = value.strUserConnectionID

                        if self.strSelectedUserProfile == AuthModel.sharedInstance.strID {
                            self.selectedIndex = indexPath.row
                            self.performSegue(withIdentifier: "toMyProfile", sender: nil)
                        }
                        else {
                            self.selectedIndex = indexPath.row
                            self.callWebServiceForUserConnectStatus(showLoader: true, viewOriginalPost: false)
//                            self.performSegue(withIdentifier: "toUserProfile", sender: nil)
                        }
                    }
                }
            })
            
            if arrFeeds[indexPath.row].strUserID == AuthModel.sharedInstance.strID {
                cell.btnLike.isHidden = true
                cell.btnConnect.isHidden = true
                cell.widthConstraintForReplyBtn.constant = 0
                
                cell.likesView.isHidden = false
                
                if arrFeeds[indexPath.row].strLikeCount == "" ||  arrFeeds[indexPath.row].strLikeCount == "0" {
                    cell.lblUserLikes.text = "No Likes"
                }
                else if arrFeeds[indexPath.row].strLikeCount == "1" {
                    cell.lblUserLikes.text = "1 Like"
                }
                else {
                    cell.lblUserLikes.text = arrFeeds[indexPath.row].strLikeCount + " Likes"
                }
                
                cell.btnUserLike.tag = indexPath.row
                cell.btnUserLike.addTarget(self, action: #selector(btnUserLikeAction(_:)), for: .touchUpInside)

                cell.btnLike.isHidden = true
                //            cell.btnConnect.isHidden = true
                cell.lblDate.isHidden = false

                cell.sensitiveBlurView.isHidden = true

            }
            else {
                
                if AuthModel.sharedInstance.strSensitivePostAllowed == kAllowSensitive {
                    cell.sensitiveBlurView.isHidden = true
                }
                else {
                    cell.sensitiveBlurView.isHidden = arrFeeds[indexPath.row].strSensitivePost == kAllowSensitive ? false : true
                }
                
                cell.btnLike.isHidden = false
                cell.btnConnect.isHidden = false
                cell.likesView.isHidden = true
                cell.widthConstraintForReplyBtn.constant = 35

                cell.btnReply.tag = indexPath.row
                cell.btnReply.addTarget(self, action: #selector(btnReplyAction(_:)), for: .touchUpInside)

                cell.btnLike.tag = indexPath.row
                cell.btnLike.addTarget(self, action: #selector(btnLikeAction(_:)), for: .touchUpInside)
                
                cell.btnConnect.tag = indexPath.row
                cell.btnConnect.addTarget(self, action: #selector(btnConnectAction(_:)), for: .touchUpInside)
                arrFeeds[indexPath.row].isLiked ?  cell.btnLike.setBackgroundImage(#imageLiteral(resourceName: "liked"), for: .normal): cell.btnLike.setBackgroundImage(#imageLiteral(resourceName: "like"), for: .normal)
            }

            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        //case of archived notification view
        if self.currentPage == self.totalPageCount {
            //case when no more items are there to load
            self.isLoadMore = false
        }
        else {
            self.isLoadMore = true
        }
        
        //get last element index
        let lastElement = arrFeeds.count - 2
        
        if indexPath.row == lastElement && isLoadMore {
            // handle your logic here to get more items, add it to dataSource and reload tableview
            
            self.currentPage = self.currentPage + 1
            callWebServiceForFeedListing()
        }
    }
    
    //MARK: - WebService Methods
    func callWebServiceForFeedListingRefresh() {
        
        if arrFeeds.count == 0 {
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        }
        
        var localeID = ""
        if Commons.isPremiumUser() {
            //            localeID = selectedLocale.strID == "" ? AuthModel.sharedInstance.strLocaleID : selectedLocale.strID
            localeID = AuthModel.sharedInstance.strLocaleID
        }
        
        let param = [
            "latitude": String(lat),
            "longitude": String(long),
            "page": currentPage,
            "localeId": localeID
            ] as [String : Any]
        
        print("------------R, ", param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Feed.feedList, param: param, withHeader: true ) { (response, errorMsg) in
            
            if self.arrFeeds.count == 0 {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            }
            
            self.showNotActive = false
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                if self.arrFeeds.count == 0 {
                    self.noPostsView.isHidden = false
                    self.collectionView.isHidden = true
                }
                else {
                    self.collectionView.isHidden = false
                    self.noPostsView.isHidden = true
                }
            }
            else{
                if response!["status"].boolValue == true {
                    FeedParser.parseFeedList(response: response!) { (arr, totalPages) in
                        if self.currentPage == 1 {
                            //if first time then set array
                            self.arrFeeds = arr
                        }
                        else {
                            let lastCounts = self.arrFeeds.count % 5
                            for _ in 1...lastCounts {
                                self.arrFeeds.removeLast()
                            }
                            
                            if lastCounts == 0 {
                                for _ in 1...5 {
                                    self.arrFeeds.removeLast()
                                }
                            }

                            //if next pages are loaded append the arrays to previous
                            self.arrFeeds.append(contentsOf: arr)
                        }
                        
                        self.totalPageCount = Int(totalPages)!
                        
                    }
                    
                    print("count: ", self.arrFeeds.count)
                    
                    
                    if self.arrFeeds.count == 0 {
                        self.noPostsView.isHidden = false
                        self.collectionView.isHidden = true
                    }
                    else {
                        self.collectionView.isHidden = false
                        self.noPostsView.isHidden = true
                        
//                        if self.selectedIndex > 0 {
//
//                            self.collectionView.scrollToItem(at:IndexPath(item: self.selectedIndex, section: 0), at: .centeredHorizontally, animated: false)
//                        }
//                        else {
                            self.collectionView.reloadData()
//                        }

                    }
                    
                }
                else {
                    
                    if response!["data"].stringValue == "failed-location"{
                        //                         let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "AppNotActivePopUp") as! AppNotActivePopUp
                        //                        self.navigationController?.pushViewController(objVC, animated: false)
                        
                        self.showNotActive = true
                        self.collectionView.reloadData()
                    }
                    else {
                        if self.arrFeeds.count == 0 {
                            self.noPostsView.isHidden = false
                            self.collectionView.isHidden = true
                        }
                        else {
                            self.collectionView.isHidden = false
                            self.noPostsView.isHidden = true
                        }
                        
                        self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                    }
                }
            }
        }
    }

    func callWebServiceForFeedListing() {
                
        if arrFeeds.count == 0 {
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        }
        
        var localeID = ""
        if Commons.isPremiumUser() {
//            localeID = selectedLocale.strID == "" ? AuthModel.sharedInstance.strLocaleID : selectedLocale.strID
            localeID = AuthModel.sharedInstance.strLocaleID
        }

        let param = [
            "latitude": String(lat),
            "longitude": String(long),
            "page": currentPage,
            "localeId": localeID
            ] as [String : Any]
        
        print("------------", param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Feed.feedList, param: param, withHeader: true ) { (response, errorMsg) in
            
            if self.arrFeeds.count == 0 {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            }
            
            self.showNotActive = false
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                if self.arrFeeds.count == 0 {
                    self.noPostsView.isHidden = false
                    self.collectionView.isHidden = true
                }
                else {
                    self.collectionView.isHidden = false
                    self.noPostsView.isHidden = true
                }
            }
            else{
                if response!["status"].boolValue == true {
                    FeedParser.parseFeedList(response: response!) { (arr, totalPages) in
                        if self.currentPage == 1 {
                            //if first time then set array
                            self.arrFeeds = arr
                        }
                        else {
                            //if next pages are loaded append the arrays to previous
                            self.arrFeeds.append(contentsOf: arr)
                        }
                        
                        self.totalPageCount = Int(totalPages)!
                        
                    }
                    
                    print("count: ", self.arrFeeds.count)
                    
               
                    if self.arrFeeds.count == 0 {
                        self.noPostsView.isHidden = false
                        self.collectionView.isHidden = true
                    }
                    else {
                        self.collectionView.isHidden = false
                        self.noPostsView.isHidden = true
//                        if self.selectedIndex > 0 {
//
//                            self.collectionView.scrollToItem(at:IndexPath(item: self.selectedIndex, section: 0), at: .centeredHorizontally, animated: false)
//                        }
//                        else {
                            self.collectionView.reloadData()
//                        }
                    }
                    
                }
                else {
                    
                    if response!["data"].stringValue == "failed-location"{
//                         let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "AppNotActivePopUp") as! AppNotActivePopUp
//                        self.navigationController?.pushViewController(objVC, animated: false)
                        
                        self.showNotActive = true
                        self.collectionView.reloadData()
                    }
                    else {
                        if self.arrFeeds.count == 0 {
                            self.noPostsView.isHidden = false
                            self.collectionView.isHidden = true
                        }
                        else {
                            self.collectionView.isHidden = false
                            self.noPostsView.isHidden = true
                        }
                        
                        self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                    }
                }
            }
        }
    }
    
    func callWebServiceForPostLike(postId: String, likeStatus: String, sender: UIButton) {
        
        //NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [
            "postId": postId,
            "likeStatus": likeStatus
            ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Feed.likePost, param: param, withHeader: true ) { (response, errorMsg) in
            
            //NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                if likeStatus == kLike {
                    sender.setBackgroundImage(#imageLiteral(resourceName: "like"), for: .normal)
                    self.arrFeeds[sender.tag].isLiked = false
                }
                else {
                    sender.setBackgroundImage(#imageLiteral(resourceName: "liked"), for: .normal)
                    self.arrFeeds[sender.tag].isLiked = true
                }
            }
            else{
                if response!["status"].boolValue == true {}
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                    if likeStatus == kLike {
                        sender.setBackgroundImage(#imageLiteral(resourceName: "like"), for: .normal)
                        self.arrFeeds[sender.tag].isLiked = false
                    }
                    else {
                        sender.setBackgroundImage(#imageLiteral(resourceName: "liked"), for: .normal)
                        self.arrFeeds[sender.tag].isLiked = true
                    }
                }
            }
        }
    }
    
    func callWebServiceForUserConnectStatus(showLoader: Bool, viewOriginalPost: Bool) {
                
        if showLoader {
           NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        }

        let param = [
            "userId": strSelectedUserProfile
            ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Connect.getConnectionStatus, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    self.strUserConnectStatus = response!["data"]["connectionStatus"].stringValue
                    self.strUserConnectStatus = response!["data"]["connectionStatus"].stringValue

                    if self.strUserConnectStatus == kBlocked {
                        if response!["data"]["blockedUserId"].stringValue != AuthModel.sharedInstance.strID {
                            //you blocked
                            self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: "User Blocked.")
                        }
                        else {
                            //you are blocked
                            if viewOriginalPost {
                                self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: "Unable to view post.")
                            }
                            else {
                                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Your are blocked by the user.")
                            }
//                            self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: "Unable to view post.")
                        }
                    }
                    else {
                        if viewOriginalPost {
                            let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
                            objVC.strPostID = self.strOriginalPostID
                            self.navigationController?.pushViewController(objVC, animated: true)
                        }
                        else {
                            self.performSegue(withIdentifier: "toUserProfile", sender: nil)
                        }
                    }

//                    self.strDaysRemaingToResend = response!["data"]["remainingDays"].stringValue
//                    self.strConnectionID = response!["data"]["connectionId"].stringValue

//                    self.setUpUI()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
        
        if segue.identifier == "toUserProfile" {
            let vc = segue.destination as! UserProfileContainer
            vc.strUserID = strSelectedUserProfile
            vc.strUserConnectionStatus = strUserConnectStatus
            vc.showIndexConnect = self.isFromConnect
            self.isFromConnect = false

            let objConnectionModel = ConnectionModel()
            objConnectionModel.strConnectionId = strUserConnectID
            objConnectionModel.strUserID = strSelectedUserProfile

            vc.objConnectionModel = objConnectionModel
        }
     }
}
