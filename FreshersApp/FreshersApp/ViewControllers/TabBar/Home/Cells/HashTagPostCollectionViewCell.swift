//
//  HashTagPostCollectionViewCell.swift
//  FreshersApp
//
//  Created by Apple on 25/08/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class HashTagPostCollectionViewCell: UICollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var imgPost:UIImageView!
    @IBOutlet weak var blurView:UIView!

}
