//
//  LocaleTableViewCell.swift
//  FreshersApp
//
//  Created by Apple on 15/07/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class LocaleTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblCountry:UILabel!
    @IBOutlet weak var bgView:UIView!
    @IBOutlet weak var imgLocation:UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
