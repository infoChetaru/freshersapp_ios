//
//  UserTableViewCell.swift
//  FreshersApp
//
//  Created by Apple on 16/07/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var imgUser:UIImageView!
    @IBOutlet weak var imgDropDown:UIImageView!
    @IBOutlet weak var btnRemove:UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
