//
//  FeedCollectionViewCell.swift
//  FreshersApp
//
//  Created by Apple on 05/05/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import ActiveLabel

class FeedCollectionViewCell: UICollectionViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet weak var lblFeedText:ActiveLabel!
    @IBOutlet weak var lblUsername:UILabel!
    @IBOutlet weak var imgPost:UIImageView!
    @IBOutlet weak var imgUser:UIImageView!
    @IBOutlet weak var imgPostBlur:UIImageView!
    @IBOutlet weak var btnLike:UIButton!
    @IBOutlet weak var btnConnect:UIButton!
    @IBOutlet weak var btnReply:UIButton!
    @IBOutlet weak var btnBottomView:UIButton!
    @IBOutlet weak var heightConstraintForCaptionText:NSLayoutConstraint!
    @IBOutlet weak var heightConstraintForTopUsername:NSLayoutConstraint!
    @IBOutlet weak var btnUserName:UIButton!
    @IBOutlet weak var likesView:UIView!
    @IBOutlet weak var btnUserLike:UIButton!
    @IBOutlet weak var lblUserLikes:UILabel!
    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var imgMaximize:UIImageView!
    @IBOutlet weak var heightConstraintForDate:NSLayoutConstraint!
    @IBOutlet weak var sensitiveBlurView:UIView!
    @IBOutlet weak var widthConstraintForReplyBtn:NSLayoutConstraint!
    @IBOutlet weak var btnOriginalPost:UIButton!
    @IBOutlet weak var heightConstraintForOriginalPost:NSLayoutConstraint!

    //MARK: - Variable
    var isExpand = false

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        self.lblFeedText.enabledTypes = [.mention, .hashtag]
//        self.lblFeedText.hashtagColor = UIColor.init(white: 1, alpha: 0.4)
//        self.lblFeedText.mentionColor = UIColor.init(white: 1, alpha: 0.4)

        self.btnBottomView.addTarget(self, action: #selector(btnBottomViewAction(_:)), for: .touchUpInside)

        if UIDevice.current.hasNotch {
            //... consider notch
            if heightConstraintForTopUsername != nil {
                heightConstraintForTopUsername.constant = 80
            }
        } else {
            //... don't have to consider notch
            if heightConstraintForTopUsername != nil {
                heightConstraintForTopUsername.constant = 50
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

//        let ht = (lblFeedText.text?.height(withConstrainedWidth: self.frame.size.width - 180, font: UIFont(name: "Poppins-Medium", size: 16)!))!
  
      /*  let ht = (lblFeedText.text?.height(withConstrainedWidth: self.lblFeedText.frame.size.width, font: UIFont(name: "Poppins-Medium", size: 16)!))!

        if ht > 55 {
            imgMaximize.isHidden = false
            imgMaximize.image = #imageLiteral(resourceName: "up")
        }
        else {
            imgMaximize.isHidden = true
        }*/
    }
    
    @objc func btnBottomViewAction(_ sender: UIButton) {
        print("bottomview button")
       
        isExpand = !isExpand
        
        if isExpand {
            heightConstraintForDate.constant = 14
            imgMaximize.image = #imageLiteral(resourceName: "down")
            
            let ht = (lblFeedText.text?.height(withConstrainedWidth: self.lblFeedText.frame.size.width , font: UIFont(name: "Poppins-Medium", size: 16)!))!
            
            if ht > 55 {
                heightConstraintForCaptionText.constant = ht
            }
            else {
                heightConstraintForCaptionText.constant = 55
            }

        }
        else {
            heightConstraintForDate.constant = 0
            imgMaximize.image = #imageLiteral(resourceName: "up")
            heightConstraintForCaptionText.constant = 55
        }
    }
}
