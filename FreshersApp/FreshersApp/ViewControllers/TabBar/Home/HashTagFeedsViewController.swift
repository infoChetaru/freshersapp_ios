//
//  HashTagFeedsViewController.swift
//  FreshersApp
//
//  Created by Apple on 08/05/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

class HashTagFeedsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var viewNoRecord: UIView!

    //MARK: - Variable
    let panel = JKNotificationPanel()
    var objTagModel = TagModel()
    var arrFeeds = [FeedModel]()
    var currentPage = 1
    var isLoadMore = true
    var totalPageCount = 1
    var premiumUser = false

    override func viewDidLoad() {
        super.viewDidLoad()

        setupData()
    }
    
    //MARK: - Custom Methods
    func setupData() {
        let flow = collectionView.collectionViewLayout as! UICollectionViewFlowLayout // If you create collectionView programmatically then just create this flow by UICollectionViewFlowLayout() and init a collectionView by this flow.

        let itemSpacing: CGFloat = 3
        let itemsInOneLine: CGFloat = 3
        flow.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let width = UIScreen.main.bounds.size.width - itemSpacing * CGFloat(itemsInOneLine - 1) //collectionView.frame.width is the same as  UIScreen.main.bounds.size.width here.
        flow.itemSize = CGSize(width: floor(width/itemsInOneLine), height: width/itemsInOneLine)
        flow.minimumInteritemSpacing = 5
        flow.minimumLineSpacing = 5
        collectionView.collectionViewLayout = flow

        premiumUser = Commons.isPremiumUser()
        
        self.lblTitle.text = "#" + objTagModel.strName
        callWebServiceForHashTagPosts()
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnBackAction(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }

    //MARK: - UICollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrFeeds.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: HashTagPostCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HashTagPostCollectionViewCell", for: indexPath) as! HashTagPostCollectionViewCell
        
        cell.imgPost.kf.setImage(with: URL(string: arrFeeds[indexPath.row].strImgUrl), placeholder: #imageLiteral(resourceName: "placeholderPost"))
        
        if arrFeeds[indexPath.row].expiredPost && !premiumUser {
            cell.blurView.isHidden = false
        }
        else {
            cell.blurView.isHidden = true
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let yourWidth = collectionView.bounds.width/3.0 - 5
            let yourHeight = yourWidth

            return CGSize(width: yourWidth, height: yourHeight)
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        //case of archived notification view
        if self.currentPage == self.totalPageCount {
            //case when no more items are there to load
            self.isLoadMore = false
        }
        
        //get last element index
        let lastElement = arrFeeds.count - 1
        
        if indexPath.row == lastElement && isLoadMore {
            // handle your logic here to get more items, add it to dataSource and reload tableview
            
            self.currentPage = self.currentPage + 1
            callWebServiceForHashTagPosts()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if arrFeeds[indexPath.row].expiredPost && !premiumUser {
            let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "NonPremiumUserPopUp") as! NonPremiumUserPopUp
            objVC.strTitle = "Upgrade To Premium To See This Post"
            objVC.strDesc = "This post has expired from the feed."
            self.navigationController?.pushViewController(objVC, animated: false)

            if navigationController == nil {
                self.present(objVC, animated: false, completion: nil)
            }
        }
        else {
            let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
//            objVC.strPostID = arrFeeds[indexPath.row].strID
            
            var arrMainFeeds = [FeedModel]()
            for value in arrFeeds {
                if !value.expiredPost || Commons.isPremiumUser(){
                    arrMainFeeds.append(value)
                }
            }
            
            objVC.arrFeedModel = arrMainFeeds
            objVC.selectedIndexPath = indexPath
            self.navigationController?.pushViewController(objVC, animated: true)
            if navigationController == nil {
                self.present(objVC, animated: true, completion: nil)
            }

        }
    }

    //MARK: - WebService Methods
    func callWebServiceForHashTagPosts() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [
            "hashTagId": objTagModel.strID,
            "page": currentPage
            ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Feed.hashTagFeedList, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                self.viewNoRecord.isHidden = false
            }
            else{
                if response!["status"].boolValue == true {
                    self.viewNoRecord.isHidden = true

                    FeedParser.parseHashTagFeedsList(response: response!) { (arr, totalPages, _) in
//                        self.arrFeeds = arr
                        
                        if self.currentPage == 1 {
                            //if first time then set array
                            self.arrFeeds = arr
                        }
                        else {
                            //if next pages are loaded append the arrays to previous
                            self.arrFeeds.append(contentsOf: arr)
                        }
                        
                        self.totalPageCount = Int(totalPages)!

                    }
                    
                    if self.arrFeeds.count == 0 {
                        self.viewNoRecord.isHidden = false
                    }

                    self.collectionView.reloadData()
                }
                else {
                    if self.arrFeeds.count == 0 {
                        self.viewNoRecord.isHidden = false
                    }

//                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
