//
//  ContactUsPopUp.swift
//  FreshersApp
//
//  Created by Apple on 11/05/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class ContactUsPopUp: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var txtContactUs:UITextView!
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var thanksView:UIView!
    @IBOutlet weak var contactUsView:UIView!
    @IBOutlet weak var lblThankYouDesc:UILabel!

    //MARK: - Variable
    var objFeedModel = FeedModel()
    let panel = JKNotificationPanel()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if objFeedModel.strID == "" {
            lblTitle.text = "Contact Us"
        }
        else {
            lblTitle.text = "Report Post"
        }
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnSubmitAction(_ sender: Any) {
        if (txtContactUs.text.isEmpty) {
            panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: StringConstant.emptyText)
        }
        else {
            if objFeedModel.strID == "" {
                callWebServiceToContactUsUserSpecific()
            }
            else {
//                callWebServiceToContactUs()
                callWebServiceForReportPost()
            }
        }
    }

    @IBAction func btnDismissAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnOkAction(_ sender: Any) {
        self.btnDismissAction(self)
    }
    
    //MARK: - WebService Methods
    func callWebServiceToContactUs() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [
            "postId": objFeedModel.strID,
            "summary": txtContactUs.text!
            ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Feed.contactUs, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    self.btnDismissAction(self)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceToContactUsUserSpecific() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [
            "summary": txtContactUs.text!
            ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Profile.userContactUs, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    self.thanksView.isHidden = false
                    self.contactUsView.isHidden = true
                    self.lblThankYouDesc.text = "We will respond as soon as possible."
                    self.view.endEditing(true)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    func callWebServiceForReportPost() {
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [
            "postId": objFeedModel.strID,
            "summary": txtContactUs.text!,
            "userId": AuthModel.sharedInstance.strID
            ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Feed.reportPost, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
//                    self.btnDismissAction(self)
                    self.thanksView.isHidden = false
                    self.contactUsView.isHidden = true
                    self.lblThankYouDesc.text = "For reporting this post."
                    self.view.endEditing(true)
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
