//
//  NonPremiumUserPopUp.swift
//  FreshersApp
//
//  Created by Apple on 14/05/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel

class NonPremiumUserPopUp: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var btnUpgradeSide:UIButton!
    @IBOutlet weak var btnUpgradeMiddle:UIButton!
    @IBOutlet weak var btnCancel:UIButton!
    @IBOutlet weak var lblDesc:UILabel!
    @IBOutlet weak var lblTitle:UILabel!

    //MARK: - Variables
    let panel = JKNotificationPanel()
    var shwoMiddleUpgrade = false
    var strTitle = "Non Premium Subscriber"
    var strDesc = "You must be a premium user to use this feature."

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if shwoMiddleUpgrade {
            btnUpgradeMiddle.isHidden = false
            btnUpgradeSide.isHidden = true
            btnCancel.isHidden = true
        }
        else {
            btnUpgradeMiddle.isHidden = true
            btnUpgradeSide.isHidden = false
            btnCancel.isHidden = false
        }
        
        lblTitle.text = strTitle
        lblDesc.text = strDesc
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnCancelAction(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: false, completion: nil)
    }

    @IBAction func btnUpgradeAction(_ sender : UIButton) {
//        self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: "Coming Soon!")
        let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "GoPremiumPopUp") as! GoPremiumPopUp
        self.navigationController?.pushViewController(objVC, animated: false)
        
        if self.navigationController == nil {
            self.present(objVC, animated: true, completion: nil)
        }
        
//        self.performSegue(withIdentifier: "toGoPremium", sender: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
