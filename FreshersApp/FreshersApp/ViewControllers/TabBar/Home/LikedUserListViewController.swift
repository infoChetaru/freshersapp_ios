//
//  LikedUserListViewController.swift
//  FreshersApp
//
//  Created by Apple on 16/07/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import JKNotificationPanel

class LikedUserListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var tblLikes: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var viewNoRecord: UIView!

    //MARK: - Variables
    let panel = JKNotificationPanel()
    var arrLikes = [UserProfileModel]()
    var arrFilteredLikes = [UserProfileModel]()
    var strPostID = ""
    var strSelectedUserID = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        setupData()
        callWebServiceForLikedUsersList()
    }
    
    //MARK: - Custom Methods
    /**
     * Set ups the UI
     */
    func setupData() {
        searchBar.delegate = self
        searchBar.backgroundImage = UIImage()

        tblLikes.tableFooterView = UIView()
    }

    //MARK: - IBAction Methods
    @IBAction func btnBackAction(_ sender : UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - UISearchBar Delegates
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        
        arrFilteredLikes = searchText.isEmpty ? arrLikes : arrLikes.filter { (item: UserProfileModel) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.strName.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        
        tblLikes.reloadData()
    }
    
    //MARK: - TableView data source and delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrFilteredLikes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UserTableViewCell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell") as! UserTableViewCell
        
        cell.selectionStyle = .none
                
        cell.lblName.text = arrFilteredLikes[indexPath.row].strName
        cell.imgUser!.kf.setImage(with: URL(string: arrFilteredLikes[indexPath.row].strProfileImage), placeholder: nil)

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        strSelectedUserID = arrFilteredLikes[indexPath.row].strID
        self.performSegue(withIdentifier: "toUserProfile", sender: nil)
    }
        
    //MARK: - WebService Methods
    func callWebServiceForLikedUsersList() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = ["postId": strPostID] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Feed.likedUser, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                if self.arrLikes.count == 0 {
                    self.viewNoRecord.isHidden = false
                    self.searchBar.isHidden = true
                }
            }
            else{
                if response!["status"].boolValue == true {
                    
                    self.viewNoRecord.isHidden = true
                    self.searchBar.isHidden = false

                    FeedParser.parseLikedUsersList(response: response!) { (arr) in
                        self.arrLikes = arr
                        self.arrFilteredLikes = arr
                    }
                    
                    if self.arrLikes.count == 0 {
                        self.viewNoRecord.isHidden = false
                        self.searchBar.isHidden = true
                    }

                    self.tblLikes.reloadData()
                }
                else {
                    if self.arrLikes.count == 0 {
                        self.viewNoRecord.isHidden = false
                        self.searchBar.isHidden = true
                    }
//                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "toUserProfile" {
            let vc = segue.destination as! UserProfileContainer
            vc.strUserID = strSelectedUserID
            
            let objConnectionModel = ConnectionModel()
//            objConnectionModel.strConnectionId = strSelectedConnectionID
            objConnectionModel.strUserID = strSelectedUserID

            vc.objConnectionModel = objConnectionModel
        }
    }
    

}
