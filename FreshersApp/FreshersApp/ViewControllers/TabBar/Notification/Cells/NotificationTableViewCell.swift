//
//  NotificationTableViewCell.swift
//  FreshersApp
//
//  Created by Apple on 14/05/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var imgView:UIImageView!
    @IBOutlet weak var lblNotificationText:UILabel!
    @IBOutlet weak var lblDate:UILabel!
    @IBOutlet weak var lblTitleText:UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
