//
//  NotificationViewController.swift
//  FreshersApp
//
//  Created by Apple on 14/05/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView

let notTagUser = "tagUser"
let notLike = "postLike"
let notCustom = "customNotification"
let notHashUser = "hashTagUser"

class NotificationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewNoRecord: UIView!

    //MARK: - Variable
    let panel = JKNotificationPanel()
    var currentPage = 1
    var arrNotifications = [NotificationModel]()
    var totalPageCount = 1
    var isLoadMore = true
    var refresher:UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: false)
//        callWebServiceForNotificationListing(showLoader: true)
        
        
        self.refresher = UIRefreshControl()
        self.tblView!.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor.white
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.tblView!.addSubview(refresher)

        currentPage = 1
        callWebServiceForNotificationListing(showLoader: arrNotifications.count > 0 ? false : true, page: currentPage)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Commons.callWebServiceToGetNotificationCount(tabBarController: self.tabBarController!, index: "3")

        if let tabItems = self.tabBarController?.tabBar.items {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[3]
            tabItem.badgeValue = nil
        }
        
        loadData()
    }
    
    //MARK: - Custom Methods
    @objc func loadData() {
        refresher.beginRefreshing()
        stopRefresher()
        
        if arrNotifications.count == 0 {
            callWebServiceForNotificationListing(showLoader:true, page: 1)
        }
        else {
            callWebServiceForNotificationListing(showLoader: false, page: 0)
        }
     }

    func stopRefresher() {
        refresher.endRefreshing()
     }

    //MARK: - UITableViewDelegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var identifier = "NotificationTableViewCell"
        if arrNotifications[indexPath.row].strType == notCustom {
            identifier = "NotificationTableViewCellAdmin"
        }
        
        let cell : NotificationTableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier) as! NotificationTableViewCell
        
        cell.selectionStyle = .none

        
        if arrNotifications[indexPath.row].strType == notCustom {
            cell.imgView!.image = #imageLiteral(resourceName: "logo")
            cell.lblTitleText.text = "Admin Notification"
        }
        else {
            cell.imgView.contentMode = .scaleAspectFill
            cell.imgView!.kf.setImage(with: URL(string: arrNotifications[indexPath.row].strImageUrl), placeholder: nil)
        }
        
        cell.lblNotificationText.text = arrNotifications[indexPath.row].strDesc
        cell.lblDate.text = arrNotifications[indexPath.row].strDate
                
        if arrNotifications[indexPath.row].isRead {
            cell.contentView.backgroundColor = ColorCodeConstant.themeLightColor
            cell.lblDate.textColor = ColorCodeConstant.themeColor
            cell.lblNotificationText.textColor = UIColor.black
            
            if cell.lblTitleText != nil {
                cell.lblTitleText.textColor = UIColor.black
            }
        }
        else {
            cell.contentView.backgroundColor = UIColor.clear
            cell.lblDate.textColor = ColorCodeConstant.themeLightColor
            cell.lblNotificationText.textColor = UIColor.white
            
            if cell.lblTitleText != nil {
                cell.lblTitleText.textColor = UIColor.white
            }
        }
        
        return cell

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !arrNotifications[indexPath.row].isRead {
            self.arrNotifications[indexPath.row].isRead = true
            self.tblView.reloadData()
            callWebServiceToMarkRead(index: indexPath.row)
        }
        
        if arrNotifications[indexPath.row].strType == notCustom {
            //admin post
            let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "AdminNotificationPopUp") as! AdminNotificationPopUp
            objVC.modalPresentationStyle = .overCurrentContext
            objVC.strDesc = arrNotifications[indexPath.row].strDesc
            self.present(objVC, animated: false, completion: nil)
            return
        }

        if Commons.isPremiumUser() || arrNotifications[indexPath.row].strType == notLike{
            let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
            objVC.strPostID = arrNotifications[indexPath.row].strPostID
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        else if !arrNotifications[indexPath.row].isInLocale {
            let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "NonPremiumUserPopUp") as! NonPremiumUserPopUp
            objVC.strTitle = "Upgrade To Premium To See This Post"
            objVC.strDesc = "This post is outside of your location."
            self.navigationController?.pushViewController(objVC, animated: false)
        }
        else if arrNotifications[indexPath.row].strPostExpired {
            let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "NonPremiumUserPopUp") as! NonPremiumUserPopUp
            objVC.strTitle = "Upgrade To Premium To See This Post"
            objVC.strDesc = "This post has expired from the feed."
            self.navigationController?.pushViewController(objVC, animated: false)
        }
        else {
            let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
            objVC.strPostID = arrNotifications[indexPath.row].strPostID
            self.navigationController?.pushViewController(objVC, animated: true)
        }
        
       /* if !arrNotifications[indexPath.row].isInLocale && !Commons.isPremiumUser() {
            let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "NonPremiumUserPopUp") as! NonPremiumUserPopUp
            objVC.strTitle = "Upgrade To Premium To See This Post"
            objVC.strDesc = "This post is outside of your location."
            self.navigationController?.pushViewController(objVC, animated: false)
        }
        else if arrNotifications[indexPath.row].strType == notLike || arrNotifications[indexPath.row].strType == notTagUser{
            let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
            objVC.strPostID = arrNotifications[indexPath.row].strPostID
            self.navigationController?.pushViewController(objVC, animated: true)
        }*/
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        //case of archived notification view
        if self.currentPage == self.totalPageCount {
            //case when no more items are there to load
            self.isLoadMore = false
        }
        
        //get last element index
        let lastElement = arrNotifications.count - 1
        
        if indexPath.row == lastElement && isLoadMore {
            // handle your logic here to get more items, add it to dataSource and reload tableview
            
            self.currentPage = self.currentPage + 1
            callWebServiceForNotificationListing(showLoader: false, page: currentPage)

        }
    }

    //MARK: - WebService Methods
    func callWebServiceForNotificationListing(showLoader: Bool, page: Int) {
        
        if showLoader {
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        }
        
        var date = ""
        if arrNotifications.count > 0 && page == 0 {
            date = arrNotifications[0].strCreatedAt
        }
        
        let param = [
            "page": page,
            "date": date
            ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Notification.notificationList, param: param, withHeader: true ) { (response, errorMsg) in
            
            print("++++++++++ ", response as Any)
            if showLoader {
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            }
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                if self.arrNotifications.count == 0 {
                    self.viewNoRecord.isHidden = false
                }
            }
            else{
                if response!["status"].boolValue == true {
                    
                    self.viewNoRecord.isHidden = true

                    NotificationParser.parseNotificationList(response: response!) { (arr, totalPages) in
                        
                        if page == 1 {
                            //if first time then set array
                            self.arrNotifications = arr
                        }
                        else {
                            if page == 0 {
                                self.arrNotifications.insert(contentsOf: arr, at: 0)
                            }
                            else {
                                //if next pages are loaded append the arrays to previous
                                self.arrNotifications.append(contentsOf: arr)
                            }
                        }
                        
                        if page != 0 {
                            //save total page count
                            self.totalPageCount = Int(totalPages)!
                        }
                        
                        if self.arrNotifications.count == 0 {
                            self.viewNoRecord.isHidden = false
                        }
                        
                        self.tblView.reloadData()
                    }
                }
                else {
                    if self.arrNotifications.count == 0 {
                        self.viewNoRecord.isHidden = false
                    }

//                    if self.currentPage == 1 {
//                        self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
//                    }
                }
            }
        }
    }

    func callWebServiceToMarkRead(index: Int) {
        
//        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [
            "notificationId": arrNotifications[index].strID
            ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Notification.markNotificationRead, param: param, withHeader: true ) { (response, errorMsg) in
            
//            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    self.tblView.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
