//
//  PostDetailViewController.swift
//  FreshersApp
//
//  Created by Apple on 22/05/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit
import JKNotificationPanel
import NVActivityIndicatorView
import ActiveLabel
import AudioToolbox

class PostDetailViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UIGestureRecognizerDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var collectionView:UICollectionView!

    //MARK: - Variable
    let panel = JKNotificationPanel()
//    var objFeedModel = FeedModel()
    var strPostID = ""
    var strSelectedUserProfile = ""
    var strSelectedUserConnectStatus = ""
    var strSelectedUserConnectConectionID = ""
    var arrFeedModel = [FeedModel]()
    var selectedIndexPath:IndexPath?
    var firstTime = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //hide navigation bar
        self.navigationController?.setNavigationBarHidden(true, animated: false)

//        setupData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupData()
    }
        
    override func viewDidLayoutSubviews() {
        if firstTime && strPostID == ""{
            firstTime = !firstTime
            collectionView.scrollToItem(at: selectedIndexPath!, at: .centeredHorizontally, animated: false)
        }
    }
    
    //MARK: - Custom Methods
    /**
     * Set ups the UI
     */
    func setupData() {
        
        //        allowLocationPermission()
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = layout
        
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        
        let lpgr : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action:#selector(handleLongPress(gestureRecognizer:)))
        lpgr.minimumPressDuration = 0.5
        lpgr.delegate = self
        lpgr.delaysTouchesBegan = true
        self.collectionView?.addGestureRecognizer(lpgr)
        
        if strPostID != "" {
            callWebServiceForPostDetail()
        }
    }
    
    @objc func btnUserLikeAction(_ sender: UIButton) {
        print("btnUserLikeAction")
        
        if Commons.isPremiumUser() {
             let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "LikedUserListViewController") as! LikedUserListViewController
            objVC.strPostID = arrFeedModel[sender.tag].strID //objFeedModel.strID
            self.present(objVC, animated: true, completion: nil)
        }
        else {
             let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "NonPremiumUserPopUp") as! NonPremiumUserPopUp
            objVC.strTitle = "Upgrade To Premium"
            objVC.strDesc = "Please upgrade to premium to view the users who have liked your post."
            self.navigationController?.pushViewController(objVC, animated: false)
//            self.navigationController?.present(objVC, animated: true, completion: nil)
            if self.navigationController == nil {
                self.present(objVC, animated: true, completion: nil)
            }
        }
    }
        
    @objc func btnLikeAction(_ sender: UIButton) {
        print("Like")
        
        if arrFeedModel[sender.tag].isLiked {
//        if objFeedModel.isLiked {
            return
        }

//        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) { }

        arrFeedModel[sender.tag].isLiked = !arrFeedModel[sender.tag].isLiked
//        objFeedModel.isLiked = objFeedModel.isLiked
        
//        if objFeedModel.isLiked {
        if arrFeedModel[sender.tag].isLiked {
            sender.setBackgroundImage(#imageLiteral(resourceName: "liked"), for: .normal)
//            callWebServiceForPostLike(postId: objFeedModel.strID, likeStatus: kLike, sender: sender)
            callWebServiceForPostLike(postId: arrFeedModel[sender.tag].strID, likeStatus: kLike, sender: sender)

    }
        else {
            sender.setBackgroundImage(#imageLiteral(resourceName: "like"), for: .normal)
            callWebServiceForPostLike(postId: arrFeedModel[sender.tag].strID, likeStatus: kUnLike, sender: sender)

//            callWebServiceForPostLike(postId: objFeedModel.strID, likeStatus: kUnLike, sender: sender)
        }
    }
    
    @objc func btnConnectAction(_ sender: UIButton) {
        print("Connect")
//        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) { }

        self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: StringConstant.titleComingSoon)
    }
    
    @objc func handleLongPress(gestureRecognizer : UILongPressGestureRecognizer){
        
        if (gestureRecognizer.state != UIGestureRecognizerState.ended){
            return
        }
                
        let p = gestureRecognizer.location(in: self.collectionView)
        
        if let indexPath : IndexPath = self.collectionView?.indexPathForItem(at: p){
            
            let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "FeedPopUp") as! FeedPopUp
            objVC.modalPresentationStyle = .overCurrentContext
            objVC.objFeedModel = arrFeedModel[indexPath.row]
            self.navigationController?.present(objVC, animated: false, completion: nil)
            if self.navigationController == nil {
                self.present(objVC, animated: true, completion: nil)
            }
        }

        
        
//        let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "FeedPopUp") as! FeedPopUp
//        objVC.modalPresentationStyle = .overCurrentContext
////        objVC.objFeedModel = objFeedModel
//        objVC.objFeedModel = objFeedModel
//        self.navigationController?.present(objVC, animated: false, completion: nil)
    }
    
    @objc func btnUserProfileAction(_ sender: UIButton) {
//        self.strSelectedUserProfile = objFeedModel.strUserID
        self.strSelectedUserProfile = arrFeedModel[sender.tag].strUserID
        self.strSelectedUserConnectStatus = arrFeedModel[sender.tag].strUserConnectStatus
        self.strSelectedUserConnectConectionID = arrFeedModel[sender.tag].strConnectionID
        
        if self.strSelectedUserProfile == AuthModel.sharedInstance.strID {
            self.performSegue(withIdentifier: "toMyProfile", sender: nil)
        }
        else {
            callWebServiceForUserConnectStatus(showLoader: true)
//            self.performSegue(withIdentifier: "toUserProfile", sender: nil)
        }
    }

    @objc func btnReplyAction(_ sender: UIButton) {
        tabBarController?.selectedIndex = 2
        var postNowTab = (self.tabBarController?.viewControllers![2] as? UINavigationController)?.topViewController as! CameraViewController
//        postNowTab.strOriginalPostID = objFeedModel.strID
        postNowTab.strOriginalPostID = arrFeedModel[sender.tag].strID

    }
    
    @objc func btnOriginalPostAction(_ sender: UIButton) {
        let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "PostDetailViewController") as! PostDetailViewController
        objVC.strPostID = arrFeedModel[sender.tag].strOriginalPostID
//        objVC.strPostID = objFeedModel.strOriginalPostID
        if self.navigationController == nil {
            self.present(objVC, animated: true, completion: nil)
        }
        else {
            self.navigationController?.pushViewController(objVC, animated: true)
        }
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnBackAction(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
            
    //MARK: - UICollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        if objFeedModel.strID == "" {
//            return 0
//        }
//            return 1
        return arrFeedModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
//        if objFeedModel.strRoleID == "1"
        if arrFeedModel[indexPath.row].strRoleID == "1"
        {
            let cell: FeedCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdminFeedCollectionViewCell", for: indexPath) as! FeedCollectionViewCell
            
            cell.lblUsername.text = arrFeedModel[indexPath.row].strUsername
            cell.lblDate.text = arrFeedModel[indexPath.row].strDate
            
            cell.imgPost!.kf.setImage(with: URL(string: arrFeedModel[indexPath.row].strImgUrl), placeholder: nil)
            cell.imgPostBlur!.kf.setImage(with: URL(string: arrFeedModel[indexPath.row].strImgUrl), placeholder: nil)
            
            cell.lblFeedText.text = arrFeedModel[indexPath.row].strCaption
            cell.lblFeedText.handleHashtagTap { hashtag in
                print("Success. You just tapped the \(hashtag) hashtag")
                
                for value in self.arrFeedModel[indexPath.row].arrHashTag {
                    if(value.strName.caseInsensitiveCompare(hashtag) == .orderedSame){
                        let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "HashTagFeedsViewController") as! HashTagFeedsViewController
                        objVC.objTagModel = value
                        self.navigationController?.pushViewController(objVC, animated: true)
                        if self.navigationController == nil {
                            self.present(objVC, animated: true, completion: nil)
                        }

                        break
                    }
                }
                
            }
            cell.lblFeedText.handleMentionTap({ (mention) in
                print("Success. You just tapped the \(mention) mention")
                //                self.panel.showNotify(withStatus: .warning, inView: self.appDelegate.window!, title: StringConstant.titleComingSoon)
                for value in self.arrFeedModel[indexPath.row].arrUserTag {
                    if value.strName == mention {
                        self.strSelectedUserProfile = value.strID
                        self.strSelectedUserConnectStatus = value.strUserConnectStatus
                        self.strSelectedUserConnectConectionID = value.strUserConnectionID
                        
                        if self.strSelectedUserProfile == AuthModel.sharedInstance.strID {
                            self.performSegue(withIdentifier: "toMyProfile", sender: nil)
                        }
                        else {
                            self.callWebServiceForUserConnectStatus(showLoader: true)
//                            self.performSegue(withIdentifier: "toUserProfile", sender: nil)
                        }
                    }
                }
            })
            
            /*if arrFeeds[indexPath.row].strUserID == AuthModel.sharedInstance.strID {
                cell.btnLike.isHidden = true
                cell.btnConnect.isHidden = true
                
                cell.likesView.isHidden = false
                
                if arrFeeds[indexPath.row].strLikeCount == "" ||  arrFeeds[indexPath.row].strLikeCount == "0" {
                    cell.lblUserLikes.text = "No Likes"
                }
                else if arrFeeds[indexPath.row].strLikeCount == "1" {
                    cell.lblUserLikes.text = "1 Like"
                }
                else {
                    cell.lblUserLikes.text = arrFeeds[indexPath.row].strLikeCount + " Likes"
                }
                
                cell.btnUserLike.tag = indexPath.row
                cell.btnUserLike.addTarget(self, action: #selector(btnUserLikeAction(_:)), for: .touchUpInside)
                
                cell.btnLike.isHidden = true
                //            cell.btnConnect.isHidden = true
                cell.lblDate.isHidden = false
                
            }
            else {*/
                cell.btnLike.isHidden = false
                cell.btnConnect.isHidden = false
                cell.likesView.isHidden = true
                
                cell.btnLike.tag = indexPath.row
                cell.btnLike.addTarget(self, action: #selector(btnLikeAction(_:)), for: .touchUpInside)
                
                cell.btnConnect.tag = indexPath.row
                cell.btnConnect.addTarget(self, action: #selector(btnConnectAction(_:)), for: .touchUpInside)
                arrFeedModel[indexPath.row].isLiked ?  cell.btnLike.setBackgroundImage(#imageLiteral(resourceName: "liked"), for: .normal): cell.btnLike.setBackgroundImage(#imageLiteral(resourceName: "like"), for: .normal)
            //}
            
            cell.lblFeedText.enabledTypes = [.mention, .hashtag]
            cell.lblFeedText.hashtagColor = UIColor.darkGray
            cell.lblFeedText.mentionColor = UIColor.darkGray

            return cell
        }
        else {
            let cell: FeedCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedCollectionViewCell", for: indexPath) as! FeedCollectionViewCell
            
            if arrFeedModel[indexPath.row].strOriginalPostID == "0" {
                cell.heightConstraintForOriginalPost.constant = 0
            }
            else {
                cell.heightConstraintForOriginalPost.constant = 25
            }

            cell.lblFeedText.enabledTypes = [.mention, .hashtag]
            cell.lblFeedText.hashtagColor = UIColor.init(white: 1, alpha: 0.4)
            cell.lblFeedText.mentionColor = UIColor.init(white: 1, alpha: 0.4)

            cell.lblUsername.text = arrFeedModel[indexPath.row].strUsername
            cell.imgUser!.kf.setImage(with: URL(string: arrFeedModel[indexPath.row].strUserImage), placeholder: nil)
            cell.imgPost!.kf.setImage(with: URL(string: arrFeedModel[indexPath.row].strImgUrl), placeholder: nil)
            cell.imgPostBlur!.kf.setImage(with: URL(string: arrFeedModel[indexPath.row].strImgUrl), placeholder: nil)
            cell.lblDate.text = arrFeedModel[indexPath.row].strDate
            cell.btnUserName.tag = indexPath.row
            cell.btnUserName.addTarget(self, action: #selector(btnUserProfileAction(_:)), for: .touchUpInside)
            cell.btnOriginalPost.tag = indexPath.row
            cell.btnOriginalPost.addTarget(self, action: #selector(btnOriginalPostAction(_:)), for: .touchUpInside)

            cell.lblFeedText.text = arrFeedModel[indexPath.row].strCaption
            cell.lblFeedText.handleHashtagTap { hashtag in
                print("Success. You just tapped the \(hashtag) hashtag")
                
                for value in self.arrFeedModel[indexPath.row].arrHashTag {
                    if(value.strName.caseInsensitiveCompare(hashtag) == .orderedSame){
                        let objVC = UIStoryboard(name: "TabBar", bundle: nil).instantiateViewController(withIdentifier: "HashTagFeedsViewController") as! HashTagFeedsViewController
                        objVC.objTagModel = value
                        
                        if self.navigationController == nil {
                            self.present(objVC, animated: true, completion: nil)
                        }
                        else {
                            self.navigationController?.pushViewController(objVC, animated: true)
                        }
                        
                        
                        break
                    }
                }
                
            }
            cell.lblFeedText.handleMentionTap({ (mention) in
                print("Success. You just tapped the \(mention) mention")
                for value in self.arrFeedModel[indexPath.row].arrUserTag {
                    if value.strName == mention {
                        self.strSelectedUserProfile = value.strID
                        self.strSelectedUserConnectStatus = value.strUserConnectStatus
                        self.strSelectedUserConnectConectionID = value.strUserConnectionID
                        
//                        if self.navigationController == nil {
//                            return
//                        }
                        if self.strSelectedUserProfile == AuthModel.sharedInstance.strID {
                            self.performSegue(withIdentifier: "toMyProfile", sender: nil)
                        }
                        else {
                            self.callWebServiceForUserConnectStatus(showLoader: true)
//                            self.performSegue(withIdentifier: "toUserProfile", sender: nil)
                        }
                    }
                }
            })
            
            if arrFeedModel[indexPath.row].strUserID == AuthModel.sharedInstance.strID {
                cell.btnLike.isHidden = true
                cell.btnConnect.isHidden = true
                cell.likesView.isHidden = false
                
                if arrFeedModel[indexPath.row].strLikeCount == "" || arrFeedModel[indexPath.row].strLikeCount == "0" {
                    cell.lblUserLikes.text = "No Likes"
                }
                else if arrFeedModel[indexPath.row].strLikeCount == "1" {
                    cell.lblUserLikes.text = "1 Like"
                }
                else {
                    cell.lblUserLikes.text = arrFeedModel[indexPath.row].strLikeCount + " Likes"
                }
                
                cell.btnUserLike.tag = indexPath.row
                cell.btnUserLike.addTarget(self, action: #selector(btnUserLikeAction(_:)), for: .touchUpInside)
                
                cell.btnLike.isHidden = true
                //            cell.btnConnect.isHidden = true
                cell.lblDate.isHidden = false
                
                cell.sensitiveBlurView.isHidden = true
            }
            else {
                if AuthModel.sharedInstance.strSensitivePostAllowed == kAllowSensitive {
                    cell.sensitiveBlurView.isHidden = true
                }
                else {
                    cell.sensitiveBlurView.isHidden = arrFeedModel[indexPath.row].strSensitivePost == kAllowSensitive ? false : true
                }

                cell.btnLike.isHidden = false
                //            cell.btnConnect.isHidden = false
                cell.lblDate.isHidden = true
                
                cell.likesView.isHidden = true
                cell.btnLike.tag = indexPath.row
                cell.btnLike.addTarget(self, action: #selector(btnLikeAction(_:)), for: .touchUpInside)
                
                cell.btnConnect.tag = indexPath.row
                cell.btnConnect.addTarget(self, action: #selector(btnConnectAction(_:)), for: .touchUpInside)
                arrFeedModel[indexPath.row].isLiked ?  cell.btnLike.setBackgroundImage(#imageLiteral(resourceName: "liked"), for: .normal): cell.btnLike.setBackgroundImage(#imageLiteral(resourceName: "like"), for: .normal)
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    //MARK: - WebService Methods
    func callWebServiceForPostDetail() {
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [
            "postId": strPostID
            ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Feed.postDetail, param: param, withHeader: true ) { (response, errorMsg) in
            
                NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    NotificationParser.parsePostDetail(response: response!["data"]) { (obj) in
                        self.arrFeedModel.append(obj)
//                        self.objFeedModel = obj
                    }
                    
                    self.collectionView.reloadData()
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }
    
    func callWebServiceForPostLike(postId: String, likeStatus: String, sender: UIButton) {
        
        //        NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        
        let param = [
            "postId": postId,
            "likeStatus": likeStatus
            ] as [String : Any]
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Feed.likePost, param: param, withHeader: true ) { (response, errorMsg) in
            
            //            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                if likeStatus == kLike {
                    sender.setBackgroundImage(#imageLiteral(resourceName: "like"), for: .normal)
                    self.arrFeedModel[sender.tag].isLiked = false
                }
                else {
                    sender.setBackgroundImage(#imageLiteral(resourceName: "liked"), for: .normal)
                    self.arrFeedModel[sender.tag].isLiked = true
                }
            }
            else{
                if response!["status"].boolValue == true {}
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                    if likeStatus == kLike {
                        sender.setBackgroundImage(#imageLiteral(resourceName: "like"), for: .normal)
                        self.arrFeedModel[sender.tag].isLiked = false
                    }
                    else {
                        sender.setBackgroundImage(#imageLiteral(resourceName: "liked"), for: .normal)
                        self.arrFeedModel[sender.tag].isLiked = true
                    }
                }
            }
        }
    }
    
    func callWebServiceForUserConnectStatus(showLoader: Bool) {
        
        if showLoader {
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(ActivityData(), nil)
        }
        
        let param = [
            "userId": strSelectedUserProfile
            ] as [String : Any]
        
        print(param)
        
        WebServiceHandler.postWebService(url: kBaseURL + NetworkConstant.Connect.getConnectionStatus, param: param, withHeader: true ) { (response, errorMsg) in
            
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
            if response == nil {
                self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
            }
            else{
                if response!["status"].boolValue == true {
                    
                    self.strSelectedUserConnectStatus = response!["data"]["connectionStatus"].stringValue
                    self.strSelectedUserConnectConectionID = response!["data"]["connectionId"].stringValue

                    if response!["data"]["connectionStatus"].stringValue == kBlocked {
                        if response!["data"]["blockedUserId"].stringValue != AuthModel.sharedInstance.strID {
                            //you blocked
                            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "User Blocked.")
                        }
                        else {
                            //you are blocked
                            self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: "Your are blocked by the user.")
                        }
                    }
                    else {
                        self.performSegue(withIdentifier: "toUserProfile", sender: nil)
                    }
                }
                else {
                    self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: errorMsg)
                }
            }
        }
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toUserProfile" {
            let vc = segue.destination as! UserProfileContainer
            vc.strUserID = strSelectedUserProfile
            vc.strUserConnectionStatus = strSelectedUserConnectStatus
            
            let objConnectionModel = ConnectionModel()
            objConnectionModel.strConnectionId = strSelectedUserConnectConectionID
            objConnectionModel.strUserID = strSelectedUserProfile

            vc.objConnectionModel = objConnectionModel
        }
    }

}
