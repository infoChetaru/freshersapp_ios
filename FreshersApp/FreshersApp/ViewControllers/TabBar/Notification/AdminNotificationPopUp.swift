//
//  AdminNotificationPopUp.swift
//  FreshersApp
//
//  Created by Apple on 24/08/20.
//  Copyright © 2020 Chetaru. All rights reserved.
//

import UIKit

class AdminNotificationPopUp: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var lblNotificationText:UILabel!

    //MARK: - Variable
    var strDesc = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblNotificationText.text = strDesc
    }
    
    //MARK: - IBAction Methods
    @IBAction func btnCancelAction(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
